/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package technicalanalysis.forceindex;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

/**
 *
 * @author tt
 */
public class ForceIndexPrice extends Price{
    private double forceIndex;
    
    public ForceIndexPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);        
    }

    public double getValue(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("force_index");
    }
    
    public void setExtraValue(double value){
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("force_index", value);
        setExtra(extraJSON);
    }
    
    public double getForceIndex() {
        return forceIndex;
    }

    public void setForceIndex(double forceIndex) {
        this.forceIndex = forceIndex;
    }
    
    public void setForceIndexGrade(int forceIndexDays){
        float forceIndexGrade = -1;
        if(this.value  == 0){
            forceIndexGrade = 5f;
        }else if(this.value < 0){
            double step = Math.abs(this.min/5);
            forceIndexGrade = (float) (TechnicalAnalysis.subtract(this.value, this.min)/step);
        }else if(this.value > 0){
            double step = Math.abs(this.max/5);
            forceIndexGrade = (float) (5+this.value/step);
        }

        setGrade(forceIndexGrade);
        
        //extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Force Index ("+forceIndexDays+")", getValue());
        setExtra(extraJSON);
    }
}
