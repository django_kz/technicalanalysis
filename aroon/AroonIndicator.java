package technicalanalysis.aroon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class AroonIndicator extends Indicator{
    int days;
    
    public AroonIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new AroonPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        days = (int)((long)settings.getTechnical().get("days"));
    }
    
   public AroonIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new AroonPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        days = hash.get("days");
    }
   
    public int getCutEdge(){
        return super.getCutEdge() + days;
    }
    
    public void calculate(){
        //cutAtBeginning(days);
        setValues();
        cutDays(days);
        prices.forEach(p->{
            ((AroonPrice)p).setAroonGrade();
        });
//        cutAtEnding();
    }

    private void setValues() {
        TreeSet<Double> highTree = new TreeSet();
        TreeSet<Double> lowTree = new TreeSet();
        HashMap<Double, Integer> countOfHighInTree = new HashMap();
        HashMap<Double, Integer> countOfLowInTree = new HashMap();
        LinkedList<Double> indexOfHigh = new LinkedList();
        LinkedList<Double> indexOfLow = new LinkedList();
        
        int index = prices.size()-1;
        for(int i = index; i > index-days+1 && i>=0; i--){            
            double high = prices.get(i).getAdjHigh();
            double low = prices.get(i).getAdjLow();
            if(!countOfHighInTree.containsKey(high)){
                highTree.add(high);
                countOfHighInTree.put(high, 1);
            }else{
                countOfHighInTree.put(high, countOfHighInTree.get(high)+1);
            }
            if(!countOfLowInTree.containsKey(low)){
                lowTree.add(low);
                countOfLowInTree.put(low, 1);
            }else{
                countOfLowInTree.put(low, countOfLowInTree.get(low)+1);
            }
            indexOfHigh.add(high);
            indexOfLow.add(low);
        }
        index = index-days+1;
        while(index >= 0){
            double high = prices.get(index).getAdjHigh();
            double low = prices.get(index).getAdjLow();
            if(!countOfHighInTree.containsKey(high)){
                highTree.add(high);
                countOfHighInTree.put(high, 1);
            }else{
                countOfHighInTree.put(high, countOfHighInTree.get(high)+1);
            }
            if(!countOfLowInTree.containsKey(low)){
                lowTree.add(low);
                countOfLowInTree.put(low, 1);
            }else{
                countOfLowInTree.put(low, countOfLowInTree.get(low)+1);
            }
            indexOfHigh.add(high);
            indexOfLow.add(low);
            
            double highestHigh = highTree.last();
            double lowestLow = lowTree.first();
            
            int daysSinceHighest = indexOf(indexOfHigh, highestHigh);
            int daysSinceLowest = indexOf(indexOfLow, lowestLow);
            
            double aroonUp = 100*TechnicalAnalysis.subtract(days, daysSinceHighest)/days;
            double aroonDown = 100*TechnicalAnalysis.subtract(days, daysSinceLowest)/days;
            
//            if (period==5) {
//                System.out.println(prices.get(index).getDate()+"\t"+high+"\t"+low+"\t"+daysSinceHighest+"\t"+daysSinceLowest+"\t"+aroonUp+"\t"+aroonDown);
//            }
            
            double value = TechnicalAnalysis.subtract(aroonUp, aroonDown);
            ((AroonPrice)prices.get(index)).setValue(value);
            
            double removeHigh = indexOfHigh.get(0);
            double removeLow = indexOfLow.get(0);
            countOfHighInTree.put(removeHigh, 
                    countOfHighInTree.get(removeHigh)-1);
            countOfLowInTree.put(removeLow, 
                    countOfLowInTree.get(removeLow)-1);
            if(countOfHighInTree.get(removeHigh)==0){
                highTree.remove(removeHigh);
                countOfHighInTree.remove(removeHigh);
            }
            if(countOfLowInTree.get(removeLow)==0){
                lowTree.remove(removeLow);
                countOfLowInTree.remove(removeLow);
            }
            indexOfHigh.remove(0);
            indexOfLow.remove(0);
            index--;
        }
    }

    private int indexOf(LinkedList<Double> indexes, double value) {
        for(int i = 0; i < indexes.size(); i++){
            if(indexes.get(i) == value){
                return indexes.size()-i;
            }
        }
        return -1;
    }
    
    
}
