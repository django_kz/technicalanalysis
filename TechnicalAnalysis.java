package technicalanalysis;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import technicalanalysis.macd.MacdIndicator;
import technicalanalysis.adx.AdxIndicator;
import technicalanalysis.aroon.AroonIndicator;
import technicalanalysis.bIndicator.BIndicator;
import technicalanalysis.bollinger.BollingerIndicator;
import technicalanalysis.cci.CciIndicator;
import technicalanalysis.cmf.CmfIndicator;
import technicalanalysis.coppockcurve.CoppockCurveIndicator;
import technicalanalysis.emv.EmvIndicator;
import technicalanalysis.forceindex.ForceIndexIndicator;
import technicalanalysis.keltner.KeltnerIndicator;
import technicalanalysis.kst.KstIndicator;
import technicalanalysis.massindex.MassIndexIndicator;
import technicalanalysis.mfi.MfiIndicator;
import technicalanalysis.ema.EmaIndicator;
import technicalanalysis.sma.SmaIndicator;
import technicalanalysis.obv.ObvIndicator;
import technicalanalysis.pivot.camarilla.CamarillaIndicator;
import technicalanalysis.pivot.classic.ClassicIndicator;
import technicalanalysis.pivot.fibonacci.FibonacciIndicator;
import technicalanalysis.pivot.wodie.WodieIndicator;
import technicalanalysis.ppo.PpoIndicator;
import technicalanalysis.rsi.RsiIndicator;
import technicalanalysis.stochasticoscillator.StochasticOscillatorIndicator;
import technicalanalysis.stochasticrsi.StochasticRsiIndicator;
import technicalanalysis.trix.TrixIndicator;
import technicalanalysis.tsi.TsiIndicator;
import technicalanalysis.ultimateoscillator.UltimateOscillatorIndicator;
import technicalanalysis.vortex.VortexIndicator;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TechnicalAnalysis {

    private static String DB_URL = "";
    private static String DB_USER = "";
    private static String DB_PASSWORD = "";

//    private final static String CONFIG = "/home/crypto/config/config.json";
    private final static String CONFIG = "/Users/apple/ADR_workin/technika/config/config.json";

    static final long SQUARE_ID = 8;
    
    private final static String QUEUE_NAME = "stochastic_rsi";
    private final static String QUEUE_BATCH = "batch";
    private final static String QUEUE_MARKETPRICE = "marketprice";

    public final static long ADX = 43147971;
    public final static long ULTIMATE_OSCILLATOR = 43147972;
    public final static long VORTEX_INDICATOR = 43147973;
    public final static long RSI = 43147974;
    public final static long STOCHASTIC_RSI = 43147975;
    public final static long STOCHASTIC_OSCILLATOR = 43147976;
    public final static long COPPOCK_CURVE = 43147977;
    public final static long KST = 43147978;
    public final static long MACD = 43147979;
    public final static long PPO = 43147980;
    public final static long BOLLINGER = 43147981;
    public final static long B_INDICATOR = 43147982;
    public final static long KELTNER = 43147983;
    public final static long MFI = 43147984;
    public final static long OBV = 43147985;
    public final static long CCI = 43147986;
    public final static long CMF = 43147987;
    public final static long EMV = 43147988;
    public final static long FORCE_INDEX = 43147989;
    public final static long MASS_INDEX = 43147990;
    public final static long AROON = 43147991;
    public final static long TRIX = 43147992;
    public final static long TSI = 43147993;
    public final static long MOVING_AVERAGE_EMA = 43147994;
    public final static long MOVING_AVERAGE_SMA = 43147995;
    public final static long CLASSIC = 43907936;
    public final static long FIBONACCI = 43907937;
    public final static long CAMARILLA = 43907938;
    public final static long WODIE = 43907939;

    public static Channel channel;

    public static PrintStream log;

    static boolean isWorking = false;

    public static void main(String[] args) throws ClassNotFoundException, SQLException, ParseException, IOException, TimeoutException {
        Class.forName("com.mysql.jdbc.Driver");

        log = new PrintStream(new File("TechnicalAnalysisLog.txt"));

        setDbSettings();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("marat_host");
        factory.setPort(5672);
        factory.setUsername("Test");
        factory.setPassword("Test");
        Connection connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.queueDeclare(QUEUE_MARKETPRICE, false, false, false, null);
        channel.queueDeclare(QUEUE_BATCH, false, false, false, null);

        channel.basicQos(1);

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException {
                isWorking = true;
                boolean update = false;
                try {
                    System.out.println(System.currentTimeMillis());
                    String ticker = new String(body, "UTF-8");

//                    int[] periods = {1, 5, 15, 30, 160, 360, 660, 1260, 124, 724, 1424, 130, 330, 630, 1230};
                    int[] periods = {1, 6, 124, 324, 724, 130, 330, 1365, 3365}; 

                    HashMap<Long, HashMap<Integer, Settings>> settings = new HashMap();
                    HashMap<Long, Pair<Date, HashMap<Integer, String>>> currentDate = new HashMap();

                    try (java.sql.Connection conn = DriverManager.getConnection(
                            DB_URL, DB_USER, DB_PASSWORD)) {

                       // try (Statement st = conn.createStatement();
                       //         ResultSet rs = st.executeQuery(
                       //                 "SELECT DISTINCT g.indicator_id, g.period, g.period_num, g.date, g.extra \n"
                       //                 + "FROM `current_indicator_grades` g\n"
                       //                 + "INNER JOIN indicators i ON g.indicator_id = i.id\n"
                       //                 + "WHERE g.ticker = '" + ticker + "' AND i.square_id = " + SQUARE_ID)) {
                       //     while (rs.next()) {
                       //         long indicatorId = rs.getLong("indicator_id");
                       //         Date date = rs.getDate("date");
                       //         String period = rs.getString("period");
                       //         int periodNum = rs.getInt("period_num");
                       //         String extra = rs.getString("extra");
                       //         if (!currentDate.containsKey(indicatorId)) {
                       //             currentDate.put(indicatorId,
                       //                     new Pair(date, new HashMap()));
                       //         }
                       //         currentDate.get(indicatorId).getValue().put(getPeriod(period, periodNum), extra);
                       //     }
                       // }
//initialize currentDate first date of our price
			// Settings are here
                        try (Statement st = conn.createStatement();
                                ResultSet rs = st.executeQuery(
                                        "SELECT indicator_id, period, technical, `interval` "
                                        + "FROM technicalanalysis_settings "
                                        + "WHERE ticker = '" + ticker + "'")) {
                            while (rs.next()) {
                                long indicatorId = rs.getLong("indicator_id");
                                int period = rs.getInt("period");
                                JSONObject technical = (JSONObject) new JSONParser().parse(rs.getString("technical"));
                                int interval = rs.getInt("interval");

                                if (!settings.containsKey(indicatorId)) {
                                    settings.put(indicatorId, new HashMap<>());
                                }
                                settings.get(indicatorId)
                                        .put(period, new Settings(period, technical, interval));
                            }
                        }

                    }
                    System.out.println(ticker);
                    System.out.println(System.currentTimeMillis());
                    boolean updateCurrent = false;
//                    for (int i = 0; i < periods.length; i++) {
                    for (int i = 3; i < periods.length ; i++) {

                        ArrayList<Price> prices = new ArrayList();
                        try (java.sql.Connection conn = DriverManager.getConnection(
                                DB_URL, DB_USER, DB_PASSWORD)) {
                            String table = getTableByPeriod(periods[i]);
                            System.out.println(table+"\t"+periods[i]);
                            try (Statement st = conn.createStatement();
                                    ResultSet rs = st.executeQuery(
                                            "SELECT mts, close, high, low, "
                                            + "volume, close, updated_at "
                                            + "FROM "+table+" WHERE ticker='" + ticker + "' "
                                            + "AND close IS NOT NULL")) {
                                while (rs.next()) {
                                    long mts = rs.getLong("mts");
                                    double adjClose = rs.getDouble("close");
                                    double adjHigh = rs.getDouble("high");
                                    double adjLow = rs.getDouble("low");
                                    double adjVolume = rs.getDouble("volume");
                                    double close = rs.getDouble("close");
                                    Date updatedAt = rs.getDate("updated_at");
                                    
                                    Date date = new Date(mts);
                                   
                                    prices.add(new Price(date, adjClose, adjHigh,
                                            adjLow, adjVolume, Math.round(close * 100),
                                            updatedAt, currentDate));
                                }
                            }
                            Collections.sort(prices, (Price r1, Price r2) -> r2.getDate().compareTo(r1.getDate()));
                        }
                        
                        checkSendMessage();
//                        FibonacciIndicator classic = new FibonacciIndicator(
//                                FIBONACCI, ticker, settings.get(FIBONACCI).get(periods[i]), prices, periods[i]);
//                        classic.calculate();
//                        
////                        List<String> sqls = classic.getSqls();
//                        List<Price> pprices = classic.getPrices();
//                        System.out.println("period " + periods[i]);
//                        System.out.println("len prices " + pprices.size());
//                        TreeMap<Date, Double> pprs = new TreeMap();
//                        TreeMap<Date, Float> grades = new TreeMap();
//                        for (Price p: pprices)
//                        {
//                            pprs.put(p.getDate(), p.getAdjClose());
//                            grades.put(p.getDate(), p.getGrade());
//                        }
//                        float acc = _CountAccuracy(grades, pprs).get(0); 
//                        System.out.println("acc " + _CountAccuracy(grades, pprs));
//                        sqls = classic.getSqls();
//                        if(!sqls.isEmpty()){
//                            updateCurrent=true;
//                        }             
//                        for(String sql : sqls){
//                            channel.basicPublish("", QUEUE_BATCH, null, sql.getBytes());
//                        }
                        HashMap<String,String> setting_1 = new HashMap<String,String>();
                        setting_1.put("name_1", "days");
                        setting_1.put("name_2", "sma1");
                        setting_1.put("name_3", "sma2");
//                        setting_1.put("name_4", "kst_sma");
                        setting_1.put("min_1","5");
                        setting_1.put("max_1","200");
                        setting_1.put("min_2","1");
                        setting_1.put("max_2","50");
                        setting_1.put("min_3","1");
                        setting_1.put("max_3","50");
//                        setting_1.put("min_4","5");
//                        setting_1.put("max_4","100");
                        putData(i, 
                                periods, 
                                STOCHASTIC_RSI, 
                                ticker, 
                                "`stochastic_rsi_grid`", 
                                prices, 
                                settings.get(STOCHASTIC_RSI).
                                        get(periods[i]), 
                                StochasticRsiIndicator.class, 
                                setting_1);
                    }
                  
                    System.out.println(System.currentTimeMillis());
                    update = true;
                } catch (Exception ex) {
                    ex.printStackTrace(log);
                } finally {
                    System.out.println("[x] Done " + update);
                    if (update) {
                        channel.basicAck(envelope.getDeliveryTag(), true);
                    } else {
                        System.exit(-1);
                        //channel.basicNack(envelope.getDeliveryTag(), true, true);
                    }
                    isWorking = false;
                }
                System.out.println("[x] Done " + update);
                isWorking = false;
            }

            private int getPeriod(String period, int periodNum) {
                if (period.equals("D")) {
                    return 1;
                } else if (period.equals("W") && periodNum == 1) {
                    return 5;
                } else if (period.equals("W") && periodNum == 2) {
                    return 10;
                } else if (period.equals("M")) {
                    return 20;
                } else if (period.equals("Q") && periodNum == 1) {
                    return 60;
                } else if (period.equals("Q") && periodNum == 2) {
                    return 125;
                } else if (period.equals("Y") && periodNum == 1) {
                    return 250;
                } else {
                    return 1250;
                }
            }
        };
        channel.basicConsume(QUEUE_NAME, false, consumer);
        System.out.println("START");

        //check for finish
        //new Thread(new Runnable() {
        //    @Override
        //    public void run() {
        //        try {
        //            while (channel.isOpen()) {
        //                Thread.sleep(10000);
        //                if (channel.consumerCount(QUEUE_MARKETPRICE) == 0
        //                        && channel.messageCount(QUEUE_NAME) == 0 && !isWorking) {
        //                    System.exit(0);
        //                }
        //            }
        //            System.exit(2);
        //        } catch (InterruptedException | IOException ex) {
        //            ex.printStackTrace(log);
        //            System.exit(1);
        //        }
        //    }
        //}).start();
    }

    public static double sum(double a, double b) {
        return (double) Math.round(a * 100000000 + b * 100000000) / 100000000;
//        return a+b;
    }

    public static double subtract(double a, double b) {
        return (double) Math.round(a * 100000000 - b * 100000000) / 100000000;
//        return a-b;
    }

    public static void checkSendMessage() throws IOException {
        while (channel.messageCount(QUEUE_BATCH) > 500000);
    }

    private static void setDbSettings() {
        try (Scanner in = new Scanner(new File(CONFIG))) {
            StringBuilder builder = new StringBuilder();
            while (in.hasNextLine()) {
                builder.append(in.nextLine());
            }

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(builder.toString());

            DB_URL = (String) json.get("db_url");
            DB_USER = (String) json.get("db_user");
            DB_PASSWORD = (String) json.get("db_password");
        } catch (FileNotFoundException | org.json.simple.parser.ParseException ex) {
            ex.printStackTrace(log);
        }
    }
    
    private static String getTableByPeriod(int period){
        String table = "prices1m";
        switch(period){
            case 1: table = "prices1m"; break;
            case 6: table = "prices5m"; break;
            case 124: table = "prices15m"; break;
            case 324: table = "prices30m"; break;
            case 724: table = "prices1h"; break;
            case 130: table = "prices6h"; break;
            case 330: table = "prices12h"; break;
            case 1365: table = "prices1d"; break;
            case 3365: table = "prices7d"; break;            
        }
        
        return table;
    }
    
//    private int getStepIntervalByPeriod(int period){
//        ArrayList<Integer> lst = new ArrayList();
//
//        switch(period){
//            case 1: return 60;//0,5 to 3x
//            case 6: return 6*12;
//            case 124: return 24*4;
//            case 324: return 3*24*2;
//            case 724: return 7*24;
//            case 130: return 30 * 4;
//            case 330: return 3 * 30 * 2;
//            case 1365: return 365;
//            case 3365: return 3 * 51;
//        }
//        return -1;
//    }
        
    private static ArrayList<Integer> getHistoryByPeriod(int period){
      ArrayList<Integer> lst = new ArrayList();
      switch(period){
        //case 1: return new List(60, 600);
        //case 6: return new List(120, 1200);
        //case 124: return new List(96, 960); 
        //case 324: return new List(144, 1440); 
        //case 724: return new List(168, 1680);
        //case 130: return new List(120, 1200);
        //case 330: return new List(180, 1800); 
        //case 1365: return new List(365, 3650);
        //case 3365: return new List(130, 650);
        case 1:   lst.add(20); lst.add(600);
        case 6:   lst.add(24); lst.add(1200);
        case 124: lst.add(32); lst.add(960); 
        case 324: lst.add(48); lst.add(1440); 
        case 724: lst.add(48); lst.add(1680);
        case 130: lst.add(40); lst.add(1200);
        case 330: lst.add(60); lst.add(1800); 
        case 1365:    lst.add(120); lst.add(1080);
        case 3365:    lst.add(26); lst.add(150);
    }
      return lst;
    }

    private static List<Float> _CountAccuracy(
                                        TreeMap<Date, Float> grades,
                                        TreeMap<Date, Double> prices
                                        ) 
    {
        Date firstDate;
        List<Float> return_array = new ArrayList();

        try{
//            System.out.println("prices size" + prices.size());
            firstDate = prices.firstKey();
        }
        catch (java.util.NoSuchElementException ex) {
            return_array.add((float)-1);
            return return_array;
        }
        long green = 0;
        long greenSuccess = 0;
        long red = 0;
        long redSuccess = 0;


        //start of counting
        String signal = "";
        TreeMap<Date, String> realSignals = new TreeMap();
        for (Date date : grades.keySet()) {
            if (date.equals(firstDate) || date.after(firstDate)) {
                float grade = grades.get(date);
                String realSignal = "";
                if (grade >= 0 && grade < 2 && !signal.equals("S100")) {
                    signal = "S100";
                    realSignal = "red";
                } else if (grade >= 2 && grade < 4 && !signal.equals("S50")) {
                    signal = "S50";
                    realSignal = "red";
                } else if (grade >= 6 && grade < 8 && !signal.startsWith("B")) {
                    signal = "B50";
                    realSignal = "green";
                } else if (grade >= 8 && grade <= 10 && !signal.equals("B100")) {
                    signal = "B100";
                    realSignal = "green";
                } else if (grade == -1 || (grade >= 4 && grade < 6)) {
                    signal = "";
                }

                if (!realSignal.isEmpty()) {
                    if (realSignals.isEmpty()
                            || !realSignals.firstEntry().getValue().equals(realSignal)) {
                        if (!realSignal.isEmpty()) {
                            for (Date signalDate : realSignals.keySet()) {
                                Entry startPriceEntry = prices.floorEntry(signalDate);
                                Entry endPriceEntry = prices.higherEntry(date);
                                if (startPriceEntry != null && endPriceEntry != null) {
                                    if (realSignal.equals("green")) {
                                        red++;
                                        if ((double) startPriceEntry.getValue()
                                                > (double) endPriceEntry.getValue()) {
                                            redSuccess++;
                                        }
                                    } else {
                                        green++;
                                        if ((double) endPriceEntry.getValue()
                                                > (double) startPriceEntry.getValue()) {
                                            greenSuccess++;
                                        }
                                    }
                                }
                            }
                            realSignals.clear();
                        }

                    }
                    realSignals.put(date, realSignal);
                }
            }
        }

        //last shtrix
        if (!realSignals.isEmpty()) {
            for (Date signalDate : realSignals.keySet()) {
                Entry startPriceEntry = prices.floorEntry(signalDate);
                Entry endPriceEntry = prices.lastEntry();
                if (startPriceEntry != null && endPriceEntry != null
                        && ((Date) endPriceEntry.getKey()).after((Date) startPriceEntry.getKey())) {
                    if (realSignals.firstEntry().getValue().equals("red")) {
                        red++;
                        if ((double) startPriceEntry.getValue()
                                > (double) endPriceEntry.getValue()) {
                            redSuccess++;
                        }
                    } else {
                        green++;
                        if ((double) endPriceEntry.getValue()
                                > (double) startPriceEntry.getValue()) {
                            greenSuccess++;
                        }
                    }
                }
            }
        }

        //save result
        float accuracy = (float)(redSuccess + greenSuccess) / (red + green);
        return_array.add(accuracy);
        return_array.add((float)red);
        return_array.add((float)redSuccess);
        return_array.add((float)green);
        return_array.add((float)greenSuccess);
        return return_array;
        
    }
    
    private static <T extends Indicator> void putData(int period,
                                                      int[] periods,
                                                      long indicator_id, 
                                                      String ticker,
                                                      String table_name,
                                                      ArrayList<Price> prices,
                                                      Settings  settings,
                                                      Class<T> IndicatorType,
                                                      HashMap<String, String> setting_1
                                                      ) throws NoSuchMethodException, 
                                                               InstantiationException, 
                                                               IllegalAccessException, 
                                                               IllegalArgumentException, 
                                                               InvocationTargetException, 
                                                               IOException
    {   
        
  	List<Float> acc_30_default = new ArrayList();
        List<Float> acc_default = new ArrayList();
  	List<Price> prices_70 = new ArrayList<Price>();
  	List<Price> prices_30 = new ArrayList<Price>();
 	List<Float> acc_max;
        HashMap<String,Integer> best_setting;
  	HashMap<String, Integer> setting_dynamic = new HashMap<String, Integer>();
  	HashMap<String, Float> history_70 = new HashMap();
  	HashMap<String, Float> history_70_default = new HashMap();
        float divide = (float) 0.7;
        if (period == 8){
            divide = (float)0.5;
        }
        prices_70 = prices.subList(0, (int)(prices.size() * divide));
        T indicator = IndicatorType.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class})
                                                      .newInstance(new Object[]{indicator_id, ticker, settings, prices});
        int start = (int)(prices.size() * divide) - indicator.getCutEdge();
        start = (start > 0 ) ? start: 0;
        prices_30 = prices.subList(start, prices.size());

//        for(int c = 0; c < prices.size(); c++){
//            if(c < prices.size()*0.7){
//                prices_70.add(prices.get(c)); 
//            }else{
//                prices_30.add(prices.get(c));
//            }
//        }


        history_70_default = findBestHistory(period,
                                             periods,
                                             indicator_id, 
                                             ticker, 
                                             prices_70, 
                                             settings, 
                                             IndicatorType, 
                                             channel,
                                             setting_dynamic,
                                             true);
        if (history_70_default.get("history").intValue() == -1){
            System.out.println("HERE IS IT");
            acc_30_default.add((float)-1);
            acc_default.add((float)-1);
            
        }
        else {
            acc_30_default = findAccuracy(
                                    indicator_id, 
                                    ticker,
                                    history_70_default.get("history").intValue(), 
                                    prices_30, 
                                    settings, 
                                    IndicatorType,
                                    setting_dynamic,
                                    true);
            acc_default = findAccuracy(
                                indicator_id, 
                                ticker,
                                history_70_default.get("history").intValue(), 
                                prices, 
                                settings, 
                                IndicatorType,
                                setting_dynamic,
                                true);
        }
  	for(int z= Integer.parseInt( setting_1.get("min_1")); z<=Integer.parseInt( setting_1.get("max_1"));z+=5){
            for(int y= Integer.parseInt( setting_1.get("min_2")); y<=Integer.parseInt( setting_1.get("max_2"));y+=2){   
                for(int x= Integer.parseInt( setting_1.get("min_3")); x<=Integer.parseInt( setting_1.get("max_3")); x+=2){
                    if(!setting_1.get("name_1").equals("None")){
                        setting_dynamic.put(setting_1.get("name_1"),z);
                    }
                    if(!setting_1.get("name_2").equals("None")){
                        setting_dynamic.put(setting_1.get("name_2"),y);
                    }
                    if(!setting_1.get("name_3").equals("None")){
                        setting_dynamic.put(setting_1.get("name_3"),x);
                    }
                    if (setting_1.get("name_2").equals("None")){
                        try{
                                history_70= findBestHistory(period,
                                                            periods,
                                                            indicator_id, 
                                                            ticker, 
                                                            prices_70, 
                                                            settings, 
                                                            IndicatorType, 
                                                            channel, 
                                                            setting_dynamic,
                                                            false);
                            }
                            catch (Exception ex){
                                history_70 = history_70_default;
        ;
                            }
                    }
                    else{
                        history_70 = history_70_default;
   
                    }

                            
//                    if (!setting_1.get("name_2").equals("None")){
//                        try{
//                            history_70= findBestHistory(period,
//                                                        periods,
//                                                        indicator_id, 
//                                                        ticker, 
//                                                        prices_70, 
//                                                        settings, 
//                                                        IndicatorType, 
//                                                        channel, 
//                                                        setting_dynamic,
//                                                        false);
//                        }
//                        catch (Exception ex){
//                            continue;
//                        }
//                    }
//                    else {
//                        history_70.put("history", (float)settings.getInterval());
//                        float acc_70 = findAccuracy(
//                                                    indicator_id, 
//                                                    ticker,
//                                                    history_70.get("history").intValue(), 
//                                                    prices_30, 
//                                                    settings, 
//                                                    IndicatorType, 
//                                                    setting_dynamic,
//                                                    false
//                                                    );
//                        history_70.put("acc_max", acc_70);
//                    }
                    if (history_70.get("history").intValue() == -1)
                        continue;
                    indicator = IndicatorType.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class, HashMap.class})
                                                       .newInstance(new Object[]{indicator_id, ticker, settings, prices, setting_dynamic});
                    start = (int)(prices.size() * divide) - indicator.getCutEdge();
                    start = (start > 0 ) ? start: 0;
                    prices_30 = prices.subList(start, prices.size());

                    List<Float> acc_30 = findAccuracy( 
                                                    indicator_id, 
                                                    ticker,
                                                    history_70.get("history").intValue(), 
                                                    prices_30, 
                                                    settings, 
                                                    IndicatorType, 
                                                    setting_dynamic,
                                                    false);
                    
                    List<Float> acc = findAccuracy( 
                                                indicator_id, 
                                                ticker,
                                                history_70.get("history").intValue(), 
                                                prices, 
                                                settings, 
                                                IndicatorType, 
                                                setting_dynamic,
                                                false);
                    
                    if (acc_30.size() == 1 || acc.size() == 1){
                        continue;
                    }
                    
                    String sql = "INSERT IGNORE INTO " + table_name + " (`period`, `" + setting_1.get("name_1") + "`";
                    if (!setting_1.get("name_2").equals("None")){
                        sql +=  ", `" + setting_1.get("name_2") + "`";
                    }
                    if (!setting_1.get("name_3").equals("None")){
                        sql += ", `" + setting_1.get("name_3") + "`";
                    }
                    sql += ", `history`, `accuracy_100_dynamic`, `accuracy_100_const`, `accuracy_70_dynamic`, `accuracy_70_const`,";
                    sql += "`accuracy_30_dynamic`, `red`, `redSuccess`, `green`, `greenSuccess`, `accuracy_30_const`)";
                    sql += " VALUES (\'" + periods[period] + "\', " + z;
                    if (!setting_1.get("name_2").equals("None")){
                        sql +=  ", " + y;
                    }
                    if (!setting_1.get("name_3").equals("None")){
                        sql += ", " + x;
                    }
                    sql += ", " + history_70.get("history").intValue() + ", " + acc.get(0) + ", " +    acc_default.get(0);
                    sql += ", "+ history_70.get("acc_max") + ", ";
                    sql += history_70_default.get("acc_max") +", " + acc_30.get(0) + ", " + acc_30.get(1) + ", ";
                    sql += acc_30.get(2) + ", " + acc_30.get(3) + ", " + acc_30.get(4) + ", " + acc_30_default.get(0) + ")";
                    System.out.println(sql);
                    channel.basicPublish("", QUEUE_BATCH, null, sql.getBytes());
                    }
            }
 	}
    }
    
        private static <T extends Indicator> void putData2(
                                                            int period,
                                                            int[] periods,
                                                            long indicator_id, 
                                                            String ticker,
                                                            String table_name,
                                                            ArrayList<Price> prices,
                                                            Settings  settings,
                                                            Class<T> IndicatorType,
                                                            HashMap<String, String> setting_1
                                                            ) throws NoSuchMethodException, 
                                                                     InstantiationException, 
                                                                     IllegalAccessException, 
                                                                     IllegalArgumentException, 
                                                                     InvocationTargetException, 
                                                                     IOException
    {   
        
  	List<Float> acc_30_default = new ArrayList();
        List<Float> acc_default = new ArrayList();
  	List<Price> prices_70 = new ArrayList<Price>();
  	List<Price> prices_30 = new ArrayList<Price>();
 	List<Float> acc_max;
        HashMap<String,Integer> best_setting;
  	HashMap<String, Integer> setting_dynamic = new HashMap<String, Integer>();
  	HashMap<String, Float> history_70 = new HashMap();
  	HashMap<String, Float> history_70_default = new HashMap();
        prices_70 = prices.subList(0, (int)(prices.size() * 0.7));
        T indicator = IndicatorType.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class})
                                                      .newInstance(new Object[]{indicator_id, ticker, settings, prices});
        int start = (int)(prices.size() * 0.7) - indicator.getCutEdge();
        start = (start > 0 ) ? start: 0;
        prices_30 = prices.subList(start, prices.size());

//        for(int c = 0; c < prices.size(); c++){
//            if(c < prices.size()*0.7){
//                prices_70.add(prices.get(c)); 
//            }else{
//                prices_30.add(prices.get(c));
//            }
//        }


        history_70_default = findBestHistory(period,
                                             periods,
                                             indicator_id, 
                                             ticker, 
                                             prices_70, 
                                             settings, 
                                             IndicatorType, 
                                             channel,
                                             setting_dynamic,
                                             true);
        if (history_70_default.get("history").intValue() == -1){
            acc_30_default.add((float)-1);
            acc_default.add((float)-1);
            
        }
        else {
            acc_30_default = findAccuracy(
                                    indicator_id, 
                                    ticker,
                                    history_70_default.get("history").intValue(), 
                                    prices_30, 
                                    settings, 
                                    IndicatorType,
                                    setting_dynamic,
                                    true);
            acc_default = findAccuracy(
                                indicator_id, 
                                ticker,
                                history_70_default.get("history").intValue(), 
                                prices, 
                                settings, 
                                IndicatorType,
                                setting_dynamic,
                                true);
        }
  	for(int z= Integer.parseInt( setting_1.get("min_1")); z<=Integer.parseInt( setting_1.get("max_1"));z+=5){
            for(int y= Integer.parseInt( setting_1.get("min_2")); y<=Integer.parseInt( setting_1.get("max_2"));y+=5){   
                for(int x= Integer.parseInt( setting_1.get("min_3")); x<=Integer.parseInt( setting_1.get("max_3")); x+=5){
                    for(int k= Integer.parseInt( setting_1.get("min_4")); k<=Integer.parseInt( setting_1.get("max_4")); k+=5){
                        if ((z >= y) || (y >= x)) {
                            continue;
                        }
                        if(!setting_1.get("name_1").equals("None")){
                            setting_dynamic.put(setting_1.get("name_1"),z);
                        }
                        if(!setting_1.get("name_2").equals("None")){
                            setting_dynamic.put(setting_1.get("name_2"),y);
                        }
                        if(!setting_1.get("name_3").equals("None")){
                            setting_dynamic.put(setting_1.get("name_3"),x);
                        }
                        if(!setting_1.get("name_4").equals("None")){
                            setting_dynamic.put(setting_1.get("name_4"),x);
                        }
                        if (setting_1.get("name_2").equals("None")){
                        try{
                                history_70= findBestHistory(period,
                                                            periods,
                                                            indicator_id, 
                                                            ticker, 
                                                            prices_70, 
                                                            settings, 
                                                            IndicatorType, 
                                                            channel, 
                                                            setting_dynamic,
                                                            false);
                            }
                            catch (Exception ex){
                                history_70 = history_70_default;
                            }
                        }
                        else{
                            history_70 = history_70_default;

                        }


    //                    if (!setting_1.get("name_2").equals("None")){
    //                        try{
    //                            history_70= findBestHistory(period,
    //                                                        periods,
    //                                                        indicator_id, 
    //                                                        ticker, 
    //                                                        prices_70, 
    //                                                        settings, 
    //                                                        IndicatorType, 
    //                                                        channel, 
    //                                                        setting_dynamic,
    //                                                        false);
    //                        }
    //                        catch (Exception ex){
    //                            continue;
    //                        }
    //                    }
    //                    else {
    //                        history_70.put("history", (float)settings.getInterval());
    //                        float acc_70 = findAccuracy(
    //                                                    indicator_id, 
    //                                                    ticker,
    //                                                    history_70.get("history").intValue(), 
    //                                                    prices_30, 
    //                                                    settings, 
    //                                                    IndicatorType, 
    //                                                    setting_dynamic,
    //                                                    false
    //                                                    );
    //                        history_70.put("acc_max", acc_70);
    //                    }
                        if (history_70.get("history").intValue() == -1)
                            continue;
                        indicator = IndicatorType.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class, HashMap.class})
                                                           .newInstance(new Object[]{indicator_id, ticker, settings, prices, setting_dynamic});
                        start = (int)(prices.size() * 0.7) - indicator.getCutEdge();
                        start = (start > 0 ) ? start: 0;
                        prices_30 = prices.subList(start, prices.size());

                        List<Float> acc_30 = findAccuracy( 
                                                        indicator_id, 
                                                        ticker,
                                                        history_70.get("history").intValue(), 
                                                        prices_30, 
                                                        settings, 
                                                        IndicatorType, 
                                                        setting_dynamic,
                                                        false);
                        List<Float> acc = findAccuracy( 
                                                    indicator_id, 
                                                    ticker,
                                                    history_70.get("history").intValue(), 
                                                    prices, 
                                                    settings, 
                                                    IndicatorType, 
                                                    setting_dynamic,
                                                    false);
                        String sql = "INSERT IGNORE INTO " + table_name + " (`period`, `" + setting_1.get("name_1") + "`";
                        if (!setting_1.get("name_2").equals("None")){
                            sql +=  ", `" + setting_1.get("name_2") + "`";
                        }
                        if (!setting_1.get("name_3").equals("None")){
                            sql += ", `" + setting_1.get("name_3") + "`";
                        }
                        if (!setting_1.get("name_4").equals("None")){
                            sql += ", `" + setting_1.get("name_4") + "`";
                        }
                        sql += ", `history`, `accuracy_100_dynamic`, `accuracy_100_const`, `accuracy_70_dynamic`, `accuracy_70_const`,";
                        sql += "`accuracy_30_dynamic`, `red`, `redSuccess`, `green`, `greenSuccess`, `accuracy_30_const`)";
                        sql += " VALUES (\'" + periods[period] + "\', " + z;
                        if (!setting_1.get("name_2").equals("None")){
                            sql +=  ", " + y;
                        }
                        if (!setting_1.get("name_3").equals("None")){
                            sql += ", " + x;
                        }
                        if (!setting_1.get("name_4").equals("None")){
                            sql += ", " + k;
                        }
                        sql += ", " + history_70.get("history").intValue() + ", " + acc.get(0) + ", " +    acc_default.get(0);
                        sql += ", "+ history_70.get("acc_max") + ", ";
                        sql += history_70_default.get("acc_max") +", " + acc_30.get(0) + ", " + acc_30.get(1) + ", ";
                        sql += acc_30.get(2) + ", " + acc_30.get(3) + ", " + acc_30.get(4) + ", " + acc_30_default.get(0) + ")";
                        System.out.println(sql);
                        channel.basicPublish("", QUEUE_BATCH, null, sql.getBytes());
                        }
                }
            }
 	}
    }
    
    private static <T extends Indicator> List<Float> findAccuracy(  
                                                                 long indicator_id, 
                                                                  String ticker,int history, 
                                                                  List<Price> prices,
                                                                  Settings  settings,
                                                                  Class<T> type,
                                                                  HashMap<String, Integer> settings_map,
                                                                  boolean is_default
                                                                  ) throws InstantiationException, 
                                                                     IllegalAccessException, 
                                                                     IllegalArgumentException, 
                                                                     InvocationTargetException, 
                                                                     NoSuchMethodException, 
                                                                     SecurityException
    {
        settings.setInterval(history);
        T indicator;
        if (is_default){
            indicator = type.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class})
                                                       .newInstance(new Object[]{indicator_id, ticker, settings, prices});

        }
        else{
             indicator = type.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class, HashMap.class})
                                                       .newInstance(new Object[]{indicator_id, ticker, settings, prices, settings_map});
        }        
        indicator.calculate();
        List<Price> pprices = indicator.getPrices();
        TreeMap<Date, Double> pprs = new TreeMap();
        TreeMap<Date, Float> grades = new TreeMap();
        for (Price p: pprices)
        {
            pprs.put(p.getDate(), p.getAdjClose());
            grades.put(p.getDate(), p.getGrade());
        }
        List<Float> acc = _CountAccuracy(grades, pprs);
        // check for NaN
        if ((acc.get(0)).equals(Float.NaN)) {
            acc.set(0, (float)-1);
        }
        return acc;
     }
  		
  private static <T extends Indicator> HashMap<String, Float> findBestHistory(int period,
                                                                              int[] periods,
                                                                              long indicator_id, 
                                                                              String ticker, 
                                                                              List<Price> prices,
                                                                              Settings  settings,
                                                                              Class<T> type,
                                                                              Channel channel,
                                                                              HashMap<String,Integer> aroon,
                                                                              boolean is_default
                                                                              ) throws NoSuchMethodException, 
                                                                                       InstantiationException, 
                                                                                       IllegalAccessException, 
                                                                                       IllegalArgumentException, 
                                                                                       InvocationTargetException, 
                                                                                       IOException 
  {
        ArrayList<Integer> lst = getHistoryByPeriod(periods[period]);
        float step = (float) (lst.get(1) - lst.get(0)) / 100;
        HashMap<String,Float> history = new HashMap<String,Float>();
        float acc_max = 0;
        int _index = -1;
        T indicator;
//              System.out.println(type.getConstructors()[0]);
        if (is_default){
            indicator = type.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class})
                                                        .newInstance(new Object[]{indicator_id, ticker, settings, prices});

        }
        else{
            indicator = type.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class, HashMap.class})
                    .newInstance(new Object[]{indicator_id, ticker, settings, prices, aroon});
        }
        List<Price> calculated_prices = indicator.calculatePrices();
        for (int j=lst.get(0); j <= lst.get(1); j++)
        {
            settings.setInterval(j);
            indicator.calculateFinal(calculated_prices);
            List<Price> pprices = indicator.getPrices();
            TreeMap<Date, Double> pprs = new TreeMap();
            TreeMap<Date, Float> grades = new TreeMap();
            for (Price p: pprices)
            {
                pprs.put(p.getDate(), p.getAdjClose());
                grades.put(p.getDate(), p.getGrade());
            }
            float acc = _CountAccuracy(grades, pprs).get(0); 

            if (acc > acc_max){
                acc_max = acc;
                _index = j;
            }
            if (acc_max != acc_max){
                acc_max = -1;
            }
            history.put("acc_max",acc_max);
            history.put("history",(float) _index);

        }        
        return history;
  //              String sql = "INSERT INTO `technicalanalysis_own_settings` (`interval`, period, accuracy,";
  //              sql += "ticker, indicator_id, technical)" ;
  //              sql += "VALUES (" + _index + ", \'" + periods[period] + "\', " + acc_max + ", \"";
  //              sql += ticker + "\", " + indicator_id + ", \'" + st.getTechnical() + "\')";
  //              
  //              System.out.println(sql);
                //channel.basicPublish("", QUEUE_BATCH, null, sql.getBytes());
  }

  private static <T extends Indicator> void  dumpValue(int period, 
                                                        long indicator_id, 
                                                        String ticker, 
                                                        ArrayList<Price> prices,
                                                        HashMap<Long, HashMap<Integer, Settings>>  settings,
                                                        int[] periods,
                                                        Class<T> type,
                                                        Channel channel
                                                        ) throws NoSuchMethodException, 
                                                                 InstantiationException, 
                                                                 IllegalAccessException, 
                                                                 IllegalArgumentException, 
                                                                 InvocationTargetException, 
                                                                 IOException 
    {
        Settings st = settings.get(indicator_id).get(periods[period]);
        T indicator;
//              System.out.println(type.getConstructors()[0]);
        indicator = type.getConstructor(new Class[]{Long.class, String.class, Settings.class, List.class})
                    .newInstance(new Object[]{indicator_id, ticker, st, prices});
        indicator.calculate();
        List<Price> pprices = indicator.getPrices();
        for (Price price : pprices)
        {
            double value = price.getValue();
            double close_price = price.getAdjClose();
            String sql_str = "INSERT INTO `indicator_value` (period, ticker, indicator_id, value, close_price, `date`)";
            sql_str += "VALUES (\'" + periods[period] + "\', \'" + ticker + "\', " + indicator_id;
            sql_str += ", " + value + ", " + close_price + ", " + price.getDate().getTime() + ")";
            channel.basicPublish("", QUEUE_BATCH, null, sql_str.getBytes());
        } 
    }
}
