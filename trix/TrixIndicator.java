package technicalanalysis.trix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class TrixIndicator extends Indicator{
    int daysEma;
    int trixEma;
    public TrixIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new TrixPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        daysEma = (int)((long)settings.getTechnical().get("ema"));
        trixEma = (int)((long)settings.getTechnical().get("trix_ema"));
    }
    
    public TrixIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new TrixPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        daysEma = hash.get("ema");
        trixEma = hash.get("trix_ema");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + daysEma*3 + trixEma;
    }
    
    public void calculate(){
        //cutAtBeginning(daysEma*3+trixEma);
        setEMA(daysEma);        
        setExtraEma();
        cutDays(daysEma);
        setDoubleEma();
        cutDays(daysEma);
        setTripleEma();
        cutDays(daysEma);
        setValues();
        cutDays(2);
        setMinMax(indicatorId);
        setDiff();
        cutDays(trixEma);
        setGrades();
        //cutAtEnding();
    }
    
    public List<Price> calculatePrices(){
       //cutAtBeginning(daysEma*3+trixEma);
        setEMA(daysEma);        
        setExtraEma();
        cutDays(daysEma);
        setDoubleEma();
        cutDays(daysEma);
        setTripleEma();
        cutDays(daysEma);
        setValues();
        cutDays(2);
        return this.prices;
    }
    
    public void calculateFinal(List<Price> prices){
        setMinMax(indicatorId);
        setDiff();
        cutDays(trixEma);
        setGrades();
    }

    private void setExtraEma() {
        if(intervalCount+(daysEma*3)+trixEma < prices.size()){
            ((TrixPrice)prices.get(0)).setExtraEma(daysEma, prices.get(intervalCount+(daysEma*2)+trixEma-2).getEma(daysEma));
        }
    }
    
    private void setDoubleEma() {
        double smoothingConstant = (double)2/(daysEma+1);
        int index = prices.size()-1;
        ArrayList<Double> emas = new ArrayList();
        for(int i = index; i > index-daysEma && i>=0; i--){
            emas.add(prices.get(i).getEma(daysEma));            
        }
        
        double doubleEma  = getSum(emas)/daysEma;
        index = index-daysEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()
                    && !Double.isNaN(((TrixPrice)prices.get(0)).getDoubleEma(indicatorId, period))){
                doubleEma = ((TrixPrice)prices.get(0)).getDoubleEma(indicatorId, period);
            }
            
            ((TrixPrice)prices.get(index+1)).setDoubleEma(doubleEma);
            while(index >= 0){          
                double ema = prices.get(index).getEma(daysEma);
                doubleEma = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(ema, doubleEma)), doubleEma);
                
                ((TrixPrice)prices.get(index)).setDoubleEma(doubleEma);
                index--;
            }
            if(intervalCount+(daysEma*2)+trixEma+1 < prices.size()){
                ((TrixPrice)prices.get(0)).setExtraDoubleEma(((TrixPrice)prices.get(intervalCount+daysEma+trixEma-1)).getDoubleEma());
            }
        }
    }

    private void setTripleEma() {
        double smoothingConstant = (double)2/(daysEma+1);
        int index = prices.size()-1;
        ArrayList<Double> doubleEmas = new ArrayList();
        for(int i = index; i > index-daysEma && i>=0; i--){
            doubleEmas.add(((TrixPrice)prices.get(i)).getDoubleEma());
        }
        
        double tripleEma  = getSum(doubleEmas)/daysEma;
        index = index-daysEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()
                    && !Double.isNaN(((TrixPrice)prices.get(0)).getTripleEma(indicatorId, period))){
                tripleEma = ((TrixPrice)prices.get(0)).getTripleEma(indicatorId, period);
            }
            
            ((TrixPrice)prices.get(index+1)).setTripleEma(tripleEma);
            while(index >= 0){
                double doubleEma = ((TrixPrice)prices.get(index)).getDoubleEma();
                tripleEma = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(doubleEma, tripleEma)), tripleEma);
                
                ((TrixPrice)prices.get(index)).setTripleEma(tripleEma);
                index--;
            }
            if(intervalCount+daysEma+trixEma+2 < prices.size()){
                ((TrixPrice)prices.get(0)).setExtraTripleEma(((TrixPrice)prices.get(intervalCount+trixEma)).getTripleEma());
            }
        }
    }

    private void setValues() {
        int index = prices.size()-1;
        if(index > 0){
            double prevTripleEma = ((TrixPrice)prices.get(index)).getTripleEma();
            index--;
            while(index>=0){
                double tripleEma = ((TrixPrice)prices.get(index)).getTripleEma();
                double value = TechnicalAnalysis.subtract(tripleEma, prevTripleEma)/prevTripleEma;
                
                prices.get(index).setValue(value);
                
                prevTripleEma = tripleEma;
                index--;
            }
        }
    }

    private void setDiff() {
        double smoothingConstant = (double)2/(trixEma+1);
        int index = prices.size()-1;
        ArrayList<Double> values = new ArrayList();
        for(int i = index; i > index-trixEma && i>=0; i--){
            values.add(prices.get(i).getValue());
        }
        
        double ema  = getSum(values)/trixEma;
        index = index-trixEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()
                    && !Double.isNaN(((TrixPrice)prices.get(0)).getTrixEma(indicatorId, period))){
                ema = ((TrixPrice)prices.get(0)).getTrixEma(indicatorId, period);
            }
            
            double value = prices.get(index+1).getValue();
            ((TrixPrice)prices.get(index+1)).setTrixEma(ema);
            ((TrixPrice)prices.get(index+1)).setDiff(TechnicalAnalysis.subtract(value, ema));
            while(index >= 0){          
                value = prices.get(index).getValue();
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(value, ema)), ema);
                
                ((TrixPrice)prices.get(index)).setTrixEma(ema);
                ((TrixPrice)prices.get(index)).setDiff(TechnicalAnalysis.subtract(value, ema));
                index--;
            }
            if(intervalCount+trixEma+2 < prices.size()){
                ((TrixPrice)prices.get(0)).setExtraTrixEma(((TrixPrice)prices.get(intervalCount)).getTrixEma());
            }
        }
    }

    private void setGrades() {
        int index = prices.size()-1;
        if(index > 0){
            double prevDiff = ((TrixPrice)prices.get(index)).getDiff();
            index--;
            while(index >= 0){
                ((TrixPrice)prices.get(index)).setTrixGrade(prevDiff, trixEma);
                prevDiff = ((TrixPrice)prices.get(index)).getDiff();
                index--;
            }
        }
    }
}
