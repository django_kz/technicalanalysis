package technicalanalysis.obv;

import java.util.ArrayList;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class ObvIndicator extends Indicator{
    public ObvIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new ObvPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
    }
    
    public void calculate(){   
        cutAtBeginning(1);
        setObv();        
        cutDays(2);        
        setMinMax(indicatorId);
        prices.forEach(p->{
            ((ObvPrice)p).setObvGrade();
        });
        cutAtEnding();
    }

    private void setObv() {
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();
            double prevObv = 0;
            if(containsGrade() && hasExtra()){
                prevObv = ((ObvPrice)prices.get(index)).getObv(indicatorId, period);
            }
            index--;
            while(index >= 0){
                double adjClose = prices.get(index).getAdjClose();
                double adjVolume = prices.get(index).getAdjVolume()/1000;
                
                int upDown = adjClose>prevAdjClose?1:adjClose<prevAdjClose?-1:0;
                double obv = TechnicalAnalysis.sum(prevObv, (adjVolume*upDown));
                
                ((ObvPrice)prices.get(index)).setObv(obv);
                
                if(prevObv > 0){
                    prices.get(index).setValue((adjVolume*upDown)*100/prevObv);
                }else if(prevObv < 0){
                    prices.get(index).setValue((adjVolume*upDown)*100/(prevObv*-1));
                }else{
                    prices.get(index).setValue(0);
                }
                prevAdjClose = adjClose;
                prevObv = obv;
                index--;
            }
            if(intervalCount+1 < prices.size()){
                ((ObvPrice)prices.get(0)).setExtraObv(((ObvPrice)prices.get(intervalCount)).getObv());
            }
        }
    }        
}
