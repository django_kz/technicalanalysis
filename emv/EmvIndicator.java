package technicalanalysis.emv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class EmvIndicator extends Indicator{
    int emvSma;
    public EmvIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new EmvPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        emvSma = (int)((long)settings.getTechnical().get("emv_sma")); 
    }
    public EmvIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new EmvPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        emvSma = hash.get("emv_sma"); 
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + emvSma + 1;
    }
    
    public void calculate(){
        cutAtBeginning(emvSma+1);
        setEmv();
        cutDays(2);
        setEmvValue();
        cutDays(emvSma);
        setMinMax(indicatorId);
        cutAtEnding();
        prices.forEach(p->{
            ((EmvPrice)p).setEmvGrade(emvSma);
        });
    }

    private void setEmv() {
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjHigh = prices.get(index).getAdjHigh();
            double prevAdjLow = prices.get(index).getAdjLow();

            index--;
            while(index >= 0){
                Price p = prices.get(index);
                double adjHigh = p.getAdjHigh();
                double adjLow = p.getAdjLow();
                if(adjHigh == adjLow){
                    ((EmvPrice)p).setEmv(0.0);
                }else{
                    double avgHighLow = TechnicalAnalysis.sum(adjHigh,adjLow)/2;
                    double prevAvgHighLow = TechnicalAnalysis.sum(prevAdjHigh,prevAdjLow)/2;;
                    double diffHighLow = TechnicalAnalysis.subtract(adjHigh, adjLow);
                    double adjVolume = p.getAdjVolume()/100000000;

                    double emv = TechnicalAnalysis.subtract(avgHighLow, prevAvgHighLow)/(adjVolume/diffHighLow);

                    ((EmvPrice)p).setEmv(emv);
                }

                prevAdjHigh = adjHigh;
                prevAdjLow = adjLow;
                index--;
            }
        }
    }

    private void setEmvValue() {
        int index = prices.size()-1;
        ArrayList<Double> emv = new ArrayList();
        double sum = 0.0;
        for(int i = index; i > index-emvSma+1 && index >=0; i--){
            EmvPrice p = (EmvPrice) prices.get(i);
            emv.add(p.getEmv());
            sum = TechnicalAnalysis.sum(sum, p.getEmv());
        }
        
        index = index-emvSma+1;
        while(index >= 0){
            EmvPrice p = (EmvPrice) prices.get(index);
            emv.add(p.getEmv());
            sum = TechnicalAnalysis.sum(sum, p.getEmv());
            
            p.setValue(sum/emvSma);
            
            sum = TechnicalAnalysis.subtract(sum, emv.get(0));
            emv.remove(0);
            index--;
        }
    }
    
    
}
