package technicalanalysis.kst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class KstIndicator extends Indicator{
    TreeMap<Integer, Integer> rocSettings = new TreeMap();
    int kstSma;
    int maxRoc = 0;
    int maxSma = 0;
    
    public KstIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new KstPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        JSONArray rocArray = (JSONArray) settings.getTechnical().get("roc");
        
        rocArray.forEach((o) -> {
            JSONObject obj = (JSONObject) o;
            int roc = (int) ((long)obj.get("R"));
            int sma = (int) ((long)obj.get("S"));
            maxRoc = maxRoc<roc?roc:maxRoc;
            maxSma = maxSma<sma?sma:maxSma;
            rocSettings.put(roc, sma);
        });
        kstSma = (int)((long)settings.getTechnical().get("kst_sma"));
    }
    public KstIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new KstPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        rocSettings.put(hash.get("roc1"), hash.get("roc1"));
        rocSettings.put(hash.get("roc2"), hash.get("roc1"));
        rocSettings.put(hash.get("roc3"), hash.get("roc2"));
        maxRoc = hash.get("roc3");
        maxSma = hash.get("roc2");
        
        kstSma = hash.get("kst_sma");
    }
    
    public void calculate(){
        //cutAtBeginning(maxRoc+maxSma+kstSma);
        rocSettings.keySet().forEach(this::setRoc);
        cutDays(maxRoc+2);
        rocSettings.forEach(this::setRocSma);
        cutDays(maxSma);
        setKst();
        setMinMax(indicatorId);
        setKstSmaDiff();
        cutDays(kstSma);
        setKstGrades();
        //cutAtEnding();
    }
 
    private void setRoc(int rocDays){
        int fIndex = prices.size() - 1- rocDays;
        int lIndex = prices.size()- 1;
        while(fIndex>=0){
            double fAdjClose = prices.get(fIndex).getAdjClose();
            double lAdjClose = prices.get(lIndex).getAdjClose();

            double roc = TechnicalAnalysis.subtract(fAdjClose, lAdjClose)/lAdjClose*100;

            ((KstPrice)prices.get(fIndex)).setRocSma(rocDays, roc, 'R');
            fIndex--;
            lIndex--;
        }
    }
    
    private void setRocSma(int rocDays, int smaDays){
        int index = prices.size()-1;
        ArrayList<Double> rocs = new ArrayList();
        double sum = 0.0;
        for(int i = index; i > index-smaDays+1 && i>=0; i--){
            rocs.add(((KstPrice)prices.get(i)).getRocSma(rocDays, 'R'));
            sum = TechnicalAnalysis.sum(sum, ((KstPrice)prices.get(i)).getRocSma(rocDays, 'R'));
        }
        
        
        index = index - smaDays + 1;
        while(index >= 0){
            rocs.add(((KstPrice)prices.get(index)).getRocSma(rocDays, 'R'));
            sum = TechnicalAnalysis.sum(sum, ((KstPrice)prices.get(index)).getRocSma(rocDays, 'R'));
            
            double sma = sum/smaDays;
            ((KstPrice)prices.get(index)).setRocSma(rocDays, sma, 'S');
            
            sum = TechnicalAnalysis.subtract(sum, rocs.get(0));
            rocs.remove(0);
            index--;
        }
    }

    private void setKst() {
        prices.forEach((p)->{
            double kst = 0.0;
            int coef = 1;
            for(int key:rocSettings.keySet()){
                kst = TechnicalAnalysis.sum(kst, coef++*((KstPrice)p).getRocSma(key, 'S'));
            }
            p.setValue(kst);
        });
    }
    
    private void setKstSmaDiff(){
        int index = prices.size()-1;
        ArrayList<Double> kst = new ArrayList();
        for(int i = index; i > index-kstSma+1 && i>=0; i--){
            kst.add(prices.get(i).getValue());            
        }                 
        double sum = getSum(kst);
        
        index = index-kstSma+1;
        while(index >= 0){
            kst.add(prices.get(index).getValue());
            sum = TechnicalAnalysis.sum(sum, prices.get(index).getValue());            
            
            ((KstPrice)prices.get(index)).setKstSma(sum/kstSma);
            ((KstPrice)prices.get(index)).setDiff();                                      
            
            sum = TechnicalAnalysis.subtract(sum, kst.get(0));
            kst.remove(0);
            index--;
        }
    }

    private void setKstGrades() {
        int index = prices.size()-1;
        if(index > 0){
            double prevDiff = ((KstPrice)prices.get(index)).getDiff();
            index--;
            while(index >= 0){
                ((KstPrice)prices.get(index)).setKstGrade(prevDiff, kstSma);
                
                prevDiff = ((KstPrice)prices.get(index)).getDiff();
                index--;
            }
        }
    }
}
