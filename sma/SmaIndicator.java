/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package technicalanalysis.sma;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;

/**
 *
 * @author tt
 */
public class SmaIndicator extends Indicator{
    
    int sma;
    
    public SmaIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices){
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new SmaPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        sma = (int)((long)settings.getTechnical().get("sma"));
    }
    
       public SmaIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash){
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new SmaPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        sma = hash.get("sma");
    }
       
    public int getCutEdge(){
        return super.getCutEdge() + sma ;
    }

    public void calculate(){
        cutAtBeginning(sma);
        setSMA(sma);
        setMinMax(indicatorId);
        cutDays(sma);
        cutAtEnding();
        for(int i=0;i<prices.size();i++){
            Price price = prices.get(i);
            ((SmaPrice)price).setMovingAverageGrade(sma);            
        }
    }
}
