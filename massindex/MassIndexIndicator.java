package technicalanalysis.massindex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class MassIndexIndicator extends Indicator{
    int highLowEma;
    int emaOfEma;
    int massIndexDays;
    int sma;
    
    public MassIndexIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new MassIndexPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
                
        highLowEma = (int)((long)settings.getTechnical().get("high_low_ema"));
        emaOfEma = (int)((long)settings.getTechnical().get("ema_of_ema"));
        massIndexDays = (int)((long)settings.getTechnical().get("mass_index"));
        sma = (int)((long)settings.getTechnical().get("sma"));
    }
    
    public MassIndexIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new MassIndexPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
                
        highLowEma = hash.get("high_low_ema");
        emaOfEma = highLowEma;
        massIndexDays = hash.get("mass_index");
        sma = hash.get("sma");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + Math.max(highLowEma+emaOfEma+massIndexDays-2, sma+1);
    }
    
    public void calculate(){
//        cutAtBeginning(Math.max(highLowEma+emaOfEma+massIndexDays-2, sma+1));
        setSMA(sma);
        setHighLowEma();
        cutDays(highLowEma);
        setRatioOfEmas();
        cutDays(emaOfEma);
        setMassIndex();
        cutDays(Math.max(massIndexDays, sma-highLowEma-emaOfEma+2));
        setValues();
        setMinMax(indicatorId);
        setGrades();
        cutDays(2);
//        cutAtEnding();
    }
    
    public List<Price> calculatePrices(){
//        cutAtBeginning(Math.max(highLowEma+emaOfEma+massIndexDays-2, sma+1));
        setSMA(sma);
        setHighLowEma();
        cutDays(highLowEma);
        setRatioOfEmas();
        cutDays(emaOfEma);
        setMassIndex();
        cutDays(Math.max(massIndexDays, sma-highLowEma-emaOfEma+2));
        setValues();
        return this.prices;
//        cutAtEnding();
    }
    
    public void calculateFinal(List<Price> prices){
        this.prices = prices;
        setMinMax(indicatorId);
        setGrades();
        cutDays(2);
    }

    private void setHighLowEma() {
        double smoothingConstant = (double)2/(highLowEma+1);
        int index = prices.size()-1;
        ArrayList<Double> diffs = new ArrayList();
        for(int i = index; i > index-highLowEma && i>=0; i--){
            double diff = TechnicalAnalysis
                    .subtract(prices.get(i).getAdjHigh(), prices.get(i).getAdjLow());
            diffs.add(diff);
        }
        
        double ema  = getSum(diffs)/highLowEma;
        index = index-highLowEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()){
                ema = ((MassIndexPrice)prices.get(index)).getHighLowEma(indicatorId, period);
            }
            
            ((MassIndexPrice)prices.get(index+1)).setHighLowEma(ema);
            while(index >= 0){          
                double diff = TechnicalAnalysis
                    .subtract(prices.get(index).getAdjHigh(), prices.get(index).getAdjLow());
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(diff, ema)), ema);
                
                ((MassIndexPrice)prices.get(index)).setHighLowEma(ema);
                index--;
            }
            if(intervalCount+Math.max(massIndexDays, sma-highLowEma-emaOfEma+2)+emaOfEma-1 < prices.size()-highLowEma){
                ((MassIndexPrice)prices.get(0)).setExtraHighLowEma(((MassIndexPrice)
                        prices.get(intervalCount+Math.max(massIndexDays, sma-highLowEma-emaOfEma+2)+emaOfEma-1)).getHighLowEma());
            }
        }
    }

    private void setRatioOfEmas() {
        double smoothingConstant = (double)2/(emaOfEma+1);
        int index = prices.size()-1;
        ArrayList<Double> highLowEmas = new ArrayList();
        for(int i = index; i > index-emaOfEma && i>=0; i--){
            double hlEma = ((MassIndexPrice)prices.get(i)).getHighLowEma();
            highLowEmas.add(hlEma);
        }
        
        double ema  = getSum(highLowEmas)/emaOfEma;
        index = index-emaOfEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()){
                ema = ((MassIndexPrice)prices.get(index)).getEmaOfEma(indicatorId, period);
            }
            ((MassIndexPrice)prices.get(index+1)).setEmaOfEma(ema);
            ((MassIndexPrice)prices.get(index+1)).setRatioOfEmas();
            while(index >= 0){          
                double hlEma = ((MassIndexPrice)prices.get(index)).getHighLowEma();
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(hlEma, ema)), ema);
                
                ((MassIndexPrice)prices.get(index)).setEmaOfEma(ema);
                ((MassIndexPrice)prices.get(index)).setRatioOfEmas();
                index--;
            }
            if(intervalCount+Math.max(massIndexDays, sma-highLowEma-emaOfEma+2) < prices.size()-emaOfEma){
                ((MassIndexPrice)prices.get(0)).setExtraEmaOfEma(((MassIndexPrice)
                        prices.get(Math.max(massIndexDays, sma-highLowEma-emaOfEma+2)+intervalCount)).getEmaOfEma());
            }
        }
    }

    private void setMassIndex() {
        int index = prices.size()-1;
        ArrayList<Double> ratios = new ArrayList();
        for(int i = index; i > index-massIndexDays+1 && i>=0; i--){
            ratios.add(((MassIndexPrice)prices.get(i)).getRatioOfEmas());
        }
        double sum = getSum(ratios);
        index = index-massIndexDays+1;
        while(index >= 0){
            double ratio = ((MassIndexPrice)prices.get(index)).getRatioOfEmas();
            ratios.add(ratio);
            sum = TechnicalAnalysis.sum(sum, ratio);
            
            ((MassIndexPrice)prices.get(index)).setMassIndex(sum);
            
            sum = TechnicalAnalysis.subtract(sum, ratios.get(0));
            ratios.remove(0);
            index--;
        }
    }
    
    private void setValues(){
        int index = prices.size()-1;
        while(index >= 0){
            double curSma = prices.get(index).getSma(this.sma);
            double close = prices.get(index).getAdjClose();
            int trend = close > curSma?-1:1;                                    // -1 Uptrend 1 DownTrend
            double value = ((MassIndexPrice)prices.get(index)).getMassIndex()*trend;

            prices.get(index).setValue(value);

            index--;
        }
    }
    
    private void setGrades(){
        int index = prices.size()-1;
        if(index > 0){
            double prevValue = prices.get(index).getValue();
            index--;
            while(index >= 0){
                ((MassIndexPrice)prices.get(index)).setMassIndexGrade(prevValue);
                prevValue = prices.get(index).getValue();
                index--;
            }
        }
    }
}
