package technicalanalysis.bollinger;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class BollingerPrice extends Price{
    private double standardDeviation;
    private double upperBand;
    private double lowerBand;
    
    public BollingerPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(double standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public double getUpperBand() {
        return upperBand;
    }

    public void setUpperBand(double upperBand) {
        this.upperBand = upperBand;
    }

    public double getLowerBand() {
        return lowerBand;
    }

    public void setLowerBand(double lowerBand) {
        this.lowerBand = lowerBand;
    }
    
    public void setBollingerValue(int days){
        double bollingerValue = 0;
        if(this.adjClose > upperBand){
            bollingerValue = TechnicalAnalysis.subtract(adjClose, upperBand);
        }else if(this.adjClose < lowerBand){
            bollingerValue = TechnicalAnalysis.subtract(adjClose, lowerBand);
        }
        setValue(bollingerValue);
        
        //extra
        JSONObject extraJSON = extra;
        extraJSON.put("top", new JSONObject());
        ((JSONObject)extraJSON.get("top")).put("Lower", lowerBand);
        ((JSONObject)extraJSON.get("top")).put("Middle", getSma(days));
        ((JSONObject)extraJSON.get("top")).put("Upper", upperBand);
        
        extraJSON.put("range", new JSONObject());
        ((JSONObject)extraJSON.get("range")).put("grade4", upperBand);
        ((JSONObject)extraJSON.get("range")).put("grade6", lowerBand);
        setExtra(extraJSON);
    }
    
    public void setBollingerGrade(int days, int period) {                   
        float bGrade = -1;
        if(adjClose >= lowerBand && adjClose <= upperBand){
            bGrade = (float) (10-(4+(TechnicalAnalysis.subtract(adjClose, lowerBand)/(2*standardDeviation))));
        }else if(adjClose > upperBand){
            if(this.max == 0){
                bGrade = 4f;
            }else{
                bGrade = (float) Math.min(Math.max(4-TechnicalAnalysis.subtract(adjClose, upperBand)/(0.25*Math.abs(this.max)), 0), 10);
            }
        }else{
            if(this.min == 0){
                bGrade = 6f;
            }else{
                bGrade = (float) Math.max(Math.min(6+TechnicalAnalysis.subtract(lowerBand, adjClose)/(0.25*Math.abs(this.min)), 10), 0);
            }
        }
        
        double tempMax = max;
        setAvg(getSma(days));
        setMax(TechnicalAnalysis.sum(lowerBand, min));
        setMin(TechnicalAnalysis.sum(upperBand, tempMax));
        
        this.setValue(this.adjClose);
        this.setGrade(bGrade);
    }
}
