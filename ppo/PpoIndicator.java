package technicalanalysis.ppo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.json.simple.JSONArray;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class PpoIndicator extends Indicator{
    ArrayList<Integer> ema = new ArrayList();
    int ppoEma;
    public PpoIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new PpoPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("ema");
        
        jsonArray.forEach((o) -> {            
            ema.add((int)((long)o));
        });
        Collections.sort(ema);
        ppoEma = (int)((long)settings.getTechnical().get("ppo_ema"));
    }
    public PpoIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new PpoPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        ema.add(hash.get("ema_first"));
        ema.add(hash.get("ema_second"));
        Collections.sort(ema);
        ppoEma = hash.get("ppo_ema");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + ema.get(ema.size()-1)+ppoEma;
    }
     
    public void calculate(){
        //cutAtBeginning(ema.get(ema.size()-1)+ppoEma);
        ema.forEach(this::setEMA);
        setExtraEma();
        cutDays(ema.get(ema.size()-1));
        setValues();
        setMinMax(indicatorId);
        setDiffs();
        cutDays(ppoEma);
        setGrades();
        cutDays(2);
        //cutAtEnding();
    }
    
    private void setExtraEma() {
        if(intervalCount+ppoEma+ema.get(ema.size()-1) < prices.size()){
            int maxEma = ema.get(ema.size()-1);
            ema.forEach(e->{
                ((PpoPrice)prices.get(0)).setExtraEma(e, prices.get(intervalCount+ppoEma+maxEma-e).getEma(e));
            });
        }
    }
    
    private void setValues(){
        prices.forEach(p->{
            double value = TechnicalAnalysis
                    .subtract(p.getEma(ema.get(0)), p.getEma(ema.get(1)));
            p.setValue(value*100/p.getEma(ema.get(1)));
        });
    }
    
    private void setDiffs(){
        double smoothingConstant = (double)2/(ppoEma+1);
        int index = prices.size()-1;
        ArrayList<Double> values = new ArrayList();
        for(int i = index; i > index-ppoEma && i>=0; i--){
            double value = prices.get(i).getValue();
            values.add(value);
        }
        
        double ema  = getSum(values)/ppoEma;
        index = index-ppoEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()){
                ema = ((PpoPrice)prices.get(index)).getPpoEma(indicatorId, period);
            }
            
            ((PpoPrice)prices.get(index+1)).setPpoEma(ema);
            ((PpoPrice)prices.get(index+1))
                    .setDiff(TechnicalAnalysis.subtract(ema, prices.get(index+1).getValue()));
            while(index >= 0){          
                double value = prices.get(index).getValue();
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(value, ema)), ema);
                
                ((PpoPrice)prices.get(index)).setPpoEma(ema);
                ((PpoPrice)prices.get(index))
                    .setDiff(TechnicalAnalysis.subtract(prices.get(index).getValue(), ema));
                index--;
            }
            if(intervalCount+ppoEma+1 < prices.size()){
                ((PpoPrice)prices.get(0)).setExtraPpoEma(((PpoPrice)prices.get(intervalCount+1)).getPpoEma());
            }
        }
        
    }

    private void setGrades() {
        int index = prices.size()-1;
        if(index > 0){
            double prevDiff = ((PpoPrice)prices.get(index)).getDiff();
            index--;
            while(index>=0){
                ((PpoPrice)prices.get(index)).setPpoGrade(prevDiff, ppoEma);
                prevDiff = ((PpoPrice)prices.get(index)).getDiff();
                index--;
            }
        }
    }
}
