package technicalanalysis.adx;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class AdxPrice extends Price{
    private double trueRange;
    
    private double dmOnePlus;
    private double dmOneMinus;
    
    private double trInDays;
    private double dmPlus;
    private double dmMinus;
    
    private double dlPlus;
    private double dlMinus;
    private double dx;
    
    private double adxEma;
    
    public AdxPrice(Date date, double adjClose, double adjHigh, double adjLow, double adjVolume, 
            long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getTrueRange() {
        return trueRange;
    }

    public void setTrueRange(double trueRange) {
        this.trueRange = trueRange;
    }

    public double getDmOnePlus() {
        return dmOnePlus;
    }

    public void setDmOnePlus(double dmOnePlus) {
        this.dmOnePlus = dmOnePlus;
    }

    public double getDmOneMinus() {
        return dmOneMinus;
    }

    public void setDmOneMinus(double dmOneMinus) {
        this.dmOneMinus = dmOneMinus;
    }

    public double getTrInDays() {
        return trInDays;
    }

    public void setTrInDays(double trInDays) {
        this.trInDays = trInDays;
    }

    public double getDmPlus() {
        return dmPlus;
    }

    public void setDmPlus(double dmPlus) {
        this.dmPlus = dmPlus;
    }

    public double getDmMinus() {
        return dmMinus;
    }

    public void setDmMinus(double dmMinus) {
        this.dmMinus = dmMinus;
    }

    public double getDlPlus() {
        return dlPlus;
    }

    public void setDlPlus(double dlPlus) {
        this.dlPlus = dlPlus;
    }

    public double getDlMinus() {
        return dlMinus;
    }

    public void setDlMinus(double dlMinus) {
        this.dlMinus = dlMinus;
    }

    public double getDx() {
        return dx;
    }

    public void setDx(double dx) {
        this.dx = dx;
    }

    public double getAdxEma() {
        return adxEma;
    }

    public void setAdxEma(double adxEma) {
        this.adxEma = adxEma;
    }

    public void setAdxValue(){
        if(dlMinus > dlPlus){
            setValue(adxEma*-1);
        }else{            
            setValue(adxEma);
        }

        //extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("ADX", getValue());
        setExtra(extraJSON);
    }
    
    public void setAdxGrade(){
        if(value >=-25 && value <= 25){
            setGrade((float)(4+Math.abs(TechnicalAnalysis.subtract(value, -25)/25)));
        }else if(value < -25){
            double step = TechnicalAnalysis.subtract(-25, min)/4;
            setGrade((float)(TechnicalAnalysis.subtract(value, min)/step));
        }else if(value > 25){
            double step = TechnicalAnalysis.subtract(max, 25)/4;
            setGrade((float)(6+TechnicalAnalysis.subtract(value, 25)/step));
        }
    }

    public double getAdxEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("adx_ema");
    }
    
    public void setExtraAdxEma(double adxEma){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("adx_ema", adxEma);
        setExtra(extraJSON);
    }

    double getTrInDays(long indicatorId, int period) {
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("trindays");
    }

    double getDmPlus(long indicatorId, int period) {
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("dmplus");
    }

    double getDmMinus(long indicatorId, int period) {
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("dmminus");
    }

    void setExtraDmValues(double trInDays, double dmPlus, double dmMinus) {
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("trindays", trInDays);
        ((JSONObject)extraJSON.get("calculation")).put("dmplus", dmPlus);
        ((JSONObject)extraJSON.get("calculation")).put("dmminus", dmMinus);
        setExtra(extraJSON);
    }
}
