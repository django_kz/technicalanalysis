package technicalanalysis.massindex;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class MassIndexPrice extends Price{
    private double highLowEma;
    private double emaOfEma;
    private double ratioOfEmas;
    private double massIndex;
    
    public MassIndexPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);        
    }

    public double getHighLowEma() {
        return highLowEma;
    }

    public void setHighLowEma(double highLowEma) {
        this.highLowEma = highLowEma;
    }
    
    public double getHighLowEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("high_low_ema");
    }

    public void setExtraHighLowEma(double ema){
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("high_low_ema", ema);
        setExtra(extraJSON);
    }
    
    public double getEmaOfEma() {
        return emaOfEma;
    }

    public double getEmaOfEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ema_of_ema");
    }
    
    public void setEmaOfEma(double emaOfEma) {
        this.emaOfEma = emaOfEma;
    }

    public void setExtraEmaOfEma(double emaOfEma){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("ema_of_ema", emaOfEma);
        setExtra(extraJSON);
    }
    
    public double getRatioOfEmas() {
        return ratioOfEmas;
    }

    public void setRatioOfEmas() {
        this.ratioOfEmas = this.highLowEma/this.emaOfEma;
    }

    public double getMassIndex() {
        return massIndex;
    }

    public void setMassIndex(double massIndex) {
        this.massIndex = massIndex;
    }
    
    public void setMassIndexGrade(double prevValue){
        float grade = -1;
        JSONObject extraJSON = extra;
        if(prevValue < -27 && getValue() >= -26.5 && getValue() < 0){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "Mass Index moves below -27 "
                    + "and holds for three (two days, one day) and reverses back "
                    + "above -26.50, the grade would be 0 (1 and 2 respectively).");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            grade = 0;
        }else if(prevValue > 27 && getValue() <= 26.5 && getValue() > 0){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "Mass Index moves above 27 and holds for three "
                    + "(two days, one day) and reverses back below 26.50, the grade "
                    + "would be 10 (9 and 8 respectively).");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            grade = 10;
        }else if(getValue() >= -27 && getValue() <= 27){
            grade = (float) (4+TechnicalAnalysis.subtract(getValue(), -27)/27);
        }else if(getValue() < -27){
            double step = TechnicalAnalysis.subtract(-27, min)/4;
            grade = (float) (TechnicalAnalysis.subtract(getValue(), min)/step);
        }else if(getValue() > 27){
            double step = TechnicalAnalysis.subtract(max, 27)/4;
            grade = (float) (6+TechnicalAnalysis.subtract(getValue(), 27)/step);            
        }
        
        setGrade(grade);

        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Mass Index", getValue());
        setExtra(extraJSON);
    }    
}
