package technicalanalysis.forceindex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class ForceIndexIndicator extends Indicator{
    int forceIndex;
    public ForceIndexIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new ForceIndexPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        forceIndex = (int)((long)settings.getTechnical().get("force_index"));
    }
    public ForceIndexIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new ForceIndexPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        forceIndex = hash.get("force_index");
    }

    public int getCutEdge(){
        return super.getCutEdge() + forceIndex + 1;
    }
    
    
    public void calculate(){
        cutAtBeginning(forceIndex+1);
        setForceIndex();
        cutDays(2);        
        setValues();        
        cutDays(forceIndex);
        setMinMax(indicatorId);
        cutAtEnding();
        prices.forEach(p->{
            ((ForceIndexPrice)p).setForceIndexGrade(forceIndex); 
        });
    }
    
    private void setForceIndex(){
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();
            index--;
            while(index >= 0){
                Price p = prices.get(index);
                double adjClose = p.getAdjClose();
                double volume = p.getAdjVolume();       
                double force = TechnicalAnalysis
                        .subtract(adjClose*volume, prevAdjClose*volume);
                ((ForceIndexPrice)p).setForceIndex(force);
                prevAdjClose = adjClose;
                index--;
            }
        }
    }
    
    private void setValues(){
        int index = prices.size()-1;
        double sum = 0.0;
        double smoothingConstant = (double)2/(forceIndex+1);
        for(int i = index; i > index-forceIndex && i>=0; i--){
            sum = TechnicalAnalysis
                    .sum(sum, ((ForceIndexPrice)prices.get(i)).getForceIndex());
        }
        index = index-forceIndex+1;
        
        if(index > 0){
            double value = sum/forceIndex;
            
            if(containsGrade() && hasExtra()){
                value = ((ForceIndexPrice)prices.get(index)).getValue(indicatorId, period);
            }
            
            prices.get(index).setValue(value);
            index--;
            while(index >= 0){
                double force = ((ForceIndexPrice)prices.get(index)).getForceIndex();
                double diff = TechnicalAnalysis.subtract(force, value);
                value = TechnicalAnalysis.sum(smoothingConstant*diff, value);                                
                
                prices.get(index).setValue(value);
                
                index--;
            }
            if(intervalCount+forceIndex < prices.size()){
                ((ForceIndexPrice)prices.get(0)).setExtraValue(prices.get(intervalCount).getValue());
            }
        }
    }
}
