package technicalanalysis.ema;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class EmaPrice extends Price{
    
    public EmaPrice(Date date, double adjClose, double adjHigh, 
            double adjLow, double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        
        this.value = this.adjClose;
    }
    
    public void setMovingAverageValue(int days) {
        double movingAverageValue = 
                TechnicalAnalysis.subtract(adjClose, getEma(days))
                                /getEma(days);
        setValue(movingAverageValue);
    }
    
    public void setMovingAverageGrade(int days) {
        setAvg(adjClose);
        
        if(getEma(days) < getAvg()){
            double step = Math.abs(TechnicalAnalysis.subtract(this.getAvg(),this.getMin())/5);
            this.setGrade((float)(Math.abs((TechnicalAnalysis.subtract(this.getEma(days), this.getMin())))/(step==0?1:step)));
        }else if(getEma(days) > getAvg()){
            double step = Math.abs(TechnicalAnalysis.subtract(this.getMax(),this.getAvg())/5);
            this.setGrade((float)(5 + Math.abs(TechnicalAnalysis.subtract(this.getEma(days),this.getAvg()))/(step==0?1:step)));
        }else{
            this.setGrade(5f);
        }
        
        if(this.getGrade() < 0){
            this.setGrade(0f);
        }else if(this.getGrade() > 10){
            this.setGrade(10f);
        }
        this.setGrade(10-this.getGrade());
        
        double temp = getMax();
        setMax(getMin());
        setMin(temp);
        
        setValue(getEma(days));
        
        JSONObject extraJSON = extra;
        extraJSON.put("top", new JSONObject());
        ((JSONObject)extraJSON.get("top")).put("ema ("+days+")", getValue());
        setExtra(extraJSON);
    }

    void setExtraEma(int days, double ema) {
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("ema ("+days+")", ema);
        setExtra(extraJSON);
    }
}
