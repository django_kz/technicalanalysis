package technicalanalysis;

import org.json.simple.JSONObject;

public class Settings{
    private int period;
    private JSONObject technical;
    private int interval;
    
    public Settings(int period, JSONObject technical, int interval){
        this.period = period;
        this.technical = technical;
        this.interval = interval;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public JSONObject getTechnical() {
        return technical;
    }

    public void setTechnical(JSONObject technical) {
        this.technical = technical;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval() {
        this.interval = interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }        
}
