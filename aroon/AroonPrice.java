package technicalanalysis.aroon;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class AroonPrice extends Price{
    
    public AroonPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    void setAroonGrade() {
        float aroonGrade = -1;
        if(getValue() >= -50 && getValue() <= 0){
            aroonGrade = (float) (TechnicalAnalysis.sum(4, TechnicalAnalysis.subtract(getValue(), -50)/50));
        }else if(getValue() > 0 && getValue() <= 50){
            aroonGrade = (float) (TechnicalAnalysis.sum(5, getValue()/50));
        }else if(getValue() < -50){
            aroonGrade = (float) (TechnicalAnalysis.sum(100, getValue())/12.5);
        }else if(getValue() > 50){
            aroonGrade = (float) TechnicalAnalysis.sum(2, getValue()/12.5);
        }
        
        setGrade(aroonGrade);
        
        //min max
        setMin(-100);
        setMax(100);
        
        //extra
        JSONObject extraJSON = new JSONObject();
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Aroon", getValue());
        setExtra(extraJSON);
    }
    
    
}
