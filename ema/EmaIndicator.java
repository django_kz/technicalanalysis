package technicalanalysis.ema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;

public class EmaIndicator extends Indicator{
    int ema;
    
    public EmaIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new EmaPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
//        ema = (int)((long)settings.getTechnical().get("ema"));
          ema = hash.get("ema");
    }
    
    public EmaIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new EmaPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        ema = (int)((long)settings.getTechnical().get("ema"));
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + ema ;
    }
    
    public void calculate(){
        cutAtBeginning(ema);
        setEMA(ema);
        setExtraEma();
        setMinMax(indicatorId);
        cutDays(ema);
        cutAtEnding();
        for(int i=0;i<prices.size();i++){
            Price price = prices.get(i);
            ((EmaPrice)price).setMovingAverageGrade(ema);            
        }        
    }

    private void setExtraEma() {
        if(intervalCount+ema < prices.size()){
            ((EmaPrice)prices.get(0)).setExtraEma(ema, prices.get(intervalCount).getEma(ema));
        }
    }
}
