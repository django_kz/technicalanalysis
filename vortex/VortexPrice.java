package technicalanalysis.vortex;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class VortexPrice extends Price{
    private double vmPlus;
    private double vmMinus;
    private double trueRange;
    
    private double vmPDays;
    private double vmMDays;
    private double trDays;
    
    private double vlPlus;
    private double vlMinus;
    
    public VortexPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getVmPlus() {
        return vmPlus;
    }

    public void setVmPlus(double vmPlus) {
        this.vmPlus = vmPlus;
    }

    public double getVmMinus() {
        return vmMinus;
    }

    public void setVmMinus(double vmMinus) {
        this.vmMinus = vmMinus;
    }

    public double getTrueRange() {
        return trueRange;
    }

    public void setTrueRange(double trueRange) {
        this.trueRange = trueRange;
    }

    public double getVmPDays() {
        return vmPDays;
    }

    public void setVmPDays(double vmPDays) {
        this.vmPDays = vmPDays;
    }

    public double getVmMDays() {
        return vmMDays;
    }

    public void setVmMDays(double vmMDays) {
        this.vmMDays = vmMDays;
    }

    public double getTrDays() {
        return trDays;
    }

    public void setTrDays(double trDays) {
        this.trDays = trDays;
    }

    public double getVlPlus() {
        return vlPlus;
    }

    public void setVlPlus() {
        this.vlPlus = vmPDays/trDays;
    }

    public double getVlMinus() {
        return vlMinus;
    }

    public void setVlMinus() {
        this.vlMinus = vmMDays/trDays;
    }
    
    public void setVortexValue(){
        this.setValue(TechnicalAnalysis.subtract(vlPlus, vlMinus));
    }
    
    public void setVortexGrade(double prevValue, int smaDays){
        float vortexGrade = -1;
        if(getValue() == 0){
            vortexGrade = 5f;
        }else if(getValue() < 0){
            double step = Math.abs(min/5);
            vortexGrade = (float) 
                    (Math.abs(TechnicalAnalysis.subtract(getValue(), min))/step);
        }else if(getValue() > 0){
            double step = Math.abs(max/5);
            vortexGrade = (float) (5+getValue()/step);
        }
        
        JSONObject extraJSON = extra;
        if(getValue() > 0 && prevValue < 0 && adjClose > getSma(smaDays)){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When price is above 50-day SMA, grade 10 is given if +VI crosses above -VI.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            vortexGrade = 10f;
        }else if(getValue() < 0 && prevValue > 0 && adjClose < getSma(smaDays)){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When price is below 50-day SMA, grade 0 is given if -VI crosses above +VI.");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            vortexGrade = 0f;
        }                
        setGrade(vortexGrade);
        
        // extra
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("VI+", vlPlus);
        ((JSONObject)extraJSON.get("bottom")).put("VI-", vlMinus);
        this.setExtra(extraJSON);
    }
}
