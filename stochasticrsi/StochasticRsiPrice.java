package technicalanalysis.stochasticrsi;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class StochasticRsiPrice extends Price{
    private double avgGain;
    private double avgLoss;
    private double change;
    private double rsiValue;    
    
    public StochasticRsiPrice(Date date, double adjClose, double adjHigh, double adjLow,
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        extra.put("calculation", new JSONObject());
    }
    
    void setExtraAvg(double avgGain, double avgLoss) {
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("avg_gain", avgGain);
        ((JSONObject)extraJSON.get("calculation")).put("avg_loss", avgLoss);
        setExtra(extraJSON);
    }
    
    public double getAvgGain(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("avg_gain");
    }    
    
    public double getAvgLoss(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("avg_loss");
    }   

    public double getAvgGain() {
        return avgGain;
    }

    public void setAvgGain(double avgGain) {
        this.avgGain = avgGain;
    }

    public double getAvgLoss() {
        return avgLoss;
    }

    public void setAvgLoss(double avgLoss) {
        this.avgLoss = avgLoss;
    }
    
    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getRsiValue() {
        return rsiValue;
    }

    public void setRsiValue(double rsiValue) {
        this.rsiValue = rsiValue;
    }
    
    public void setStochRsiGrade(ArrayList<Integer> sma, int stochDays){        
        double trend = getSma(sma.get(0)) > getSma(sma.get(sma.size()-1))?1:
                getSma(sma.get(0)) < getSma(sma.get(sma.size()-1))?-1:0;
        
        JSONObject extraJSON = extra;
        if((trend == 1 && getValue() >= 0.2) || (trend == -1 && getValue() <= 0.8)){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "Oversold conditions trigger only if the bigger "
                    + "trend is up and overbought conditions if the bigger trend is down.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 0);
            this.setGrade(5f);
        }else if(getValue() >= 0.2 && getValue() <= 0.8){
            this.setGrade((float)(4f+TechnicalAnalysis.subtract(0.8, getValue())/0.3));
        }
        else if(getValue() < 0.2){
            this.setGrade((float)(6f + TechnicalAnalysis.subtract(0.2, getValue())/0.05));
        }else if(getValue() > 0.8){
            this.setGrade((float)(TechnicalAnalysis.subtract(1, getValue())/0.05));
        }
        
        // min max
        setMax(0);
        setMin(1);
        setAvg(0.5);
        
        // extra
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Stochastic RSI ("+stochDays+")", getValue());
        setExtra(extraJSON);
    }
}
