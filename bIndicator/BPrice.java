package technicalanalysis.bIndicator;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class BPrice extends Price{
    private double standardDeviation;
    private double upperBand;
    private double lowerBand;
    
    public BPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(double standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public double getUpperBand() {
        return upperBand;
    }

    public void setUpperBand(double upperBand) {
        this.upperBand = upperBand;
    }

    public double getLowerBand() {
        return lowerBand;
    }

    public void setLowerBand(double lowerBand) {
        this.lowerBand = lowerBand;
    }
    
    public void setBValue(int technicalCount){
        double bValue;
        if(upperBand==lowerBand){
            bValue = 0;
        }else{
            bValue = (TechnicalAnalysis.subtract(adjClose, lowerBand))
                    /(TechnicalAnalysis.subtract(upperBand, lowerBand));
        }
        setValue(bValue);
        
        //extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("%B Indicator", bValue);
        setExtra(extraJSON);
    }
    
    public void setBGrade() {
        if(this.getValue() >= 0 
                && this.getValue() <=1){
            this.setGrade((float)(4+(TechnicalAnalysis.subtract(1, this.getValue()))*2));
        }else if(this.getValue() > 1){
            double step = TechnicalAnalysis.subtract(this.getMax(), 1)/4.0;
            if(step == 0){
                this.setGrade(0f);
            }else{
                this.setGrade((float)(TechnicalAnalysis
                    .subtract(this.getMax(), this.getValue())/step));
            }
        }else{
            double step = TechnicalAnalysis.subtract(0, this.getMin())/4.0;
            if(step == 0){
                this.setGrade(10f);
            }else{
                this.setGrade((float)(10-(TechnicalAnalysis
                    .subtract(this.getValue(), this.getMin())/step)));
            }
        }
        
        setAvg(0.5);
        double temp = getMax();
        setMax(getMin());
        setMin(temp);
    }
}
