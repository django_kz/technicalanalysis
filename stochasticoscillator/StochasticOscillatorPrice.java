package technicalanalysis.stochasticoscillator;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class StochasticOscillatorPrice extends Price{
    
    private double smaStochastic;
    private double diff;
    
    public StochasticOscillatorPrice(Date date, double adjClose, double adjHigh,
            double adjLow, double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getSmaStochastic() {
        return smaStochastic;
    }

    public void setSmaStochastic(double smaStochastic) {
        this.smaStochastic = smaStochastic;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff() {
        this.diff = TechnicalAnalysis.subtract(getValue(), getSmaStochastic());
    }
    
    public void setStochasticGrade(double prevDiff, int smaDays){
        float grade = -1;
        if(getValue() >= 20 && getValue() <=80){
            grade = (float)TechnicalAnalysis.sum(4, TechnicalAnalysis.subtract(80, getValue())/30);
        }else if(getValue() < 20){
            grade = (float)TechnicalAnalysis.sum(6, TechnicalAnalysis.subtract(20, getValue())/5);
        }else if(getValue() > 80){
            grade = (float)(TechnicalAnalysis.subtract(100, getValue())/5);
        }
        
        this.setGrade((float)Math.round(grade*100)/100);
        
        JSONObject extraJSON = extra;
        if(prevDiff < 0 && diff > 0 && this.getGrade() > 6){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "Grade 10 if %K is either below 20 and at the same time %K line rised above %D line.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            this.setGrade(10f);
        }else if(prevDiff > 0 && diff < 0 && this.getGrade() < 4){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "Grade 0 if %K is above 80 and at the same time %D line rised above %K line.");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            this.setGrade(0f);
        }
        
        // min max
        setMin(100);
        setMax(0);
        setAvg(50);
        
        // extra
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Stochastic Oscillator(%K)", getValue());
        ((JSONObject)extraJSON.get("bottom")).put("SMA"+smaDays+" (%K)", getSmaStochastic());
        setExtra(extraJSON);
    }
    
}
