package technicalanalysis.kst;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class KstPrice extends Price{
    public HashMap<Integer, HashMap<Character, Double>> rocSma = new HashMap();
    private double kstSma;
    private double diff;
    
    public KstPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }
    
    public Double getRocSma(int index, char type) {
        return rocSma.get(index).get(type);
    }

    public void setRocSma(int index, double value, char type) {
        if(this.rocSma.containsKey(index)){
            this.rocSma.get(index).put(type, value);
        }else{            
            this.rocSma.put(index, new HashMap());
            this.rocSma.get(index).put(type, value);
        }
    }

    public double getKstSma() {
        return kstSma;
    }

    public void setKstSma(double kstSma) {
        this.kstSma = kstSma;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff() {
        this.diff = TechnicalAnalysis.subtract(getValue(), getKstSma());
    }
    
    public void setKstGrade(double prevDiff, int smaDays){
        float kstGrade = -1;
        if(getValue() == 0){
            kstGrade = 5f;
        }else if(getValue() < 0){
            double step = Math.abs(min/5);
            kstGrade = (float) (TechnicalAnalysis.subtract(getValue(), min)/step);
        }else if(getValue() > 0){
            double step = Math.abs(max/5);
            kstGrade = (float) (5 + getValue()/step);
        }
        
        JSONObject extraJSON = extra;
        if(prevDiff < 0 && diff > 0 && kstGrade > 6){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the KST rises above the signal line, we assign Grade 10.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            kstGrade = 10;
        }else if(prevDiff > 0 && diff < 0 && kstGrade < 4){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the KST falls below the signal line, we assign Grade 0.");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            kstGrade = 0;
        }
        
        setGrade(kstGrade);
        
        //extra
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("KST", getValue());
        ((JSONObject)extraJSON.get("bottom")).put("SMA"+smaDays+" (KST)", getKstSma());
        setExtra(extraJSON);
    }
}
