package technicalanalysis.pivot;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class PivotPrice extends Price{
    private double pp;
    private double ppClose;
    private double ppHigh;
    private double ppLow;
    private double diff;
    private double resistance[] = new double[5];
    private double support[] = new double[5];
    
    public PivotPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        
        this.value = this.adjClose;
    }

    public double getPp() {
        return pp;
    }

    public void setPp(double pp) {
        this.pp = pp;
    }   
    
    public void calculatePp(double highestHigh, double lowestLow) {
        this.pp = TechnicalAnalysis.sum(TechnicalAnalysis.sum(highestHigh, lowestLow), adjClose)/3;
    }

    public double getPpClose() {
        return ppClose;
    }

    public void setPpClose(double ppClose) {
        this.ppClose = ppClose;
    }

    public double getPpHigh() {
        return ppHigh;
    }

    public void setPpHigh(double ppHigh) {
        this.ppHigh = ppHigh;
    }

    public double getPpLow() {
        return ppLow;
    }

    public void setPpLow(double ppLow) {
        this.ppLow = ppLow;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }
    
    public void calculatePpWoodie(double highestHigh, double lowestLow) {
        this.pp = TechnicalAnalysis.sum(TechnicalAnalysis.sum(highestHigh, lowestLow), 2*adjClose)/4;
    }

    public double[] getResistance() {
        return resistance;
    }

    public void setResistance(double[] resistance) {
        this.resistance = resistance;
    }

    public double[] getSupport() {
        return support;
    }

    public void setSupport(double[] support) {
        this.support = support;
    }

    public void setPivotGrade(){
        float pivotGrade = -1;
        if(getValue() < support[4]){
            pivotGrade = 0f;
        }else if(getValue() < support[3]){
            double denominator = TechnicalAnalysis.subtract(support[3], support[4]);
            pivotGrade = (float) (TechnicalAnalysis.subtract(getValue(), support[4])/denominator);
        }else if(getValue() < support[2]){
            double denominator = TechnicalAnalysis.subtract(support[2], support[3]);
            pivotGrade = (float) (1+TechnicalAnalysis.subtract(getValue(), support[3])/denominator);
        }else if(getValue() < support[1]){
            double denominator = TechnicalAnalysis.subtract(support[1], support[2]);
            pivotGrade = (float) (2+TechnicalAnalysis.subtract(getValue(), support[2])/denominator);
        }else if(getValue() < support[0]){
            double denominator = TechnicalAnalysis.subtract(support[0], support[1]);
            pivotGrade = (float) (7-TechnicalAnalysis.subtract(getValue(), support[1])/denominator);
        }else if(getValue() < pp){
            double denominator = TechnicalAnalysis.subtract(pp, support[0]);
            pivotGrade = (float) (4+TechnicalAnalysis.subtract(getValue(), support[0])/denominator);
        }else if(getValue() < resistance[0]){
            double denominator = TechnicalAnalysis.subtract(resistance[0], pp);
            pivotGrade = (float) (5+TechnicalAnalysis.subtract(getValue(), pp)/denominator);
        }else if(getValue() < resistance[1]){
            double denominator = TechnicalAnalysis.subtract(resistance[1], resistance[0]);
            pivotGrade = (float) (4-TechnicalAnalysis.subtract(getValue(), resistance[0])/denominator);
        }else if(getValue() < resistance[2]){
            double denominator = TechnicalAnalysis.subtract(resistance[2], resistance[1]);
            pivotGrade = (float) (7+TechnicalAnalysis.subtract(getValue(), resistance[1])/denominator);
        }else if(getValue() < resistance[3]){
            double denominator = TechnicalAnalysis.subtract(resistance[3], resistance[2]);
            pivotGrade = (float) (8+TechnicalAnalysis.subtract(getValue(), resistance[2])/denominator);
        }else if(getValue() < resistance[4]){
            double denominator = TechnicalAnalysis.subtract(resistance[4], resistance[3]);
            pivotGrade = (float) (9+TechnicalAnalysis.subtract(getValue(), resistance[3])/denominator);
        }else{
            pivotGrade = 10f;
        }
        
        setGrade(pivotGrade);
        
        //Min Max
        setMin(resistance[4]);
        setMax(support[4]);
        setAvg(pp);
        
        // Extra
        JSONObject extraJSON = extra;
        extraJSON.put("top", new JSONObject());
//        ((JSONObject)extraJSON.get("top")).put("R5", resistance[4]);
//        ((JSONObject)extraJSON.get("top")).put("R4", resistance[3]);
//        ((JSONObject)extraJSON.get("top")).put("R3", resistance[2]);
        ((JSONObject)extraJSON.get("top")).put("Resistance 2", resistance[1]);
        ((JSONObject)extraJSON.get("top")).put("Resistance 1", resistance[0]);
        ((JSONObject)extraJSON.get("top")).put("PP", pp);
        ((JSONObject)extraJSON.get("top")).put("Support 1", support[0]);
        ((JSONObject)extraJSON.get("top")).put("Support 2", support[1]);
//        ((JSONObject)extraJSON.get("top")).put("S3", support[2]);
//        ((JSONObject)extraJSON.get("top")).put("S4", support[3]);
//        ((JSONObject)extraJSON.get("top")).put("S5", support[4]);


        extraJSON.put("range", new JSONObject());
        ((JSONObject)extraJSON.get("range")).put("grade9", resistance[3]);
        ((JSONObject)extraJSON.get("range")).put("grade8", resistance[2]);
        ((JSONObject)extraJSON.get("range")).put("grade7", resistance[1]);
        ((JSONObject)extraJSON.get("range")).put("grade4", resistance[0]);
        ((JSONObject)extraJSON.get("range")).put("grade6", support[0]);
        ((JSONObject)extraJSON.get("range")).put("grade3", support[1]);
        ((JSONObject)extraJSON.get("range")).put("grade2", support[2]);
        ((JSONObject)extraJSON.get("range")).put("grade1", support[3]);
        setExtra(extraJSON);
    }
}
