/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package technicalanalysis.bIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

/**
 *
 * @author tt
 */
public class BIndicator extends Indicator{
    
    int sma;
    
    public BIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices){
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new BPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;

        sma = (int)((long)settings.getTechnical().get("sma"));        
    } 
    
    public BIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash){
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new BPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;

        sma = hash.get("sma");        
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + sma;
    }
     
    public void calculate(){
        //cutAtBeginning(sma);
        setSMA(sma);
        setBValue();
        cutDays(sma);
        setMinMax(indicatorId);
        cutAtEnding();
        for(int i=0;i<prices.size();i++){
            Price price = prices.get(i);
            ((BPrice)price).setBGrade();
        }
    }

    private void setBValue() {
        int index = prices.size()-1;
        ArrayList<Double> close = new ArrayList();
        for(int i = index; i > index-sma+1 && i>=0; i--){
            close.add(prices.get(i).getAdjClose());            
        }
        
        index = index-sma+1;
        while(index >= 0){
            close.add(prices.get(index).getAdjClose());
            
            ((BPrice)prices.get(index))
                    .setStandardDeviation(standardDeviation(close,prices.get(index).getSma(sma)));
            ((BPrice)prices.get(index))
                    .setUpperBand(TechnicalAnalysis.sum(
                            (prices.get(index)).getSma(sma),
                            ((BPrice)prices.get(index)).getStandardDeviation()*2));
            ((BPrice)prices.get(index))
                    .setLowerBand(TechnicalAnalysis.subtract(
                            (prices.get(index)).getSma(sma),
                            ((BPrice)prices.get(index)).getStandardDeviation()*2));
            
            ((BPrice)prices.get(index)).setBValue(sma);                        
                        
            close.remove(0);
            index--;
        }
    }
    
    private double standardDeviation(ArrayList<Double> close,
                                                        double middleBand) {
        double result = 0;
        for(double a:close){
            double x = TechnicalAnalysis.subtract(a, middleBand);
            result = TechnicalAnalysis.sum(result, x*x);
        }
        
        return Math.sqrt(result/sma);
    }
}
