package technicalanalysis.keltner;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class KeltnerPrice extends Price{
    HashMap<String, Double> keltnerChannels = new HashMap();
    
    
    public KeltnerPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        extra.put("calculation", new JSONObject());
    }
    
    public double getKeltnerEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("keltner_ema");
    }

    public void setExtraKeltnerEma(double ema){
        JSONObject extraJSON = extra;        
        ((JSONObject)extraJSON.get("calculation")).put("keltner_ema", ema);
        setExtra(extraJSON);
    }
    
    public double getKeltnerAtr(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("keltner_atr");
    }

    public void setExtraKeltnerAtr(double atr){
        JSONObject extraJSON = extra;        
        ((JSONObject)extraJSON.get("calculation")).put("keltner_atr", atr);
        setExtra(extraJSON);
    }
    
    public void setKeltnerValue(){
        setValue(this.getAdjClose());
    }
    
    public void setKeltnerChannel(String channel, double value){
        this.keltnerChannels.put(channel, value);
    }
    
    public double getKeltnerChannel(String channel){
        return this.keltnerChannels.get(channel);
    }
    
    public void setKeltnerGrade(int emaDays){
        if(!keltnerChannels.isEmpty()){
            if(this.getAdjClose() > keltnerChannels.get("Upper5")){
                this.setGrade(10f);
            }else if(this.getAdjClose() < keltnerChannels.get("Lower5")){
                this.setGrade(0f);                
            }else if(this.getAdjClose() == keltnerChannels.get("Neutral")){
                this.setGrade(5f);
            }else if(this.getAdjClose() > keltnerChannels.get("Neutral")){
                double step = (double)TechnicalAnalysis
                        .subtract(keltnerChannels.get("Neutral"), keltnerChannels.get("Lower5"))/5;
                this.setGrade((float)(5+(TechnicalAnalysis
                                .subtract(this.getAdjClose(), keltnerChannels.get("Neutral"))/step)));
                            
            }else if(this.getAdjClose() < keltnerChannels.get("Neutral")){
                double step = (double)TechnicalAnalysis
                        .subtract(keltnerChannels.get("Neutral"), keltnerChannels.get("Lower5"))/5;
                this.setGrade((float)((TechnicalAnalysis
                                .subtract(this.getAdjClose(), keltnerChannels.get("Lower5"))/step)));
            }
            
            //extra
            JSONObject extraJSON = extra;
            extraJSON.put("top", new JSONObject());
//            ((JSONObject)extraJSON.get("top")).put("lower2", keltnerChannels.get("Lower2"));
            ((JSONObject)extraJSON.get("top")).put("Lower 1", keltnerChannels.get("Lower1"));
            ((JSONObject)extraJSON.get("top")).put("EMA"+emaDays, keltnerChannels.get("Neutral"));
            ((JSONObject)extraJSON.get("top")).put("Upper 1", keltnerChannels.get("Upper1"));
//            ((JSONObject)extraJSON.get("top")).put("upper2", keltnerChannels.get("Upper2"));
            
            setExtra(extraJSON);
            
            //min, max, avg
            setMin(keltnerChannels.get("Lower5"));
            setAvg(keltnerChannels.get("Neutral"));
            setMax(keltnerChannels.get("Upper5"));
        }
    }
}
