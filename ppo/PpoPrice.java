package technicalanalysis.ppo;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class PpoPrice extends Price{
    private double ppoEma;
    private double diff;
    
    public PpoPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);        
    }

    void setExtraEma(int days, double ema) {
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("ema ("+days+")", ema);
        setExtra(extraJSON);
    }
    
    public double getPpoEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ppo_ema");
    }

    public void setExtraPpoEma(double ema){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("ppo_ema", ema);
        setExtra(extraJSON);
    }
    
    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

    public double getPpoEma() {
        return ppoEma;
    }

    public void setPpoEma(double ppoEma) {
        this.ppoEma = ppoEma;
    }
    
    public void setPpoGrade(double prevDiff, int emaDays){
        float grade = -1;
        if(getValue() == 0){
            grade = 5f;
        }else if(getValue() < 0){
            double step = Math.abs(min/5);
            grade = (float) (TechnicalAnalysis.subtract(getValue(), min)/step);
        }else if(getValue() > 0){
            double step = max/5;
            grade = (float) (5+getValue()/step);
        }
        
        JSONObject extraJSON = extra;
        if(prevDiff < 0 && this.diff > 0 && grade >= 6){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the PPO rises above the signal line, we assign Grade 10.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            grade = 10f;
        }else if(prevDiff > 0 && this.diff < 0 && grade <=4){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the PPO falls below the signal line, we assign Grade 0.");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            grade = 0f;
        }
        
        setGrade(grade);
        
        // extra
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("PPO", getValue());
        ((JSONObject)extraJSON.get("bottom")).put("EMA"+emaDays+" (PPO)", this.ppoEma);
        setExtra(extraJSON);
    }
}
