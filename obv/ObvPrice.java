package technicalanalysis.obv;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class ObvPrice extends Price{
    private double obv;
    private HashMap<Integer, Double> smaObv = new HashMap();
    
    public ObvPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);

        this.adjClose = (double)Math.round(this.adjClose*100000000)/100000000;
    }

    public double getObv() {
        return obv;
    }

    public double getObv(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("obv");
    }
    
    public void setObv(double obv) {
        this.obv = obv;
    }
    
    public double getSmaObv(int days){
        return smaObv.get(days);
    }
    
    public void setSmaObv(int days, double value){
        smaObv.put(days, value);
    }
    
    public void setExtraObv(double obv){
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("obv", obv);
        setExtra(extraJSON);
    }
    
    public void setObvGrade(){        
        float obvGrade = -1f;
        if(getValue() == 0){
            obvGrade = 5f;
        }else if(getValue() > 0){
            double step = Math.abs(max/5);
            obvGrade = (float) (5+getValue()/step);
        }else if(getValue() < 0){
            double step = Math.abs(min/5);
            obvGrade = (float) (TechnicalAnalysis.subtract(getValue(), min)/step);
        }
        
        if(obvGrade > 10) obvGrade = 10;
        if(obvGrade < 0) obvGrade = 0;
        
        this.setGrade(obvGrade);

        // extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("OBV", getObv());
        setExtra(extraJSON);        
    }
}
