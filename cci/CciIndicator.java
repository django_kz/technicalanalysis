package technicalanalysis.cci;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class CciIndicator extends Indicator{
    int tpSma;
    
    public CciIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new CciPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
                
//        tpSma = (int)((long)settings.getTechnical().get("tp_sma"));
        tpSma = hash.get("tp_sma");
    }
    public CciIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new CciPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
                
        tpSma = (int)((long)settings.getTechnical().get("tp_sma"));
    }
    public int getCutEdge(){
        return super.getCutEdge() + tpSma ;
    }
    
    public void calculate(){
        cutAtBeginning(tpSma);
        setTypicalPrice();
        setValue();
        cutDays(tpSma);
        setMinMax(indicatorId);
        cutAtEnding();
        prices.forEach(p->{
            ((CciPrice)p).setCciGrade(tpSma);
        });
    }
    
    private void setTypicalPrice(){
        prices.forEach(p->{
            double high = p.getAdjHigh();
            double low = p.getAdjLow();
            double close = p.getAdjClose();
            
            double tp = TechnicalAnalysis.sum(TechnicalAnalysis.sum(high, low), close)/3;
            ((CciPrice)p).setTypicalPrice(tp);
        });
    }
    
    private void setValue(){
        int index = prices.size()-1;
        ArrayList<Double> tp = new ArrayList();
        for(int i = index; i > index-tpSma+1 && i>=0; i--){
            tp.add(((CciPrice)prices.get(i)).getTypicalPrice());            
        }                 
        double sum = getSum(tp);
        
        index = index-tpSma+1;
        while(index >= 0){
            double typicalPrice = ((CciPrice)prices.get(index)).getTypicalPrice();
            tp.add(typicalPrice);
            sum = TechnicalAnalysis.sum(sum, typicalPrice);
            
            double sma = sum/tpSma;
            double meanDev = meanDeviation(tp, sma);
            double value = TechnicalAnalysis.subtract(typicalPrice, sma)/(0.015*meanDev);
            value = Double.isNaN(value)?0:value;
            
            prices.get(index).setValue(value);
            
            sum = TechnicalAnalysis.subtract(sum, tp.get(0));
            tp.remove(0);
            index--;
        }
    }
    
    private double meanDeviation(ArrayList<Double> tpArray, double sma){
        double meanDev = 0.0;
        
        for(double tp:tpArray){
            double diff = Math.abs(TechnicalAnalysis.subtract(sma, tp));
            meanDev = TechnicalAnalysis.sum(meanDev, diff);
        }
        
        return meanDev/tpSma;
    }
}
