/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package technicalanalysis.keltner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

/**
 *
 * @author tt
 */
public class KeltnerIndicator extends Indicator{
    
    int ema;
    int atr;
        
    public KeltnerIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> { 
            temp.add(new KeltnerPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        ema = (int)((long)settings.getTechnical().get("ema"));
        atr = (int)((long)settings.getTechnical().get("atr"));
    }
    
    public KeltnerIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> { 
            temp.add(new KeltnerPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        ema = hash.get("ema");
        atr = hash.get("atr");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + ema;
    }
    
    public void calculate(){        
        cutAtBeginning(ema);
        setKeltnerChannels();        
        cutDays(ema);
        prices.forEach((p) -> {
            ((KeltnerPrice)p).setKeltnerGrade(ema);
        });
        cutAtEnding();
    }
    
    private void setKeltnerChannels(){
        double smoothingConstant = (double)2/(ema+1);
        int index = prices.size()-1;
        double emaSum = 0.0;
        double atrSum = 0.0;
        double trueRange = 0.0;
        for(int i = index; i >  index-ema && i>=0; i--){            
            emaSum = TechnicalAnalysis.sum(emaSum, prices.get(i).getAdjClose());
            if(i <= index-atr){
                double a = TechnicalAnalysis
                        .subtract(prices.get(i).getAdjHigh(), prices.get(i).getAdjLow());
                double b = Math.abs(TechnicalAnalysis
                        .subtract(prices.get(i).getAdjHigh(), prices.get(i+1).getAdjClose()));
                double c = Math.abs(TechnicalAnalysis
                        .subtract(prices.get(i).getAdjLow(), prices.get(i+1).getAdjClose()));
                trueRange = (a > b ?
                                (a > c ? a : c)
                                    :
                                (b > c ? b : c));
                atrSum = TechnicalAnalysis.sum(atrSum, trueRange);
            }            
        }
        index -= ema;
        if(index >= 0){
            double keltnerEma = TechnicalAnalysis.sum((double)(emaSum/ema), 0);
            double keltnerAtr = TechnicalAnalysis.sum((double)(atrSum/atr), 0);                    
            
            double adjClose = prices.get(index+1).getAdjClose();

            if(containsGrade()){
                keltnerEma = ((KeltnerPrice)prices.get(index)).getKeltnerEma(indicatorId, period);
                keltnerAtr = ((KeltnerPrice)prices.get(index)).getKeltnerAtr(indicatorId, period);
            }
            while(index >= 0){
                double a = TechnicalAnalysis
                            .subtract(prices.get(index).getAdjHigh(), prices.get(index).getAdjLow());
                double b = Math.abs(TechnicalAnalysis
                        .subtract(prices.get(index).getAdjHigh(), adjClose));
                double c = Math.abs(TechnicalAnalysis
                        .subtract(prices.get(index).getAdjLow(), adjClose));

                trueRange = (a > b ?
                                    (a > c ? a : c)
                                        :
                                    (b > c ? b : c));

                keltnerAtr = (double)(TechnicalAnalysis
                        .sum(keltnerAtr*(atr-1), trueRange)/atr);

                adjClose = prices.get(index).getAdjClose();
                keltnerEma = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(adjClose, keltnerEma)), keltnerEma);
                for(int i = 5; i >= 1; i--){
                    double upper = TechnicalAnalysis.sum(keltnerEma, (double)keltnerAtr*i);
                    double lower = TechnicalAnalysis.subtract(keltnerEma, (double)keltnerAtr*i);

                    ((KeltnerPrice)prices.get(index)).setKeltnerChannel("Upper"+i, upper);
                    ((KeltnerPrice)prices.get(index)).setKeltnerChannel("Lower"+i, lower);
                    if(i == 1){
                        ((KeltnerPrice)prices.get(index)).setKeltnerChannel("Neutral", 
                                (double)TechnicalAnalysis.sum(upper, lower)/2);
                    }
                }
                ((KeltnerPrice)prices.get(index)).setKeltnerValue();            

                index--;
            }
            ((KeltnerPrice)prices.get(0)).setExtraKeltnerEma(keltnerEma);
            ((KeltnerPrice)prices.get(0)).setExtraKeltnerAtr(keltnerAtr);
        }
    }        
}
