package technicalanalysis.mfi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class MfiIndicator extends Indicator{
    int days;
    
    public MfiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new MfiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
//        days = (int)((long)settings.getTechnical().get("days"));
        days = hash.get("days");
    }
    
    public MfiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new MfiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        days = (int)((long)settings.getTechnical().get("days"));
    }
    public int getCutEdge(){
        return super.getCutEdge() + days + 1;
    }

    
    public void calculate(){
        //cutAtBeginning(days+1);
        setMfiValue();
        cutDays(days+1);
        prices.forEach(p->{
            ((MfiPrice)p).setMfiGrade(days);
        });
        //cutAtEnding();
    }
    
    private void setMfiValue(){
        int index = prices.size()-1;
        if(index >= days){
            Price p = prices.get(index);
            double prevTypicalPrice = TechnicalAnalysis
                    .sum(TechnicalAnalysis.sum(p.getAdjClose(), p.getAdjHigh()), p.getAdjLow())/3;
            index--;
            ArrayList<Double> positiveMF = new ArrayList();
            ArrayList<Double> negativeMF = new ArrayList();        
            for(int i = index; i > index-days+1 && i>=0; i--){
                p = prices.get(i);
                double typicalPrice = TechnicalAnalysis
                    .sum(TechnicalAnalysis.sum(p.getAdjClose(), p.getAdjHigh()), p.getAdjLow())/3;
                if(prevTypicalPrice == typicalPrice){
                    positiveMF.add(0.0);
                    negativeMF.add(0.0);
                }else if(prevTypicalPrice > typicalPrice){
                    positiveMF.add(0.0);
                    negativeMF.add(typicalPrice*p.getAdjVolume()/1000);
                }else{
                    positiveMF.add(typicalPrice*p.getAdjVolume()/1000);
                    negativeMF.add(0.0);
                }
                
                prevTypicalPrice = typicalPrice;
            }

            double positiveSum = getSum(positiveMF);
            double negativeSum = getSum(negativeMF);
            
            index = index-days+1;
            while(index >= 0){
                p = prices.get(index);
                double typicalPrice = TechnicalAnalysis
                    .sum(TechnicalAnalysis.sum(p.getAdjClose(), p.getAdjHigh()), p.getAdjLow())/3;
                if(prevTypicalPrice == typicalPrice){
                    positiveMF.add(0.0);
                    negativeMF.add(0.0);
                }else if(prevTypicalPrice > typicalPrice){
                    positiveMF.add(0.0);
                    negativeMF.add(typicalPrice*p.getAdjVolume()/1000);
                    negativeSum = TechnicalAnalysis
                            .sum(negativeSum, typicalPrice*p.getAdjVolume()/1000);
                }else{
                    positiveMF.add(typicalPrice*p.getAdjVolume()/1000);
                    negativeMF.add(0.0);
                    positiveSum = TechnicalAnalysis
                            .sum(positiveSum, typicalPrice*p.getAdjVolume()/1000);
                }
                
                if(negativeSum > 0){
                    double value = TechnicalAnalysis.subtract(100, 
                            (100/(TechnicalAnalysis.sum(1, positiveSum/negativeSum))));
                    p.setValue(value);
                }else{
                    p.setValue(100);
                }

                prevTypicalPrice = typicalPrice;
                positiveSum = TechnicalAnalysis
                        .subtract(positiveSum, positiveMF.get(0));
                negativeSum = TechnicalAnalysis
                        .subtract(negativeSum, negativeMF.get(0));
                positiveMF.remove(0);
                negativeMF.remove(0);
                index--;
            }
        }
    }
    
}
