package technicalanalysis.stochasticoscillator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class StochasticOscillatorIndicator extends Indicator{
    int days;
    int smaStochastic;
    
    public StochasticOscillatorIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
                
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new StochasticOscillatorPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        days = (int)((long)settings.getTechnical().get("days"));
        smaStochastic = (int)((long)settings.getTechnical().get("sma_stochastic"));
    }
    public StochasticOscillatorIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
                
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new StochasticOscillatorPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        days = hash.get("days");
        smaStochastic = hash.get("sma_stochastic");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + days + smaStochastic;
    }
    
    public void calculate(){
        //cutAtBeginning(days+smaStochastic);
        setStochasticOscillator();
        cutDays(days);
        setDifference();
        cutDays(smaStochastic);
        setGrades();
        cutDays(2);
        //cutAtEnding();
    }

    private void setStochasticOscillator() {
        TreeSet<Double> highTree = new TreeSet();
        TreeSet<Double> lowTree = new TreeSet();
        HashMap<Double, Integer> countOfHighInTree = new HashMap();
        HashMap<Double, Integer> countOfLowInTree = new HashMap();
        LinkedList<Double> indexOfHigh = new LinkedList();
        LinkedList<Double> indexOfLow = new LinkedList();
        
        int index = prices.size()-1;
        for(int i = index; i > index-days+1 && i>=0; i--){
            //insert
            double high = prices.get(i).getAdjHigh();
            double low = prices.get(i).getAdjLow();
            if(!countOfHighInTree.containsKey(high)){
                highTree.add(high);
                countOfHighInTree.put(high, 1);
            }else{
                countOfHighInTree.put(high, countOfHighInTree.get(high)+1);
            }
            
            if(!countOfLowInTree.containsKey(low)){
                lowTree.add(low);
                countOfLowInTree.put(low, 1);
            }else{
                countOfLowInTree.put(low, countOfLowInTree.get(low)+1);
            }
            indexOfHigh.add(high);
            indexOfLow.add(low);
            //end of insert                        
        }
        index = index-days+1;
        while(index>=0){
            //insert
            double high = prices.get(index).getAdjHigh();
            double low = prices.get(index).getAdjLow();
            if(!countOfHighInTree.containsKey(high)){
                highTree.add(high);
                countOfHighInTree.put(high, 1);
            }else{
                countOfHighInTree.put(high, countOfHighInTree.get(high)+1);
            }
            
            if(!countOfLowInTree.containsKey(low)){
                lowTree.add(low);
                countOfLowInTree.put(low, 1);
            }else{
                countOfLowInTree.put(low, countOfLowInTree.get(low)+1);
            }
            indexOfHigh.add(high);
            indexOfLow.add(low);
            //end of insert
            
            double highMax = highTree.last();
            double lowMin = lowTree.first();
            
            double stochastic = TechnicalAnalysis.subtract(prices.get(index).getAdjClose(), lowMin);
            stochastic = highMax==lowMin?50.0:stochastic*100/TechnicalAnalysis.subtract(highMax,lowMin);
            
            prices.get(index).setValue(stochastic);
                
            
            //remove first
            double removeHigh = indexOfHigh.get(0);
            countOfHighInTree.put(removeHigh, 
                    countOfHighInTree.get(removeHigh)-1);
            if(countOfHighInTree.get(removeHigh)==0){
                highTree.remove(removeHigh);
                countOfHighInTree.remove(removeHigh);
            }
            double removeLow = indexOfLow.get(0);
            countOfLowInTree.put(removeLow, 
                    countOfLowInTree.get(removeLow)-1);
            if(countOfLowInTree.get(removeLow)==0){
                lowTree.remove(removeLow);
                countOfLowInTree.remove(removeLow);
            }
            indexOfHigh.remove(0);
            indexOfLow.remove(0);
            //end of remove
            index--;
        }
    }
    
    private void setDifference(){
        int index = prices.size()-1;
        ArrayList<Double> stochastic = new ArrayList();
        for(int i = index; i > index-smaStochastic+1 && i>=0; i--){
            stochastic.add(prices.get(i).getValue());            
        }                 
        double sum = getSum(stochastic);
        
        index = index-smaStochastic+1;
        while(index >= 0){
            stochastic.add(prices.get(index).getValue());
            sum = TechnicalAnalysis.sum(sum, prices.get(index).getValue());            
            
            double sma = sum/smaStochastic;
            
            ((StochasticOscillatorPrice)prices.get(index)).setSmaStochastic(sma);
            ((StochasticOscillatorPrice)prices.get(index)).setDiff();
            
            sum = TechnicalAnalysis.subtract(sum, stochastic.get(0));
            stochastic.remove(0);
            index--;
        }
    }

    private void setGrades(){
        int index = prices.size()-1;
        if(index > 0){
            double diff = ((StochasticOscillatorPrice)prices.get(index)).getDiff();
            index--;

            while(index >= 0){
                ((StochasticOscillatorPrice)prices.get(index)).setStochasticGrade(diff, smaStochastic);
                diff = ((StochasticOscillatorPrice)prices.get(index)).getDiff();
                index--;
            }
        }
    }
    
}
