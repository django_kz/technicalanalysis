package technicalanalysis.coppockcurve;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.json.simple.JSONArray;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class CoppockCurveIndicator extends Indicator{
    List<Integer> rocSettings = new ArrayList();
    int days;
    
    public CoppockCurveIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new CoppockCurvePrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("roc");
        
        jsonArray.forEach((o) -> {            
            rocSettings.add((int)((long)o));
        });
        Collections.sort(rocSettings);
        days = (int)((long)settings.getTechnical().get("days"));
    }
    
    public CoppockCurveIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new CoppockCurvePrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("roc");
        
        jsonArray.forEach((o) -> {            
            rocSettings.add((int)((long)o));
        });
        rocSettings.add(hash.get("roc1"));
        rocSettings.add(hash.get("roc2"));
        Collections.sort(rocSettings);
        days = hash.get("days");
    }
    
   public int getCutEdge(){
        return super.getCutEdge() + rocSettings.get(rocSettings.size()-1) + days;
    }
    
    public void calculate(){
        //cutAtBeginning(rocSettings.get(rocSettings.size()-1)+days);
        rocSettings.forEach(this::setRoc);
        cutDays(rocSettings.get(rocSettings.size()-1)+1);
        setRocSum();
        setRocValue();
        cutDays(days);
        setMinMax(indicatorId);
        cutAtEnding();
        prices.forEach(p->{
            ((CoppockCurvePrice)p).setCoppockGrade();
        });
    }
    
    private void setRoc(int rocDays){
        int fIndex = prices.size() - 1- rocDays;
        int lIndex = prices.size()- 1;
        while(fIndex>=0){
            double fAdjClose = prices.get(fIndex).getAdjClose();
            double lAdjClose = prices.get(lIndex).getAdjClose();

            double roc = TechnicalAnalysis.subtract(fAdjClose, lAdjClose)/lAdjClose*100;

            ((CoppockCurvePrice)prices.get(fIndex)).setRoc(rocDays, roc);
            fIndex--;
            lIndex--;
        }
    }
    
    private void setRocSum(){
        prices.forEach(p->{
            double sum = 0.0;
            for(int key:rocSettings){    
                sum = TechnicalAnalysis.sum(sum, ((CoppockCurvePrice)p).getRoc(key));
            }
            ((CoppockCurvePrice)p).setRocSum(sum);
        });
    }

    private void setRocValue() {
        int fIndex = 0;
        int lIndex = days-1;
        
        int daysSum = 0;
        for(int i = 1; i <= days; i++){
            daysSum += i;
        }
        
        while(lIndex < prices.size()){
            double sum = 0.0;
            int index = fIndex;
            for(int i = days; i >=1; i--){
                double rocSum = ((CoppockCurvePrice)prices.get(index)).getRocSum();
                sum = TechnicalAnalysis.sum(sum, i*rocSum);
                index++;
            }
            prices.get(fIndex).setValue(sum/daysSum);
            fIndex++;
            lIndex++;
        }
    }
}
