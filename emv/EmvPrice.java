package technicalanalysis.emv;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class EmvPrice extends Price{
    private double emv;

    public EmvPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getEmv() {
        return emv;
    }

    public void setEmv(double emv) {
        this.emv = emv;
    }
    
    public void setEmvGrade(int emvDays){
        float emvGrade = -1;
        if(this.value  == 0){
            emvGrade = 5f;
        }else if(this.value < 0){
            double step = Math.abs(this.min/5);
            emvGrade = (float) (TechnicalAnalysis.subtract(this.value, this.min)/step);
        }else if(this.value > 0){
            double step = Math.abs(this.max/5);
            emvGrade = (float) (5+this.value/step);
        }

        setGrade(emvGrade);
        
        //extra 
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("EMV ("+emvDays+")", getValue());
        setExtra(extraJSON);
    }
}
