package technicalanalysis.coppockcurve;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class CoppockCurvePrice extends Price{
    private HashMap<Integer, Double> roc = new HashMap();
    private double rocSum;    
    
    public CoppockCurvePrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public Double getRoc(int index) {
        return roc.get(index);
    }

    public void setRoc(int index, double value) {
        this.roc.put(index, value);
    }

    public double getRocSum() {
        return rocSum;
    }

    public void setRocSum(double rocSum) {
        this.rocSum = rocSum;
    }
    
    public void setCoppockGrade(){
        float coppockGrade = -1;
        if(getValue() == 0){
            coppockGrade = 5f;
        }else if(getValue() < 0){
            double step = Math.abs(min/5);
            coppockGrade = (float) (TechnicalAnalysis.subtract(getValue(), min)/step);
        }else if(getValue() > 0){
            double step = Math.abs(max/5);
            coppockGrade = (float) (TechnicalAnalysis.sum(5, getValue()/step));
        }
        setGrade(coppockGrade);
        
        //extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Coppock Curve", getValue());
        setExtra(extraJSON);
    }
    
}
