/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package technicalanalysis;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author tt
 */
public class Price {
    
    protected Date date;
    protected double adjClose;
    protected double adjHigh;
    protected double adjLow;
    protected double adjVolume;
    protected long closeInCents;
    protected Date updatedAt; 
    
    protected double value;
    protected double min;
    protected double max;
    protected double avg = 0;
    protected float grade;
    protected JSONObject extra = new JSONObject();
    
    
    protected HashMap<Long, Pair<Date, HashMap<Integer, String>>> currentDate = new HashMap();
    protected HashMap<Integer, Double> sma = new HashMap();
    protected HashMap<Integer, Double> ema = new HashMap();        
    
    public Price(Date date, double adjClose, double adjHigh, double adjLow, double adjVolume, 
            long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastGrade) {        
        this.date = date;
        this.adjClose = adjClose;
        this.adjHigh = adjHigh;
        this.adjLow = adjLow;
        this.adjVolume = adjVolume;
        this.closeInCents = close;
        this.updatedAt = updatedAt;
        this.currentDate = lastGrade;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAdjClose() {
        return adjClose;
    }

    public void setAdjClose(double adjClose) {
        this.adjClose = adjClose;
    }

    public double getAdjHigh() {
        return adjHigh;
    }

    public void setAdjHigh(double adjHigh) {
        this.adjHigh = adjHigh;
    }

    public double getAdjLow() {
        return adjLow;
    }

    public void setAdjLow(double adjLow) {
        this.adjLow = adjLow;
    }   

    public double getAdjVolume() {
        return adjVolume;
    }

    public void setAdjVolume(double adjVolume) {
        this.adjVolume = adjVolume;
    }
    
    public long getCloseInCents() {
        return closeInCents;
    }

    public void setCloseInCents(long closeInCents) {
        this.closeInCents = closeInCents;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {       
        this.min = min;
    }
    
    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }
    
    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }        

    public String getExtra() {
        return extra.toString();
    }

    public void setExtra(JSONObject extra) {
        this.extra = extra;
    }
    
    public boolean isNeedToCount(long indicatorId){
        if(currentDate.containsKey(indicatorId)){
            return date.compareTo(currentDate.get(indicatorId).getKey())==1;
        }
        return true;
    }        
    
    public HashMap<Long, Pair<Date, HashMap<Integer, String>>> getCurrentDate(){
        return currentDate;
    }
    
    public double getSma(int days){
        return sma.get(days);
    }
    
    public void setSma(int days, double value){
        sma.put(days, value);
    }
    
    public double getEma(int days){
        return ema.get(days);
    }
    
    public void setEma(int days, double value){
        ema.put(days, value);
    }

    public double getEma(long indicatorId, int period, int days){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
            if(object.get("calculation") == null){
                return Double.NaN;
            }
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ema ("+days+")"); 
    }
    
    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }
    
    
}
