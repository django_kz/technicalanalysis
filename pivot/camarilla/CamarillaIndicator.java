package technicalanalysis.pivot.camarilla;

import java.util.ArrayList;
import java.util.List;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;
import technicalanalysis.pivot.PivotIndicator;
import technicalanalysis.pivot.PivotPrice;

public class CamarillaIndicator extends PivotIndicator{
    
    public CamarillaIndicator(long indicatorId, String ticker, Settings settings, List<Price> prices, int period) {
        super(indicatorId, ticker, settings, prices, period);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new PivotPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
    }
    
    public void calculate(){
        cutAtBeginning(25);
        findPP();
        setSupportAndResistance();
        cutAtEnding();
        prices.forEach((p)->{
            ((PivotPrice)p).setValue(p.getAdjClose());
            ((PivotPrice)p).setPivotGrade();
//            if(period==5){
//                double[] res = ((PivotPrice)p).getResistance();
//                double[] sup = ((PivotPrice)p).getSupport();
//                System.out.println(p.getDate()+"\t"+((PivotPrice)p).getPp()+"\t"+res[0]+"\t"+res[1]+"\t"+res[2]+"\t"+res[3]+"\t"+res[4]+"\t"+sup[0]+"\t"+sup[1]+"\t"+sup[2]+"\t"+sup[3]+"\t"+sup[4]+"\t"+((PivotPrice)p).getMin()+"\t"+((PivotPrice)p).getMax()+"\t"+p.getGrade());
//            }
        });   
    }

    private void setSupportAndResistance() {
        double coef[] = {1.0833, 1.1666, 1.25, 1.5, 1.168};
        prices.forEach((p)->{
            double support[] = new double[5];
            double resistance[] = new double[5];
            
            for(int i = 0; i < 4; i++){
                support[i] = TechnicalAnalysis.subtract(((PivotPrice)p).getPpClose(), ((PivotPrice)p).getDiff()*coef[i]);
                resistance[i] = TechnicalAnalysis.sum(((PivotPrice)p).getPpClose(), ((PivotPrice)p).getDiff()*coef[i]);
            }
            support[4] = TechnicalAnalysis.subtract(support[3], 
                    TechnicalAnalysis.subtract(support[2], support[3])*coef[4]);
            resistance[4] = TechnicalAnalysis.sum(resistance[3], 
                    TechnicalAnalysis.subtract(resistance[3], resistance[2])*coef[4]);
            
            ((PivotPrice)p).setSupport(support);
            ((PivotPrice)p).setResistance(resistance);
        });
    }
}
