package technicalanalysis.cci;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class CciPrice extends Price{
    private double typicalPrice;
    
    public CciPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getTypicalPrice() {
        return typicalPrice;
    }

    public void setTypicalPrice(double typicalPrice) {
        this.typicalPrice = typicalPrice;
    }
    
    public void setCciGrade(int cciDays){
        float cciGrade = -1;
        if(value == 0){
            cciGrade = 5f;
        }else if(value >= -100 && value < 0){
            cciGrade = (float)(TechnicalAnalysis.sum(4, TechnicalAnalysis.subtract(value, -100)/100));
        }else if(value <= 100 && value > 0){
            cciGrade = (float)(TechnicalAnalysis.sum(5, value/100));
        }else if(value < -100){
            double step = Math.abs(TechnicalAnalysis.subtract(min, -100)/4);
            cciGrade = (float) (TechnicalAnalysis.subtract(value, min)/step);
        }else if(value > 100){
            double step = TechnicalAnalysis.subtract(max, 100)/4;
            cciGrade = (float)(TechnicalAnalysis.sum(6, TechnicalAnalysis.subtract(value, 100)/step));
        }
        
        setGrade(10-cciGrade);
        
        double temp = getMax();
        setMax(getMin());
        setMin(temp);
        
        //extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("CCI ("+cciDays+")", getValue());
        setExtra(extraJSON);
    }
}
