package technicalanalysis.tsi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class TsiIndicator extends Indicator{
    int emaMomentum;
    int emaOfEma;
    
    public TsiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new TsiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        emaMomentum = (int)((long)settings.getTechnical().get("ema_momentum"));
        emaOfEma = (int)((long)settings.getTechnical().get("ema_of_ema"));
    }
    public TsiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new TsiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        emaMomentum = hash.get("ema_momentum");
        emaOfEma = hash.get("ema_of_ema");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + emaMomentum+emaOfEma;
    }
    
    public void calculate(){
        //cutAtBeginning(emaMomentum+emaOfEma);
        setMomentum();
        cutDays(2);
        setEmaMomentum();
        cutDays(emaMomentum);
        setEmaOfEma();
        cutDays(emaOfEma);
        prices.forEach(p->{
            ((TsiPrice)p).setTsiValue();
            ((TsiPrice)p).setTsiGrade();
        });
        cutAtEnding();
    }
    
    public List<Price> calculatePrices(){
//        cutAtBeginning(Math.max(highLowEma+emaOfEma+massIndexDays-2, sma+1));
        return this.prices;
//        cutAtEnding();
    }
    
    public void calculateFinal(List<Price> prices){
        this.calculate();
    }

    private void setMomentum() {
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();
            index--;
            
            while(index >= 0){
                double adjClose = prices.get(index).getAdjClose();
                double momentum = TechnicalAnalysis.subtract(adjClose, prevAdjClose);
                
                ((TsiPrice)prices.get(index)).setMomentum(momentum);
                ((TsiPrice)prices.get(index)).setMomentumAbs(Math.abs(momentum));
                
                prevAdjClose = adjClose;
                index--;
            }
        }
    }

    private void setEmaMomentum() {
        double smoothingConstant = (double)2/(emaMomentum+1);
        int index = prices.size()-1;
        ArrayList<Double> momentums = new ArrayList();
        ArrayList<Double> momentumsAbs = new ArrayList();
        for(int i = index; i > index-emaMomentum && i>=0; i--){
            momentums.add(((TsiPrice)prices.get(i)).getMomentum());
            momentumsAbs.add(((TsiPrice)prices.get(i)).getMomentumAbs());
        }
        
        double ema  = getSum(momentums)/emaMomentum;
        double emaAbs  = getSum(momentumsAbs)/emaMomentum;
        index = index-emaMomentum;
        if(index >= 0){
            if(containsGrade()){
                ema = ((TsiPrice)prices.get(index)).getEmaMomentum(indicatorId, period);
                emaAbs = ((TsiPrice)prices.get(index)).getEmaMomentumAbs(indicatorId, period);
            }
            
            ((TsiPrice)prices.get(index+1)).setEmaMomentum(ema);
            ((TsiPrice)prices.get(index+1)).setEmaMomentumAbs(emaAbs);
            while(index >= 0){
                double momentum = ((TsiPrice)prices.get(index)).getMomentum();
                double momentumAbs = ((TsiPrice)prices.get(index)).getMomentumAbs();
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(momentum, ema)), ema);
                emaAbs = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(momentumAbs, emaAbs)), emaAbs);
                
                ((TsiPrice)prices.get(index)).setEmaMomentum(ema);
                ((TsiPrice)prices.get(index)).setEmaMomentumAbs(emaAbs);
                index--;
            }
            ((TsiPrice)prices.get(0)).setExtraEmaMomentum(((TsiPrice)prices.get(emaOfEma-1)).getEmaMomentum());
            ((TsiPrice)prices.get(0)).setExtraEmaMomentumAbs(((TsiPrice)prices.get(emaOfEma-1)).getEmaMomentumAbs());
        }
    }

    private void setEmaOfEma() {
        double smoothingConstant = (double)2/(emaOfEma+1);
        int index = prices.size()-1;
        ArrayList<Double> emaOfMomentums = new ArrayList();
        ArrayList<Double> emaOfMomentumsAbs = new ArrayList();
        for(int i = index; i > index-emaOfEma && i>=0; i--){
            emaOfMomentums.add(((TsiPrice)prices.get(i)).getEmaMomentum());            
            emaOfMomentumsAbs.add(((TsiPrice)prices.get(i)).getEmaMomentumAbs());            
        }
        
        double ema  = getSum(emaOfMomentums)/emaOfEma;        
        double emaAbs  = getSum(emaOfMomentumsAbs)/emaOfEma;
        index = index-emaOfEma;
        if(index >= 0){
            if(containsGrade()){
                ema = ((TsiPrice)prices.get(index)).getEmaOfEma(indicatorId, period);
                emaAbs = ((TsiPrice)prices.get(index)).getEmaOfEmaAbs(indicatorId, period);
            }
            
            ((TsiPrice)prices.get(index+1)).setEmaOfEma(ema);
            ((TsiPrice)prices.get(index+1)).setEmaOfEmaAbs(emaAbs);
            while(index >= 0){
                double emaOfMomentum = ((TsiPrice)prices.get(index)).getEmaMomentum();
                double emaOfMomentumAbs = ((TsiPrice)prices.get(index)).getEmaMomentumAbs();
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(emaOfMomentum, ema)), ema);
                emaAbs = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(emaOfMomentumAbs, emaAbs)), emaAbs);
                ((TsiPrice)prices.get(index)).setEmaOfEma(ema);
                ((TsiPrice)prices.get(index)).setEmaOfEmaAbs(emaAbs);
                
                index--;
            }
            ((TsiPrice)prices.get(0)).setExtraEmaOfEma(((TsiPrice)prices.get(0)).getEmaOfEma());
            ((TsiPrice)prices.get(0)).setExtraEmaOfEmaAbs(((TsiPrice)prices.get(0)).getEmaOfEmaAbs());
        }
    }
}
