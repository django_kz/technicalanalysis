package technicalanalysis.rsi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class RsiIndicator extends Indicator{
    
    int days;
    
    public RsiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new RsiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
//        days = (int)((long)settings.getTechnical().get("days"));
        days = hash.get("days");
    }
    
    public RsiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new RsiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        days = (int)((long)settings.getTechnical().get("days"));
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + days + 1 ;
    }

    
    public void calculate(){
        //cutAtBeginning(days+1);
        setChange();
        setRsiValue();
        cutDays(days+1);
        prices.forEach(p->{
            ((RsiPrice)p).setRsiGrade(days);
        });
        //cutAtEnding();
    }   
    
    private void setChange() {
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();
            index--;        
            while(index >= 0){
                double adjClose = prices.get(index).getAdjClose();
                ((RsiPrice)prices.get(index))
                        .setChange(TechnicalAnalysis.subtract(adjClose, prevAdjClose));
                prevAdjClose = adjClose;
                index--;
            }
        }
    }

    private void setRsiValue() {
        int index = prices.size()-2;
        ArrayList<Double> gain = new ArrayList();
        ArrayList<Double> loss = new ArrayList();
        for(int i = index; i > index-days && i>=0; i--){
            double change = ((RsiPrice)prices.get(i)).getChange();
            if(change > 0){
                gain.add(change);
                loss.add(0.0);
            }else{          
                gain.add(0.0);
                loss.add(Math.abs(change));
            }
        }
        if(index > 0){        
            double gainSum = getSum(gain);
            double lossSum = getSum(loss);
            index = index-days;

            double prevAvgGain = gainSum/days;
            double prevAvgLoss = lossSum/days;

            if(containsGrade()){
                prevAvgGain = ((RsiPrice)prices.get(index)).getAvgGain(indicatorId, period);
                prevAvgLoss = ((RsiPrice)prices.get(index)).getAvgLoss(indicatorId, period);
            }
            
            double rsi = prevAvgLoss==0?100:TechnicalAnalysis
                        .subtract(100, 100/TechnicalAnalysis.sum(1, prevAvgGain/prevAvgLoss));

            if(index - days >= 0){
                ((RsiPrice)prices.get(index+1)).setAvgGain(prevAvgGain);
                ((RsiPrice)prices.get(index+1)).setAvgLoss(prevAvgLoss);
                prices.get(index+1).setValue(rsi);        
            }
            
            while(index >= 0){
                double change = ((RsiPrice)prices.get(index)).getChange();                        

                double avgGain = TechnicalAnalysis.sum((prevAvgGain*(days-1)), change>0?change:0)/days;
                double avgLoss = TechnicalAnalysis.sum((prevAvgLoss*(days-1)), change<0?Math.abs(change):0)/days;

                rsi = avgLoss==0?100:TechnicalAnalysis
                        .subtract(100, 100/(TechnicalAnalysis.sum(1, avgGain/avgLoss)));

                ((RsiPrice)prices.get(index)).setAvgGain(avgGain);
                ((RsiPrice)prices.get(index)).setAvgLoss(avgLoss);
                prices.get(index).setValue(rsi);                                                                        

                prevAvgGain = avgGain;
                prevAvgLoss = avgLoss;
                index--;
            }
            ((RsiPrice)prices.get(0))
                    .setExtraAvg(((RsiPrice)prices.get(0)).getAvgGain(),
                            ((RsiPrice)prices.get(0)).getAvgLoss());
        }
    }
    
    
}
