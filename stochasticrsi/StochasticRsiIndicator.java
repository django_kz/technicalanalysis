package technicalanalysis.stochasticrsi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import org.json.simple.JSONArray;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class StochasticRsiIndicator extends Indicator{    
    int days;
    ArrayList<Integer> sma = new ArrayList();
    
    public StochasticRsiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new StochasticRsiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        days = (int)((long)settings.getTechnical().get("days"));
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("sma");
        
        jsonArray.forEach((o) -> {            
            sma.add((int)((long)o));
        });
        Collections.sort(sma);
    }
    
    public StochasticRsiIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new StochasticRsiPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });        
        this.prices = temp;        
        
        days = hash.get("days");
        sma.add(hash.get("sma1"));
        sma.add(hash.get("sma2"));
        Collections.sort(sma);
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + Math.max(2*days+1, sma.get(sma.size()-1));
    }
    
    public void calculate(){
        //cutAtBeginning(Math.max(2*days+1, sma.get(sma.size()-1)));
        setChange();
        setRsiValue();
        sma.forEach(this::setSMA);
        cutDays(days+1);
        setStochRsiValue();
        cutDays(Math.max(days, sma.get(sma.size()-1)- days));
        prices.forEach((p) -> {
            ((StochasticRsiPrice)p).setStochRsiGrade(sma, days);
        });
        //cutAtEnding();
    }
    
    private void setChange() {
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();
            index--;        
            while(index >= 0){
                double adjClose = prices.get(index).getAdjClose();
                ((StochasticRsiPrice)prices.get(index))
                        .setChange(TechnicalAnalysis.subtract(adjClose, prevAdjClose));
                prevAdjClose = adjClose;
                index--;
            }
        }
    }

    private void setRsiValue() {
        int index = prices.size()-2;
        ArrayList<Double> gain = new ArrayList();
        ArrayList<Double> loss = new ArrayList();
        for(int i = index; i > index-days && i>=0; i--){
            double change = ((StochasticRsiPrice)prices.get(i)).getChange();
            if(change > 0){
                gain.add(change);
                loss.add(0.0);
            }else{          
                gain.add(0.0);
                loss.add(Math.abs(change));
            }
        }
        if(index > 0){
            double gainSum = getSum(gain);
            double lossSum = getSum(loss);
            index = index-days;

            double prevAvgGain = gainSum/days;
            double prevAvgLoss = lossSum/days;

            if(containsGrade()){
                prevAvgGain = ((StochasticRsiPrice)prices.get(index)).getAvgGain(indicatorId, period);
                prevAvgLoss = ((StochasticRsiPrice)prices.get(index)).getAvgLoss(indicatorId, period);
            }
            
            double rsi = prevAvgLoss==0?100:TechnicalAnalysis
                        .subtract(100, 100/TechnicalAnalysis.sum(1, prevAvgGain/prevAvgLoss));

            if(index >= 0){
                ((StochasticRsiPrice)prices.get(index+1)).setAvgGain(prevAvgGain);
                ((StochasticRsiPrice)prices.get(index+1)).setAvgLoss(prevAvgLoss);
                ((StochasticRsiPrice)prices.get(index+1)).setRsiValue(rsi);        
            }
            while(index >= 0){
                double change = ((StochasticRsiPrice)prices.get(index)).getChange();

                double avgGain = TechnicalAnalysis.sum((prevAvgGain*(days-1)), change>0?change:0)/days;
                double avgLoss = TechnicalAnalysis.sum((prevAvgLoss*(days-1)), change<0?Math.abs(change):0)/days;

                rsi = avgLoss==0?100:TechnicalAnalysis
                        .subtract(100, 100/(TechnicalAnalysis.sum(1, avgGain/avgLoss)));

                ((StochasticRsiPrice)prices.get(index)).setAvgGain(avgGain);
                ((StochasticRsiPrice)prices.get(index)).setAvgLoss(avgLoss);
                ((StochasticRsiPrice)prices.get(index)).setRsiValue(rsi);                                                                        

                if(index == 0){
                    ((StochasticRsiPrice)prices.get(index))
                        .setExtraAvg(((StochasticRsiPrice)prices.get(Math.max(days, sma.get(sma.size()-1)- days - 1))).getAvgGain(),
                                ((StochasticRsiPrice)prices.get(Math.max(days, sma.get(sma.size()-1)- days - 1))).getAvgLoss());
                }
                
                prevAvgGain = avgGain;
                prevAvgLoss = avgLoss;                
                index--;
            }            
        }
    }
    
    private void setStochRsiValue(){
        TreeSet<Double> rsiTree = new TreeSet();
        HashMap<Double, Integer> countOfRsiInTree = new HashMap();
        LinkedList<Double> indexOfRsi = new LinkedList();
        
        int index = prices.size()-1;
        for(int i = index; i > index-days+1 && i>=0; i--){
            //insert
            double rsiValue = ((StochasticRsiPrice)prices.get(i)).getRsiValue();            
            if(!countOfRsiInTree.containsKey(rsiValue)){
                rsiTree.add(rsiValue);
                countOfRsiInTree.put(rsiValue, 1);
            }else{
                countOfRsiInTree.put(rsiValue, countOfRsiInTree.get(rsiValue)+1);
            }            
            indexOfRsi.add(rsiValue);
            //end of insert   
        }
        index = index-days+1;
        while(index>=0){
            //insert
            double rsiValue = ((StochasticRsiPrice)prices.get(index)).getRsiValue();            
            if(!countOfRsiInTree.containsKey(rsiValue)){
                rsiTree.add(rsiValue);
                countOfRsiInTree.put(rsiValue, 1);
            }else{
                countOfRsiInTree.put(rsiValue, countOfRsiInTree.get(rsiValue)+1);
            }      
            indexOfRsi.add(rsiValue);
            //end of insert
            
            double max = rsiTree.last();
            double min = rsiTree.first();
            
            double stochastic = TechnicalAnalysis.subtract(rsiValue, min);
            stochastic = max==min?0.5:stochastic/TechnicalAnalysis.subtract(max,min);
            
            prices.get(index).setValue(stochastic);                
            
            //remove first
            double removeRsi = indexOfRsi.get(0);
            countOfRsiInTree.put(removeRsi, 
                    countOfRsiInTree.get(removeRsi)-1);
            if(countOfRsiInTree.get(removeRsi)==0){
                rsiTree.remove(removeRsi);
                countOfRsiInTree.remove(removeRsi);
            }
            indexOfRsi.remove(0);
            //end of remove
            index--;
        }
    }
}
