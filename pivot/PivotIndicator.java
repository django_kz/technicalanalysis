package technicalanalysis.pivot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class PivotIndicator extends Indicator{
    
    public PivotIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, int period) {
        super(indicatorId, ticker, settings, prices); 
        this.period = period;
    }
    
    public void findPP(){
        TreeSet<Double> highSet = new TreeSet();
        TreeSet<Double> lowSet = new TreeSet();
        int step = getStepByPeriod(period);
        int len = prices.size() - 1;
        for(int i = prices.size()-2; i >= 0; i--){
            Price previous = prices.get(i + 1);
            highSet.add(previous.getAdjHigh());
            lowSet.add(previous.getAdjLow());
            if((len - i) % step == 0){
                ((PivotPrice)previous).calculatePp(highSet.last(), lowSet.first());
                ((PivotPrice)previous).setPpClose(previous.getAdjClose());
                ((PivotPrice)previous).setPpHigh(highSet.last());
                ((PivotPrice)previous).setPpLow(lowSet.first());
                ((PivotPrice)previous)
                        .setDiff(TechnicalAnalysis.subtract(highSet.last(), lowSet.first()));
                highSet.clear();
                lowSet.clear();
            }
        }


        double pp = 0.0;
        double diff = 0.0;
        double close = 0.0;
        double high = 0.0;
        double low = 0.0;
        int cutDays = 0;
        for(int i=prices.size()-1;i>=0;i--){
            Price p = prices.get(i);
            
            if(((PivotPrice)p).getPp() > 0){
                double temp = ((PivotPrice)p).getPp();
                double diffTemp = ((PivotPrice)p).getDiff();
                double closeTemp = ((PivotPrice)p).getPpClose();
                double highTemp = ((PivotPrice)p).getPpHigh();
                double lowTemp = ((PivotPrice)p).getPpLow();
                ((PivotPrice)p).setPp(pp);
                ((PivotPrice)p).setPpClose(close);
                ((PivotPrice)p).setPpHigh(high);
                ((PivotPrice)p).setPpLow(low);
                ((PivotPrice)p).setDiff(diff);
                pp = temp;
                close = closeTemp;
                high = highTemp;
                low = lowTemp;
                diff = diffTemp;
            }else{
                ((PivotPrice)p).setPp(pp);
                ((PivotPrice)p).setPpClose(close);
                ((PivotPrice)p).setPpHigh(high);
                ((PivotPrice)p).setPpLow(low);                
                ((PivotPrice)p).setDiff(diff);
            }            
            
            if(pp==0){
                cutDays++;
            }
        }
        if(cutDays > 0){
            cutDays(cutDays);
        }
    }
    
    public void findPPWoodie(){
        TreeSet<Double> highSet1 = new TreeSet();
        TreeSet<Double> lowSet1 = new TreeSet();
        int step = getStepByPeriod(period);
        int len = prices.size() - 1;
        for(int i = prices.size()-2; i >= 0; i--){
            Price previous = prices.get(i+1);
            Price current = prices.get(i);
            highSet1.add(previous.getAdjHigh());
            lowSet1.add(previous.getAdjLow());
            if((len - i) % step == 0){
                ((PivotPrice)previous).setPpHigh(highSet1.last());
                ((PivotPrice)previous).setPpLow(lowSet1.first());
                ((PivotPrice)previous).calculatePpWoodie(highSet1.last(), lowSet1.first());
                ((PivotPrice)previous)
                        .setDiff(TechnicalAnalysis.subtract(highSet1.last(), lowSet1.first()));
                highSet1.clear();
                lowSet1.clear();
            }
        }        

        double pp = 0.0;
        double diff = 0.0;
        double high = 0.0;
        double low = 0.0;
        int cutDays = 0;
        for(int i=prices.size()-1;i>=0;i--){
            Price p = prices.get(i);
            
            if(((PivotPrice)p).getPp() > 0){
                double temp = ((PivotPrice)p).getPp();
                double diffTemp = ((PivotPrice)p).getDiff();
                double highTemp = ((PivotPrice)p).getPpHigh();
                double lowTemp = ((PivotPrice)p).getPpLow();
                ((PivotPrice)p).setPp(pp);
                ((PivotPrice)p).setDiff(diff);
                ((PivotPrice)p).setPpHigh(high);
                ((PivotPrice)p).setPpLow(low);
                pp = temp;
                diff = diffTemp;
                high = highTemp;
                low = lowTemp;
            }else{
                ((PivotPrice)p).setPp(pp);
                ((PivotPrice)p).setPpHigh(high);
                ((PivotPrice)p).setPpLow(low);
                ((PivotPrice)p).setDiff(diff);
            }            
            
            if(pp==0){
                cutDays++;
            }
        }
        if(cutDays > 0){
            cutDays(cutDays);
        }
    }
    
    private int dayOfWeek(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_WEEK)-1;
    }
    
    private int dayOfMonth(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_MONTH);
    }
    
    private int getStepByPeriod(int period){
        switch(period){
            case 1: return 60;
            case 6: return 6*12;
            case 124: return 24*4;
            case 324: return 3*24*2;
            case 724: return 7*24;
            case 130: return 30 * 4;
            case 330: return 3 * 30 * 2;
            case 1365: return 365;
            case 3365: return 3 * 51;
        }
        return -1;
    }
    
    public List<String> getSqls(){
        Date today = new Date();
        Date yearAgo = new Date(today.getTime() - ((long)366*24*60*60*1000));
        List<String> sqls = new ArrayList();                
        
        for(Price p : prices){
            double support[] = ((PivotPrice)p).getSupport();
            double resistance[] = ((PivotPrice)p).getResistance();

            String sql = "INSERT IGNORE INTO `prices_pivot_classic`"
                + "(`ticker`, `date`, `period`, `value`,"
                + " `grade`, `pivot_point`, `s1`, `r1`, `s2`, `r2`, `s3`, `r3`, `s4`, `r4`, `s5`, `r5`) VALUES"
                + "('"+ticker+"','"+p.getDate().getTime()+"','"+ this.period + "'," + p.getValue() + ","
                + p.getGrade() + "," + ((PivotPrice)p).getPp();
            for (int i=0; i<5; i++){
                sql += ", " + support[i] + ", " + resistance[i];
            }
            sql += ")";
            sqls.add(sql);
//            if(p.getDate().compareTo(yearAgo)==1){
//                sql = "INSERT IGNORE INTO `yearly_indicator_grades` PARTITION("+ticker.charAt(0)+")"
//                + "(`ticker`, `date`, `period`, `period_num`, `indicator_id`, `value`,"
//                + " `grade`, `min`, `max`, `avg`, `extra`) VALUES"
//                + "('"+ticker+"','"+p.getDate()+"','"+getPeriod()+"', "
//                    + getPeriodNum()+", "+indicatorId+","
//                    + p.getValue()+","+p.getGrade()+","
//                    + p.getMin()+","+p.getMax()+", "+p.getAvg()+", "
//                    + "'"+p.getExtra()+"')";
//
//                sqls.add(sql);
//            }
        }
                        
        return sqls;
    }
}
