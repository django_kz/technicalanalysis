package technicalanalysis.ultimateoscillator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.json.simple.JSONArray;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class UltimateOscillatorIndicator extends Indicator{
    List<Integer> avgDays = new ArrayList();
    HashMap<Integer, Integer> avgDaysCoef = new HashMap();
    
    public UltimateOscillatorIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new UltimateOscillatorPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("avg");
        
        jsonArray.forEach((o) -> {            
            avgDays.add((int)((long)o));
        });
        Collections.sort(avgDays);
        
        int lcm = lcm(avgDays);
        avgDays.forEach(a->{
            avgDaysCoef.put(a, lcm/a);
        });
    }
    
    public UltimateOscillatorIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new UltimateOscillatorPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("avg");
        
        avgDays.add(hash.get("avg"));
        avgDays.add(2 * hash.get("avg"));
        avgDays.add(4 * hash.get("avg"));
        
        int lcm = lcm(avgDays);
        avgDays.forEach(a->{
            avgDaysCoef.put(a, lcm/a);
        });
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + avgDays.get(avgDays.size()-1)+1;
    }
    
    public void calculate(){
        //cutAtBeginning(avgDays.get(avgDays.size()-1)+1);
        setBpTr();        
        cutDays(2);
        avgDays.forEach(this::setAverages);
        cutDays(avgDays.get(avgDays.size()-1));                
        
        prices.forEach(p->{
            ((UltimateOscillatorPrice)p).setUltimateOscillatorValue(avgDaysCoef);
            ((UltimateOscillatorPrice)p).setUltimateOscillatorGrade();           
        });
        //cutAtEnding();
    }
    
    // Buying Pressure & True Range
    public void setBpTr(){
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();

            index--;
            while(index >= 0){
                double adjClose = prices.get(index).getAdjClose();
                double adjHigh = prices.get(index).getAdjHigh();
                double adjLow = prices.get(index).getAdjLow();

                double max = adjHigh > prevAdjClose?adjHigh:prevAdjClose;
                double min = adjLow < prevAdjClose?adjLow:prevAdjClose;
                double trueRange = TechnicalAnalysis.subtract(max, min);
                double buyingPressure = TechnicalAnalysis.subtract(adjClose, min);
                ((UltimateOscillatorPrice)prices.get(index)).setTrueRange(trueRange);
                ((UltimateOscillatorPrice)prices.get(index)).setBuyingPressure(buyingPressure);

                prevAdjClose = adjClose;
                index--;
            }
        }
    }
    
    private void setAverages(int days){
        int index = prices.size()-1;
        double sumBP = 0.0;
        ArrayList<Double> buyingPressures = new ArrayList();
        Double sumTR = 0.0;
        ArrayList<Double> trueRanges = new ArrayList();
        
        for(int i = index; i > index-days+1 && i>=0; i--){
            buyingPressures.add(((UltimateOscillatorPrice)prices.get(i)).getBuyingPressure());
            sumBP = TechnicalAnalysis.sum(sumBP, ((UltimateOscillatorPrice)prices.get(i)).getBuyingPressure());
            trueRanges.add(((UltimateOscillatorPrice)prices.get(i)).getTrueRange());
            sumTR = TechnicalAnalysis.sum(sumTR, ((UltimateOscillatorPrice)prices.get(i)).getTrueRange());
        }
        
        index = index-days+1;
        
        while(index >= 0){
            buyingPressures.add(((UltimateOscillatorPrice)prices.get(index)).getBuyingPressure());
            sumBP = TechnicalAnalysis.sum(sumBP, ((UltimateOscillatorPrice)prices.get(index)).getBuyingPressure());
            trueRanges.add(((UltimateOscillatorPrice)prices.get(index)).getTrueRange());
            sumTR = TechnicalAnalysis.sum(sumTR, ((UltimateOscillatorPrice)prices.get(index)).getTrueRange());                        
            
            ((UltimateOscillatorPrice)prices.get(index)).avg.put(days, sumBP/sumTR);
            
            sumBP = TechnicalAnalysis.subtract(sumBP, buyingPressures.get(0));
            buyingPressures.remove(0);
            sumTR = TechnicalAnalysis.subtract(sumTR, trueRanges.get(0));
            trueRanges.remove(0);
            index--;
        }
    }
    
    private static int lcm(List<Integer> input){
        int result = input.get(0);
        for(int i = 1; i < input.size(); i++) result = lcm(result, input.get(i));
        return result;
    }
    
    private static int lcm(int a, int b){
        return a * (b / gcd(a, b));
    }
    
    private static int gcd(int a, int b){
        while (b > 0){
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
