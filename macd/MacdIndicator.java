package technicalanalysis.macd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.HashMap;
import org.json.simple.JSONArray;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class MacdIndicator extends Indicator{
    ArrayList<Integer> ema = new ArrayList();
    
    int macdEma;
    
    public MacdIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> emas){


            
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new MacdPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        ema.add(emas.get("macd_fast"));
        ema.add(emas.get("macd_slow"));
        Collections.sort(ema);
        macdEma = emas.get("macd_signal");
//        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("ema");
//        
//        jsonArray.forEach((o) -> {
//            ema.add((int)((long)o));
//        });
//        Collections.sort(ema);
//        macdEma = (int)((long)settings.getTechnical().get("ema_macd"));
    }
    public MacdIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices){


            
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new MacdPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        JSONArray jsonArray = (JSONArray) settings.getTechnical().get("ema");
        
        jsonArray.forEach((o) -> {
            ema.add((int)((long)o));
        });
        Collections.sort(ema);
        macdEma = (int)((long)settings.getTechnical().get("ema_macd"));
    }
    public int getCutEdge(){
        return super.getCutEdge() + ema.get(ema.size()-1) + macdEma;
    }
 
    
    public void calculate(){
        //cutAtBeginning(ema.get(ema.size()-1)+macdEma);
        ema.forEach(this::setEMA);        
        setExtraEma();
        cutDays(ema.get(ema.size()-1));
        setMacd();
        setMinMax(indicatorId);        
        cutDays(macdEma);
        setGrades();
        cutDays(2);
        //cutAtEnding();
    }
    
    private void setExtraEma() {
        if(intervalCount+macdEma+ema.get(ema.size()-1) < prices.size()){
            int maxEma = ema.get(ema.size()-1);
            ema.forEach(e->{
                ((MacdPrice)prices.get(0)).setExtraEma(e, prices.get(intervalCount+macdEma+maxEma-e).getEma(e));
            });
        }
    }
    
    private void setMacd(){
        double smoothingConstant = (double)2/(macdEma+1);
        int index = prices.size()-1;
        double macdLine = 0.0;
        double sum = 0;
        for(int i = index; i > index-macdEma && i>=0; i--){
            macdLine = TechnicalAnalysis.subtract(
                    prices.get(i).getEma(ema.get(0)), prices.get(i).getEma(ema.get(ema.size()-1)));
            prices.get(i).setValue(macdLine);
            sum = TechnicalAnalysis.sum(sum, macdLine);
        }
        double macdLineEma = sum/macdEma;
        
        index = index-macdEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()){
                macdLineEma = ((MacdPrice)prices.get(index)).getMacdEma(indicatorId, period);
            }
                        
            ((MacdPrice)prices.get(index+1)).setMacdLineEma(macdLineEma);
            ((MacdPrice)prices.get(index+1)).setDiff(TechnicalAnalysis.subtract(macdLine, macdLineEma));
            while(index >= 0){          
                macdLine = TechnicalAnalysis.subtract(
                    prices.get(index).getEma(ema.get(0)), prices.get(index).getEma(ema.get(ema.size()-1)));
                prices.get(index).setValue(macdLine);
                macdLineEma = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(macdLine, macdLineEma)), macdLineEma);
                
                ((MacdPrice)prices.get(index)).setMacdLineEma(macdLineEma);
                ((MacdPrice)prices.get(index)).setDiff(TechnicalAnalysis.subtract(macdLine, macdLineEma));
                
                index--;
            }
            if(intervalCount+macdEma+1 < prices.size()){
                ((MacdPrice)prices.get(0)).setExtraMacdEma(((MacdPrice)prices.get(intervalCount+1)).getMacdLineEma());
            }
        }
    }
    
    private void setGrades(){
        int index = prices.size()-1;
        if(index > 0){
            double diff = ((MacdPrice)prices.get(index)).getDiff();
            index--;

            while(index >= 0){
                ((MacdPrice)prices.get(index)).setMacdGrade(diff, macdEma);
                diff = ((MacdPrice)prices.get(index)).getDiff();
                index--;
            }
        }
    }
}
