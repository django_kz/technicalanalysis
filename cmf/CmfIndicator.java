package technicalanalysis.cmf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class CmfIndicator extends Indicator{
    int days;
    
    public CmfIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new CmfPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        days = (int)((long)settings.getTechnical().get("days")); 
    }
    
    public CmfIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new CmfPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        days = hash.get("days");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + days + 1 ;
    }
    
    public void calculate(){
        //cutAtBeginning(days+1);
        prices.forEach(p->{
            ((CmfPrice)p).setMfVolume(period);
        });
        setCmfValues();
        cutDays(days);
        setCmfGrades();
        //cutAtEnding();
    }    

    private void setCmfValues() {
        int index = prices.size()-1;
        
        ArrayList<Double> mfVolumes = new ArrayList();
        double sumMfVolume = 0.0;
        ArrayList<Double> adjVolumes = new ArrayList();
        double sumAdjVolume = 0.0;
        for(int i = index; i > index-days+1 && i>=0; i--){
            CmfPrice p = (CmfPrice) prices.get(i);
            mfVolumes.add(p.getMfVolume());
            sumMfVolume = TechnicalAnalysis.sum(sumMfVolume, p.getMfVolume());
            adjVolumes.add(p.getAdjVolume());
            sumAdjVolume = TechnicalAnalysis.sum(sumAdjVolume, p.getAdjVolume());
        }
        
        index = index-days+1;
        
        while(index>=0){
            CmfPrice p = (CmfPrice) prices.get(index);
            mfVolumes.add(p.getMfVolume());
            sumMfVolume = TechnicalAnalysis.sum(sumMfVolume, p.getMfVolume());
            adjVolumes.add(p.getAdjVolume());
            sumAdjVolume = TechnicalAnalysis.sum(sumAdjVolume, p.getAdjVolume());
                                                
            p.setValue(sumMfVolume/sumAdjVolume);           

            sumMfVolume = TechnicalAnalysis.subtract(sumMfVolume, mfVolumes.get(0));
            sumAdjVolume = TechnicalAnalysis.subtract(sumAdjVolume, adjVolumes.get(0));
            mfVolumes.remove(0);
            adjVolumes.remove(0);
            index--;
        }     
    }

    private void setCmfGrades() {
        int index = prices.size()-1;
        if(index > 0){
            double prevValue = prices.get(index).getValue();
            index--;
            while(index>=0){
                ((CmfPrice)prices.get(index)).setCmfGrade(prevValue, days);
                
                prevValue = prices.get(index).getValue();
                index--;
            }
        }
    }
}
