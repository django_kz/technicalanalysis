package technicalanalysis.adx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class AdxIndicator extends Indicator{
    int days;
    int adxEma;
    
    public AdxIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new AdxPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        days = (int)((long)settings.getTechnical().get("days"));
        adxEma = (int)((long)settings.getTechnical().get("adx_ema"));
    }
    public AdxIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new AdxPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        days = hash.get("days");
        adxEma = hash.get("adx_ema");
        
    }
    public int getCutEdge(){
        return super.getCutEdge() + days + adxEma;
    }
    
    public void calculate(){
        cutAtBeginning(days+adxEma);
        setTrueRange();
        setDMOne();        
        cutDays(2);
        setDx();
        cutDays(days);
        setAdxEma();        
        cutDays(adxEma);
        setMinMax(indicatorId);
        cutAtEnding();
        for(int i=0;i<prices.size();i++){
            Price price = prices.get(i);
            ((AdxPrice)price).setAdxGrade();
        }
    }
    
    private void setTrueRange(){
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjClose = prices.get(index).getAdjClose();

            index--;
            while(index >= 0){
                double adjHigh = prices.get(index).getAdjHigh();
                double adjLow = prices.get(index).getAdjLow();

                double max = adjHigh > prevAdjClose?adjHigh:prevAdjClose;
                double min = adjLow < prevAdjClose?adjLow:prevAdjClose;
                double trueRange = TechnicalAnalysis.subtract(max, min);
                ((AdxPrice)prices.get(index)).setTrueRange(trueRange);

                prevAdjClose = prices.get(index).getAdjClose();
                index--;
            }
        }
    }
    
    private void setDMOne(){
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjHigh = prices.get(index).getAdjHigh();
            double prevAdjLow = prices.get(index).getAdjLow();

            index--;
            while(index >= 0){
                double adjHigh = prices.get(index).getAdjHigh();
                double adjLow = prices.get(index).getAdjLow();

                double high = TechnicalAnalysis.subtract(adjHigh, prevAdjHigh);
                double low = TechnicalAnalysis.subtract(prevAdjLow, adjLow);

                if(high > low){
                    double result = high > 0?high:0;
                    ((AdxPrice)prices.get(index)).setDmOnePlus(result);
                    ((AdxPrice)prices.get(index)).setDmOneMinus(0);
                }else if(low > high){
                    double result = low > 0?low:0;
                    ((AdxPrice)prices.get(index)).setDmOneMinus(result);
                    ((AdxPrice)prices.get(index)).setDmOnePlus(0);
                }else{
                    ((AdxPrice)prices.get(index)).setDmOnePlus(0);
                    ((AdxPrice)prices.get(index)).setDmOneMinus(0);
                }                                
                
                prevAdjHigh = adjHigh;
                prevAdjLow = adjLow;
                index--;
            }
        }
    }
    
    private void setDx(){
        int index = prices.size()-1;
        if(index > 0){
            double sum = 0.0;
            double sumPlus = 0.0;
            double sumMinus = 0.0;
            for(int i = index; i > index-days && i>=0; i--){
                sum = TechnicalAnalysis.sum(sum, ((AdxPrice)prices.get(i)).getTrueRange());
                sumPlus = TechnicalAnalysis.sum(sumPlus, ((AdxPrice)prices.get(i)).getDmOnePlus());
                sumMinus = TechnicalAnalysis.sum(sumMinus, ((AdxPrice)prices.get(i)).getDmOneMinus());
            }
            double prevTrInDays = sum;
            double prevDmPlus = sumPlus;
            double prevDmMinus = sumMinus;

            if(containsGrade() && hasExtra()){
                prevTrInDays = ((AdxPrice)prices.get(index)).getTrInDays(indicatorId, period);
                prevDmPlus = ((AdxPrice)prices.get(index)).getDmPlus(indicatorId, period);
                prevDmMinus = ((AdxPrice)prices.get(index)).getDmMinus(indicatorId, period);
            }
            
            double dlPlus = 100*prevDmPlus/prevTrInDays;
            double dlMinus = 100*prevDmMinus/prevTrInDays;
            double dx = 100*(Math.abs(TechnicalAnalysis.subtract(dlPlus, dlMinus))
                        /Math.abs(TechnicalAnalysis.sum(dlPlus, dlMinus)));

            if(index-days+1 >= 0){
                ((AdxPrice)prices.get(index-days+1)).setTrInDays(prevTrInDays);
                ((AdxPrice)prices.get(index-days+1)).setDmPlus(prevDmPlus);
                ((AdxPrice)prices.get(index-days+1)).setDmMinus(prevDmMinus);
                ((AdxPrice)prices.get(index-days+1)).setDlPlus(dlPlus);
                ((AdxPrice)prices.get(index-days+1)).setDlMinus(dlMinus);
                ((AdxPrice)prices.get(index-days+1)).setDx(dx);
            }
            
            index = index - days;
            while(index >= 0){
                double trInDays = TechnicalAnalysis.sum(
                        TechnicalAnalysis.subtract(prevTrInDays, (prevTrInDays/days)),
                        ((AdxPrice)prices.get(index)).getTrueRange());
                double dmPlus = TechnicalAnalysis.sum(
                        TechnicalAnalysis.subtract(prevDmPlus, (prevDmPlus/days)),
                        ((AdxPrice)prices.get(index)).getDmOnePlus());
                double dmMinus = TechnicalAnalysis.sum(
                        TechnicalAnalysis.subtract(prevDmMinus, (prevDmMinus/days)),
                        ((AdxPrice)prices.get(index)).getDmOneMinus());

                
                dlPlus = 100*(dmPlus/trInDays);
                dlMinus = 100*(dmMinus/trInDays);
                dx = 100*(Math.abs(TechnicalAnalysis.subtract(dlPlus, dlMinus))
                        /Math.abs(TechnicalAnalysis.sum(dlPlus, dlMinus)));

                ((AdxPrice)prices.get(index)).setTrInDays(trInDays);
                ((AdxPrice)prices.get(index)).setDmPlus(dmPlus);
                ((AdxPrice)prices.get(index)).setDmMinus(dmMinus);
                ((AdxPrice)prices.get(index)).setDlPlus(dlPlus);
                ((AdxPrice)prices.get(index)).setDlMinus(dlMinus);
                ((AdxPrice)prices.get(index)).setDx(dx);

                prevTrInDays = trInDays;
                prevDmPlus = dmPlus;
                prevDmMinus = dmMinus;
                index--;
            }
            if(intervalCount+adxEma+days-1 < prices.size()){
                ((AdxPrice)prices.get(0)).setExtraDmValues(((AdxPrice)prices.get(intervalCount+adxEma-1)).getTrInDays(),
                    ((AdxPrice)prices.get(intervalCount+adxEma-1)).getDmPlus(),
                    ((AdxPrice)prices.get(intervalCount+adxEma-1)).getDmMinus());
            }
        }
    }
    
    private void setAdxEma(){        
        int index = prices.size()-1;
        ArrayList<Double> dx = new ArrayList();
        for(int i = index; i > index-adxEma && i>=0; i--){
            dx.add(((AdxPrice)prices.get(i)).getDx());            
        }
        
        double ema  = getSum(dx)/adxEma;
        
        index = index-adxEma;
        if(index >= 0){
            if(containsGrade() && hasExtra()){
                ema = ((AdxPrice)prices.get(index)).getAdxEma(indicatorId, period);
            }
            
            ((AdxPrice)prices.get(index+1)).setAdxEma(ema);
            ((AdxPrice)prices.get(index+1)).setAdxValue();
            while(index >= 0){
                double adx = ((AdxPrice)prices.get(index)).getDx();
                ema = TechnicalAnalysis.sum(ema*(adxEma-1), adx)/adxEma;
                
                ((AdxPrice)prices.get(index)).setAdxEma(ema);
                ((AdxPrice)prices.get(index)).setAdxValue();
                                
                index--;
            }
            if(intervalCount+adxEma < prices.size()){
                ((AdxPrice)prices.get(0)).setExtraAdxEma(((AdxPrice)prices.get(intervalCount)).getAdxEma());
            }
        }       
    }   
}
