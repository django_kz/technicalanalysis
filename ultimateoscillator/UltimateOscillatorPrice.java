package technicalanalysis.ultimateoscillator;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class UltimateOscillatorPrice extends Price{
    private double buyingPressure;
    private double trueRange;
    
    HashMap<Integer, Double> avg = new HashMap();
    
    public UltimateOscillatorPrice(Date date, double adjClose, double adjHigh, double adjLow,
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getBuyingPressure() {
        return buyingPressure;
    }

    public void setBuyingPressure(double buyingPressure) {
        this.buyingPressure = buyingPressure;
    }

    public double getTrueRange() {
        return trueRange;
    }

    public void setTrueRange(double trueRange) {
        this.trueRange = trueRange;
    }
    
    public double getAvg(int days){
        return avg.get(days);
    }
    
    public void setAvg(int days, double value){
        avg.put(days, value);
    }
 
    public void setUltimateOscillatorValue(HashMap<Integer, Integer> coef){
        int divisor = 0;
        double sum = 0.0;
        for(int key:avg.keySet()){
            sum = TechnicalAnalysis.sum(sum, avg.get(key)*coef.get(key));
            divisor += coef.get(key);
        }
        
        setValue(100*sum/divisor);
    }
    
    public void setUltimateOscillatorGrade(){
        float ultimateOscillatorGrade = -1f;
        if(this.value >= 30 &&  this.value <= 70){
            ultimateOscillatorGrade = 
                    (float) (4.0+TechnicalAnalysis.subtract(70, this.value)/20);
        }else if(this.value < 30){
            ultimateOscillatorGrade = 
                    (float) (6.0+TechnicalAnalysis.subtract(30, this.value)/7.5);
        }else if(this.value > 70){
            ultimateOscillatorGrade = 
                    (float) (TechnicalAnalysis.subtract(100, this.value)/7.5);
        }
        
        setGrade(ultimateOscillatorGrade);
        
        // Min Max Avg
        setMin(100);
        setMax(0);
        setAvg(50);
        
        // extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("Ultimate Oscillator", getValue());
        setExtra(extraJSON);
    }
}
