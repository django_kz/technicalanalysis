package technicalanalysis.tsi;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class TsiPrice extends Price{
    private double momentum;
    private double momentumAbs;
    
    private double emaMomentum;
    private double emaMomentumAbs;
    
    private double emaOfEma;
    private double emaOfEmaAbs;
    
    public TsiPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        extra.put("calculation", new JSONObject());
    }

    public double getMomentum() {
        return momentum;
    }

    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public double getMomentumAbs() {
        return momentumAbs;
    }

    public void setMomentumAbs(double momentumAbs) {
        this.momentumAbs = momentumAbs;
    }    
    
    public double getEmaMomentum() {
        return emaMomentum;
    }

    public double getEmaMomentum(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ema_momentum");
    }
    
    public void setEmaMomentum(double emaMomentum) {
        this.emaMomentum = emaMomentum;
    }

    public double getEmaMomentumAbs() {
        return emaMomentumAbs;
    }

    public double getEmaMomentumAbs(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ema_momentum_abs");
    }
    
    public void setEmaMomentumAbs(double emaMomentumAbs) {
        this.emaMomentumAbs = emaMomentumAbs;
    }

    public void setExtraEmaMomentum(double emaMomentum){
        JSONObject extraJSON = extra;        
        ((JSONObject)extraJSON.get("calculation")).put("ema_momentum", emaMomentum);
        setExtra(extraJSON);
    }
    
    public void setExtraEmaMomentumAbs(double emaMomentumAbs){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("ema_momentum_abs", emaMomentumAbs);
        setExtra(extraJSON);
    }
    
    public double getEmaOfEma() {
        return emaOfEma;
    }

    public double getEmaOfEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ema_of_ema");
    }
    
    public void setEmaOfEma(double emaOfEma) {
        this.emaOfEma = emaOfEma;
    }

    public double getEmaOfEmaAbs() {
        return emaOfEmaAbs;
    }

    public double getEmaOfEmaAbs(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("ema_of_ema_abs");
    }
    
    public void setEmaOfEmaAbs(double emaOfEmaAbs) {
        this.emaOfEmaAbs = emaOfEmaAbs;
    }
    
    public void setExtraEmaOfEma(double emaOfEma){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("ema_of_ema", emaOfEma);
        setExtra(extraJSON);
    }
    
    public void setExtraEmaOfEmaAbs(double emaOfEmaAbs){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("ema_of_ema_abs", emaOfEmaAbs);
        setExtra(extraJSON);
    }
    
    public void setTsiValue(){
        setValue(emaOfEma/emaOfEmaAbs*100);
        
        // MIN MAX AVG
        setMax(-100);
        setMin(100);
    }
    
    public void setTsiGrade(){
        float tsiGrade = -1;
        if(getValue() == 0){
            tsiGrade = 5f;
        }else if(getValue() >= -25 && getValue() < 0){
            tsiGrade = (float) (4 + TechnicalAnalysis.subtract(getValue(), -25)/25);
        }else if(getValue() <= 25 && getValue() > 0){
            tsiGrade = (float) (5+getValue()/25);
        }else if(getValue() < -25){
            tsiGrade = (float) (4+TechnicalAnalysis.sum(25, getValue())/18.75);
        }else if(getValue() > 25){
            tsiGrade = (float) (6+(getValue()-25)/18.75);
        }

        setGrade(10-tsiGrade);
        
        // extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("TSI", getValue());
        setExtra(extraJSON);
    }
}
