package technicalanalysis.rsi;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class RsiPrice extends Price{
    private double avgGain;
    private double avgLoss;
    private double change;
    
    public RsiPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        extra.put("calculation", new JSONObject());
    }   

    void setExtraAvg(double avgGain, double avgLoss) {
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("avg_gain", avgGain);
        ((JSONObject)extraJSON.get("calculation")).put("avg_loss", avgLoss);
        setExtra(extraJSON);
    }
    
    public double getAvgGain(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("avg_gain");
    }    
    
    public double getAvgLoss(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("avg_loss");
    }   

    public double getAvgGain() {
        return avgGain;
    }

    public void setAvgGain(double avgGain) {
        this.avgGain = avgGain;
    }

    public double getAvgLoss() {
        return avgLoss;
    }

    public void setAvgLoss(double avgLoss) {
        this.avgLoss = avgLoss;
    }
    
    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }        
    
    void setRsiGrade(int rsiDays) {
        float grade = -1;
        if(getValue() >= 30 && getValue() <=70){
            grade = (float)TechnicalAnalysis.sum(4, TechnicalAnalysis.subtract(70, getValue())/20);
        }else if(getValue() < 30){
            grade = (float)TechnicalAnalysis.sum(6, TechnicalAnalysis.subtract(30, getValue())/7.5);
        }else if(getValue() > 70){
            grade = (float)(TechnicalAnalysis.subtract(100, getValue())/7.5);
        }
        
        if(grade < 0){ 
            grade = 0;
        }else if(grade > 10){
            grade = 10;
        }
        
        setGrade(grade);
        
        // min max
        setMin(100);
        setMax(0);
        setAvg(50);
        
        // extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("RSI ("+rsiDays+")", getValue());
        setExtra(extraJSON);
    }
    
}
