package technicalanalysis.cmf;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class CmfPrice extends Price{
    private double mfVolume;
    
    public CmfPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }

    public double getMfVolume() {
        return mfVolume;
    }

    public void setMfVolume(int period) {
        if(this.adjHigh == this.adjLow){
            this.mfVolume = 0.0;
        }else{
            double a = TechnicalAnalysis.subtract(this.adjClose, this.adjLow);
            double b = TechnicalAnalysis.subtract(this.adjHigh, this.adjClose);
            double c = TechnicalAnalysis.subtract(this.adjHigh, this.adjLow);
            double mfMultiplier = TechnicalAnalysis.subtract(a, b)/c;                        
            
            this.mfVolume = mfMultiplier*this.adjVolume;
        }
    }
    
    public void setCmfGrade(double prevValue, int cmfDays){
        float gradeCmf = -1;
        JSONObject extraJSON = extra;
        if(value < -0.5){
            gradeCmf = 0f;
        }else if(value > 0.5){
            gradeCmf = 10f;
        }else if(this.value > 0 && prevValue > 0){
            gradeCmf = (float) (TechnicalAnalysis.sum(this.value, 0.5)/0.1);                            
        }else if(this.value < 0 && prevValue < 0){
            gradeCmf = (float) (5+this.value/0.1);            
        }else{
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "Change of sign of CMF. Hence, we put grade 5.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 0);
            gradeCmf = 5f;
        }        
        
        setGrade(gradeCmf);
        
        // Min Max Avg
        setMin(-0.5);
        setMax(0.5);
        setAvg(0.0);
        
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("CMF ("+cmfDays+")", getValue());
        setExtra(extraJSON);
    }
}