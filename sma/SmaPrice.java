package technicalanalysis.sma;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class SmaPrice extends Price{
    public SmaPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
        
        this.value = this.adjClose;
    }        
    
    public void setMovingAverageGrade(int days) {
        setAvg(adjClose);
        
        if(getSma(days) < getAvg()){
            double step = Math.abs(TechnicalAnalysis.subtract(this.getAvg(),this.getMin())/5);
            this.setGrade((float)(Math.abs((TechnicalAnalysis.subtract(this.getSma(days), this.getMin())))/(step==0?1:step)));
        }else if(getSma(days) > getAvg()){
            double step = Math.abs(TechnicalAnalysis.subtract(this.getMax(),this.getAvg())/5);
            this.setGrade((float)(5 + Math.abs(TechnicalAnalysis.subtract(this.getSma(days),this.getAvg()))/(step==0?1:step)));
        }else{
            this.setGrade(5f);
        }
        
        this.setGrade(10-this.getGrade());
        
        double temp = getMax();
        setMax(getMin());
        setMin(temp);
        
        setValue(getSma(days));
        
        JSONObject extraJSON = new JSONObject();
        extraJSON.put("top", new JSONObject());
        ((JSONObject)extraJSON.get("top")).put("SMA ("+days+")", getValue());
        setExtra(extraJSON);
    }
}
