package technicalanalysis.mfi;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class MfiPrice extends Price{
    
    public MfiPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);
    }
    
    public void setMfiGrade(int mfiDays){
        float grade = -1f;
        if(this.getValue() >= 20 
                && this.getValue() < 80){                
            grade = (float)TechnicalAnalysis.sum(4.0, TechnicalAnalysis
                    .subtract(80, this.getValue())/30);
        }else if(this.getValue() < 20){
            grade = (float)TechnicalAnalysis.sum(6.0f, TechnicalAnalysis
                    .subtract(20, this.getValue())/5);
        }else if(this.getValue() > 80){
            grade = (float)(TechnicalAnalysis.subtract(100, this.getValue())/5);            
        }
        
        if(grade > 10){
            grade = 10;
        }else if(grade < 0){
            grade = 0;
        }
        
        setGrade(grade);
        
        //min max
        setMin(100);
        setMax(0);
        setAvg(50);
        
        // extra
        JSONObject extraJSON = extra;
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("MFI ("+mfiDays+")", getValue());
        setExtra(extraJSON);
    }
    
}
