package technicalanalysis.macd;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class MacdPrice extends Price{
    private double macdLineEma;
    private double diff;

    public MacdPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);        
    }

    void setExtraEma(int days, double ema) {
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("ema ("+days+")", ema);
        setExtra(extraJSON);
    }
    
    public double getMacdEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("macd_ema");
    }

    public void setExtraMacdEma(double ema){
        JSONObject extraJSON = extra;
        ((JSONObject)extraJSON.get("calculation")).put("macd_ema", ema);
        setExtra(extraJSON);
    }
    
    public double getMacdLineEma() {
        return macdLineEma;
    }

    public void setMacdLineEma(double macdLineEma) {
        this.macdLineEma = macdLineEma;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }       
    
    public void setMacdGrade(double prevDiff, int emaDays){
        if(this.getValue() < 0){
            double step = Math.abs(this.getMin()/5);
            this.setGrade((float)((this.getValue()-this.getMin())
                            /(step==0?1:step)));
        }else if(this.getValue() > 0){
            double step = Math.abs(this.getMax()/5);
            this.setGrade((float)(5 + (this.getValue())/(step==0?1:step)));
        }else{
            this.setGrade(5f);
        }
        
        JSONObject extraJSON = extra;
        if(prevDiff < 0 && diff > 0 && this.getGrade() > 6){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the MACD rises above the signal line, we assign Grade 10.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            this.setGrade(10f);
        }else if(prevDiff > 0 && diff < 0 && this.getGrade() < 4){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the MACD falls below the signal line, we assign Grade 0.");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            this.setGrade(0f);
        }
        
        extraJSON.put("top", new JSONObject());
        ema.keySet().forEach(key->{
            ((JSONObject)extraJSON.get("top")).put("EMA"+key, ema.get(key));
        });
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("MACD", getValue());
        ((JSONObject)extraJSON.get("bottom")).put("EMA"+emaDays+" (MACD)", getMacdLineEma());
        setExtra(extraJSON);
    }
}
