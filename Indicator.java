package technicalanalysis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Indicator {      
    
    protected long indicatorId;
    protected String ticker;
    protected int intervalCount;
    protected int period;
    
    protected List<Price> prices = new ArrayList();
    
    public Indicator(Long indicatorId, String ticker, Settings settings, List<Price> prices){
        this.indicatorId = indicatorId;
        this.ticker = ticker;
        this.intervalCount = settings.getInterval();
        this.period = settings.getPeriod();
        this.prices = prices;
    }
    
    protected void setSMA(int days){
        int index = prices.size()-1;
        ArrayList<Double> close = new ArrayList();
        for(int i = index; i > index-days+1 && i>=0; i--){
            close.add(prices.get(i).getAdjClose());
        }
        double sum = getSum(close);
        
        index = index-days+1;
        while(index >= 0){
            close.add(prices.get(index).getAdjClose());
            sum = TechnicalAnalysis.sum(sum, prices.get(index).getAdjClose());            
            
            prices.get(index).setSma(days, sum/days);                                      
            
            sum = TechnicalAnalysis.subtract(sum, close.get(0));
            close.remove(0);
            index--;
        }
    }
    
    protected void setEMA(int days){
        double smoothingConstant = (double)2/(days+1);
        int index = prices.size()-1;
        ArrayList<Double> close = new ArrayList();
        for(int i = index; i > index-days && i>=0; i--){
            close.add(prices.get(i).getAdjClose());
        }
        
        double ema  = getSum(close)/days;                        
        index = index-days;
        if(index >= 0){
            if(containsGrade() && !Double.isNaN(prices.get(0).getEma(indicatorId, period, days))){
                ema = prices.get(0).getEma(indicatorId, period, days);
            }            
            prices.get(index+1).setEma(days, ema);
            while(index >= 0){
                double adjClose = prices.get(index).getAdjClose();
                ema = TechnicalAnalysis
                        .sum(smoothingConstant*(TechnicalAnalysis.subtract(adjClose, ema)), ema);
                prices.get(index).setEma(days, ema);
                index--;
            }            
        }
    }
    
    protected void setMinMax(long indicatorId){
        TreeSet<Double> values = new TreeSet();
        HashMap<Double, Integer> countOfValueInTree = new HashMap();
        LinkedList<Double> indexOfValue = new LinkedList();
        
        int index = prices.size()-1;
        for(int i = index; i>index-intervalCount+1 && i>=0; i--){
            //insert
            double value = prices.get(i).getValue();
            if(!countOfValueInTree.containsKey(value)){
                values.add(value);
                countOfValueInTree.put(value, 1);
            }else{
                countOfValueInTree.put(value, countOfValueInTree.get(value)+1);
            }
            indexOfValue.add(value);
            //end of insert
            
            //set MinMax if size less than 7500
//            if(i<7500){
                prices.get(i).setMin(values.first());
                prices.get(i).setMax(values.last());
//            }
        }
        index = index-intervalCount+1;
        while(index>=0){
            //insert
            double value = prices.get(index).getValue();
            if(!countOfValueInTree.containsKey(value)){
                values.add(value);
                countOfValueInTree.put(value, 1);
            }else{
                countOfValueInTree.put(value, countOfValueInTree.get(value)+1);
            }
            indexOfValue.add(value);
            //end of insert
            
            prices.get(index).setMin(values.first());
            prices.get(index).setMax(values.last());
            
//            System.out.println(values.toString());
//            System.out.println(prices.get(index).getDate()+"\t"+value+"\t"+values.first()+"\t"+values.last());
            
            //remove first
            double removeValue = indexOfValue.get(0);
            countOfValueInTree.put(removeValue, 
                    countOfValueInTree.get(removeValue)-1);
            if(countOfValueInTree.get(removeValue)==0){
                values.remove(removeValue);
                countOfValueInTree.remove(removeValue);
            }
            indexOfValue.remove(0);
            //end of remove
            index--;
        }
    }
    
    public int getCutEdge(){
        return  1;
    }
    
    public void cutAtBeginning(int technicalMax){
        int till = this.prices.size();
        for(int i=0;i<this.prices.size();i++){
            Price price = this.prices.get(i);
            if(!price.isNeedToCount(indicatorId)){                              // || i==7499
                till = i;
                break;
            }
        }
        if(till>0){
            till+= technicalMax+intervalCount;
            if(till<this.prices.size()){
                this.prices = this.prices.subList(0, till);
            }
        }else{
            this.prices.clear();
        }
    }
    
    public void cutDays(int days){
        if(days<prices.size() && days != 0){
            prices = prices.subList(0, prices.size()-days+1);
        }else{
            prices.clear();
        }
    }
    
    public void cutAtEnding(){
        //remove prices without grades
//        if(prices.size()>7500){
//            prices = prices.subList(0, 7500);
//        }
        
        int till = this.prices.size();
        for(int i=0;i<this.prices.size();i++){
            Price price = this.prices.get(i);
            boolean startCut = true;
            if(price.isNeedToCount(indicatorId)){
                startCut = false;
            }
            if(startCut){
                till = i;
                break;
            }
        }
        if(till<this.prices.size()){
            this.prices = this.prices.subList(0, till);
        }
    }
    
    public List<String> getSqls(){
        Date today = new Date();
        Date yearAgo = new Date(today.getTime() - ((long)366*24*60*60*1000));
        List<String> sqls = new ArrayList();                
        
        for(Price p : prices){
            String sql = "INSERT IGNORE INTO `technicalanalysis_indicator_grades` PARTITION("+ticker.charAt(0)+")"
                + "(`ticker`, `date`, `period`, `period_num`, `indicator_id`, `value`,"
                + " `grade`, `min`, `max`, `avg`, `extra`) VALUES"
                + "('"+ticker+"','"+p.getDate().getTime()+"','"+getPeriod()+"', "
                    + getPeriodNum()+", "+indicatorId+","
                    + p.getValue()+","+p.getGrade()+","
                    + p.getMin()+","+p.getMax()+", "+p.getAvg()+", "
                    + "'"+p.getExtra()+"')";

            sqls.add(sql);
//            if(p.getDate().compareTo(yearAgo)==1){
//                sql = "INSERT IGNORE INTO `yearly_indicator_grades` PARTITION("+ticker.charAt(0)+")"
//                + "(`ticker`, `date`, `period`, `period_num`, `indicator_id`, `value`,"
//                + " `grade`, `min`, `max`, `avg`, `extra`) VALUES"
//                + "('"+ticker+"','"+p.getDate()+"','"+getPeriod()+"', "
//                    + getPeriodNum()+", "+indicatorId+","
//                    + p.getValue()+","+p.getGrade()+","
//                    + p.getMin()+","+p.getMax()+", "+p.getAvg()+", "
//                    + "'"+p.getExtra()+"')";
//
//                sqls.add(sql);
//            }
        }
                        
        return sqls;
    }

    public List<Price> getPrices(){
      return this.prices;
        
    }
    
    protected char getPeriod(){
        switch(period){
            case 1: return 'H'; 
            case 6: return 'H'; 
            case 124: return 'D'; 
            case 324: return 'D';
            case 724: return 'D';
            case 130: return 'M';
            case 330: return 'M';
            case 1365: return 'Y';
            case 3365: return 'Y';
            default: return 'H';
        }
    }
    
    protected int getPeriodNum(){
        switch(period){
            case 1: return 1; 
            case 6: return 6; 
            case 124: return 1; 
            case 324: return 3;
            case 724: return 7;
            case 130: return 1;
            case 330: return 3;
            case 1365: return 1;
            case 3365: return 3;
            default: return 1;
        }
    }        
    
    protected double getSum(ArrayList<Double> values){
        double result = 0;
        for(double a:values){
            result = TechnicalAnalysis.sum(result, a);
        }
        return result;
    }
    
    protected boolean containsGrade(){
        return prices.get(0).currentDate.containsKey(indicatorId);
    }
    
    protected boolean hasExtra(){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(prices.get(0).currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return object.containsKey("calculation");
    }
    
    protected void calculate(){
        
    }
    
    protected List<Price> calculatePrices(){
        return this.prices;
    }
    
    protected void calculateFinal(List<Price> prices){
        this.calculate();
    }
}
