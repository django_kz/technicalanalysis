package technicalanalysis.pivot.classic;

import java.util.ArrayList;
import java.util.List;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;
import technicalanalysis.pivot.PivotIndicator;
import technicalanalysis.pivot.PivotPrice;

public class ClassicIndicator extends PivotIndicator{
    
    public ClassicIndicator(long indicatorId, String ticker, Settings settings, List<Price> prices, int period) {
        super(indicatorId, ticker, settings, prices, period);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new PivotPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
    }
    
    public void calculate(){
        //cutAtBeginning(25);
        findPP();
        setSupportAndResistance();
        //cutAtEnding();        
        prices.forEach((p)->{
            ((PivotPrice)p).setValue(p.getAdjClose());              
            ((PivotPrice)p).setPivotGrade();
//            if(period==5){
//                double[] res = ((PivotPrice)p).getResistance();
//                double[] sup = ((PivotPrice)p).getSupport();
//                System.out.println(p.getDate()+"\t"+sup[4]+"\t"+sup[3]+"\t"+sup[2]+"\t"+sup[1]+"\t"+sup[0]+"\t"+((PivotPrice)p).getPp()+"\t"+res[0]+"\t"+res[1]+"\t"+res[2]+"\t"+res[3]+"\t"+res[4]+"\t"+p.getGrade());
//                System.out.println(p.getDate()+"\t"+p.getValue()+"\t"+p.getGrade());
//            }
        });   
    }

    private void setSupportAndResistance() {
        prices.forEach((p)->{
            double support[] = new double[5];
            double resistance[] = new double[5];
            
            support[0] = TechnicalAnalysis.subtract(2*((PivotPrice)p).getPp(), ((PivotPrice)p).getPpHigh());
            resistance[0] = TechnicalAnalysis.subtract(2*((PivotPrice)p).getPp(), ((PivotPrice)p).getPpLow());
            for(int i  = 1; i < 5; i++){
                support[i] = TechnicalAnalysis.subtract(((PivotPrice)p).getPp(), ((PivotPrice)p).getDiff()*i);
                resistance[i] = TechnicalAnalysis.sum(((PivotPrice)p).getPp(), ((PivotPrice)p).getDiff()*i);
            }
            
            ((PivotPrice)p).setSupport(support);
            ((PivotPrice)p).setResistance(resistance);
        });
    }        
}
