package technicalanalysis.trix;

import java.util.Date;
import java.util.HashMap;
import javafx.util.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import technicalanalysis.Price;
import technicalanalysis.TechnicalAnalysis;

public class TrixPrice extends Price{
    private double doubleEma;
    private double tripleEma;
    private double trixEma;
    private double diff;    
    
    public TrixPrice(Date date, double adjClose, double adjHigh, double adjLow, 
            double adjVolume, long close, Date updatedAt, HashMap<Long, Pair<Date, HashMap<Integer, String>>> lastDate) {
        super(date, adjClose, adjHigh, adjLow, adjVolume, close, updatedAt, lastDate);        
    }

    void setExtraEma(int days, double ema) {
        JSONObject extraJSON = extra;
        extraJSON.put("calculation", new JSONObject());
        ((JSONObject)extraJSON.get("calculation")).put("ema ("+days+")", ema);
        setExtra(extraJSON);
    }
    
    public double getDoubleEma() {
        return doubleEma;
    }

    public double getDoubleEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("double_ema");
    }
    
    public void setDoubleEma(double doubleEma) {
        this.doubleEma = doubleEma;
    }

    public void setExtraDoubleEma(double doubleEma){
        JSONObject extraJSON = extra;        
        ((JSONObject)extraJSON.get("calculation")).put("double_ema", doubleEma);
        setExtra(extraJSON);
    }
    
    public double getTripleEma() {
        return tripleEma;
    }

    public double getTripleEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("triple_ema");
    }
    
    public void setTripleEma(double tripleEma) {
        this.tripleEma = tripleEma;
    }

    public void setExtraTripleEma(double tripleEma){
        JSONObject extraJSON = extra;        
        ((JSONObject)extraJSON.get("calculation")).put("triple_ema", tripleEma);
        setExtra(extraJSON);
    }
    
    public double getTrixEma() {
        return trixEma;
    }

    public double getTrixEma(long indicatorId, int period){
        JSONObject object = new JSONObject();
        try {
            object =  (JSONObject) new JSONParser().parse(currentDate.get(indicatorId).getValue().get(period));
        } catch (ParseException ex) {
            ex.printStackTrace(TechnicalAnalysis.log);
        }
        return (double) ((JSONObject)object.get("calculation")).get("trix_ema");
    }
    
    public void setTrixEma(double trixEma) {
        this.trixEma = trixEma;
    }    
    
    public void setExtraTrixEma(double trixEma){
        JSONObject extraJSON = extra;        
        ((JSONObject)extraJSON.get("calculation")).put("trix_ema", trixEma);
        setExtra(extraJSON);
    }
    
    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }    

    public void setTrixGrade(double prevDiff, int emaDays) {
        float trixGrade = -1;
        if(getValue() == 0){
            trixGrade = 5f;
        }else if(getValue() < 0){
            double step = Math.abs(min/5);
            trixGrade = (float) (TechnicalAnalysis.subtract(getValue(), min)/step);
        }else if(getValue() > 0){
            double step = Math.abs(max/5);
            trixGrade = (float) TechnicalAnalysis.sum(5, getValue()/step);
        }
        
        JSONObject extraJSON = extra;
        if(diff < 0 && prevDiff > 0 && trixGrade < 4){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the TRIX falls below the signal line, we assign Grade 0.");
            ((JSONObject)extraJSON.get("warning")).put("sign", -1);
            trixGrade = 0;
        }else if(diff > 0 && prevDiff < 0 && trixGrade > 6){
            extraJSON.put("warning", new JSONObject());
            ((JSONObject)extraJSON.get("warning")).put("text", "When the TRIX rises above the signal line, we assign Grade 10.");
            ((JSONObject)extraJSON.get("warning")).put("sign", 1);
            trixGrade = 10;
        }
        
        setGrade(trixGrade);
        
        // extra
        extraJSON.put("bottom", new JSONObject());
        ((JSONObject)extraJSON.get("bottom")).put("TRIX", getValue());
        ((JSONObject)extraJSON.get("bottom")).put("EMA"+emaDays+" (TRIX)", getTrixEma());
        setExtra(extraJSON);
    }
    
}
