package technicalanalysis.vortex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import technicalanalysis.Indicator;
import technicalanalysis.Price;
import technicalanalysis.Settings;
import technicalanalysis.TechnicalAnalysis;

public class VortexIndicator extends Indicator{
    int vmDays;
    int trDays;
    int smaDays;
    public VortexIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices) {
        super(indicatorId, ticker, settings, prices);
        
        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new VortexPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;
        
        vmDays = (int)((long)settings.getTechnical().get("vm"));
        trDays = (int)((long)settings.getTechnical().get("tr"));
        smaDays = (int)((long)settings.getTechnical().get("sma"));
    }

    public VortexIndicator(Long indicatorId, String ticker, Settings settings, List<Price> prices, HashMap<String, Integer> hash) {
        super(indicatorId, ticker, settings, prices);

        List<Price> temp = new ArrayList();
        this.prices.forEach((price) -> {
            temp.add(new VortexPrice(price.getDate(), price.getAdjClose(), 
                    price.getAdjHigh(), price.getAdjLow(), price.getAdjVolume(),
                    price.getCloseInCents(), price.getUpdatedAt(), price.getCurrentDate()));
        });
        this.prices = temp;

        vmDays = hash.get("vm");
        trDays = vmDays;
        smaDays = hash.get("sma");
    }
    
    public int getCutEdge(){
        return super.getCutEdge() + max() + 2 ;
    }

    public void calculate(){
        //cutAtBeginning(max()+2);
        setVmTr();
        cutDays(2);
        setVm();
        setTr();        
        setSMA(smaDays);
        prices.forEach((t) -> {
            ((VortexPrice)t).setVlPlus();
            ((VortexPrice)t).setVlMinus();
            ((VortexPrice)t).setVortexValue();
        });
        
        setMinMax(indicatorId);
        cutDays(max());
        setGrades();
        cutDays(2);
        //cutAtEnding();
//        if(period == 5){
//            prices.forEach(p->{
//                System.out.println(p.getDate()+"\t"+p.getValue()+"\t"+p.getMax()+"\t"+p.getMin()+"\t"+p.getGrade());
////                System.out.println(p.getDate()+"\t"+((VortexPrice)p).getTrueRange()+"\t"+((VortexPrice)p).getVmPlus()+"\t"+((VortexPrice)p).getVmMinus()+"\t"+((VortexPrice)p).getTrDays()+"\t"+((VortexPrice)p).getVmPDays()+"\t"+((VortexPrice)p).getVmMDays()+"\t"+((VortexPrice)p).getVlPlus()+"\t"+((VortexPrice)p).getVlMinus());
//            });
//        }
    }
    
       public List<Price> calculatePrices(){
//        cutAtBeginning(Math.max(highLowEma+emaOfEma+massIndexDays-2, sma+1));
        setVmTr();
        cutDays(2);
        setVm();
        setTr();        
        setSMA(smaDays);
        prices.forEach((t) -> {
            ((VortexPrice)t).setVlPlus();
            ((VortexPrice)t).setVlMinus();
            ((VortexPrice)t).setVortexValue();
        });

        return this.prices;
//        cutAtEnding();
    }
    
    public void calculateFinal(List<Price> prices){       
        setMinMax(indicatorId);
        cutDays(max());
        setGrades();
        cutDays(2); 
    }
    
    private void setVmTr(){
        int index = prices.size()-1;
        if(index > 0){
            double prevAdjHigh = prices.get(index).getAdjHigh();
            double prevAdjLow = prices.get(index).getAdjLow();
            double prevAdjClose = prices.get(index).getAdjClose();

            index--;
            while(index >= 0){
                double adjHigh = prices.get(index).getAdjHigh();
                double adjLow = prices.get(index).getAdjLow();
                double adjClose = prices.get(index).getAdjClose();

                double vmPlus = Math.abs(TechnicalAnalysis.subtract(adjHigh, prevAdjLow));
                double vmMinus = Math.abs(TechnicalAnalysis.subtract(adjLow, prevAdjHigh));
                double max = adjHigh > prevAdjClose?adjHigh:prevAdjClose;
                double min = adjLow < prevAdjClose?adjLow:prevAdjClose;
                double tr = TechnicalAnalysis.subtract(max, min);
                ((VortexPrice)prices.get(index)).setTrueRange(tr);
                
                
                ((VortexPrice)prices.get(index)).setVmPlus(vmPlus);
                ((VortexPrice)prices.get(index)).setVmMinus(vmMinus);

                prevAdjHigh = adjHigh;
                prevAdjLow = adjLow;
                prevAdjClose = adjClose;
                index--;
            }
        }
    }

    private void setVm() {
        int index = prices.size()-1;
        
        ArrayList<Double> vmP = new ArrayList();
        ArrayList<Double> vmM = new ArrayList();
        double sumP = 0.0;
        double sumM = 0.0;
        
        for(int i = index; i > index-vmDays+1 && i>=0; i--){
            vmP.add(((VortexPrice)prices.get(i)).getVmPlus());
            vmM.add(((VortexPrice)prices.get(i)).getVmMinus());
            sumP = TechnicalAnalysis.sum(sumP, 
                    ((VortexPrice)prices.get(i)).getVmPlus());
            sumM = TechnicalAnalysis.sum(sumM, 
                    ((VortexPrice)prices.get(i)).getVmMinus());
        }
        
        index = index-vmDays+1;
        while(index >= 0){
            vmP.add(((VortexPrice)prices.get(index)).getVmPlus());
            vmM.add(((VortexPrice)prices.get(index)).getVmMinus());
            sumP = TechnicalAnalysis.sum(sumP, 
                    ((VortexPrice)prices.get(index)).getVmPlus());
            sumM = TechnicalAnalysis.sum(sumM, 
                    ((VortexPrice)prices.get(index)).getVmMinus());
            
            ((VortexPrice)prices.get(index)).setVmPDays(sumP);
            ((VortexPrice)prices.get(index)).setVmMDays(sumM);            
            
            sumP = TechnicalAnalysis.subtract(sumP,vmP.get(0));
            sumM = TechnicalAnalysis.subtract(sumM,vmM.get(0));
            vmP.remove(0);
            vmM.remove(0);
            index--;
        }
    }

    private void setTr() {
        int index = prices.size()-1;                                            
        
        ArrayList<Double> tr = new ArrayList();
        double sum = 0.0;
        
        for(int i = index; i > index-trDays+1 && i>=0; i--){
            tr.add(((VortexPrice)prices.get(i)).getTrueRange());
            sum = TechnicalAnalysis.sum(sum, ((VortexPrice)prices.get(i)).getTrueRange());
        }
        
        index = index-trDays+1;
        while(index >= 0){
            tr.add(((VortexPrice)prices.get(index)).getTrueRange());
            sum = TechnicalAnalysis.sum(sum, ((VortexPrice)prices.get(index)).getTrueRange());
            
            ((VortexPrice)prices.get(index)).setTrDays(sum);
            
            sum = TechnicalAnalysis.subtract(sum, tr.get(0));
            tr.remove(0);
            index--;
        }
    }
    
    private void setGrades(){
        int index = prices.size()-1;
        if(index > 0){
            double prevValue = prices.get(index).getValue();
            index--;
            while(index >= 0){
                ((VortexPrice)prices.get(index)).setVortexGrade(prevValue, smaDays);
                
                prevValue = prices.get(index).getValue();
                index--;
            }
        }
    }
    
    private int max(){
        return Math.max(smaDays, Math.max(vmDays, trDays));
    }
}
